<?php

namespace App\Form;

use App\Entity\Event;
use App\Repository\EventRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $globalEvents = $options['global_events'];
        $builder
            ->add('title')
            ->add('date', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('isGlobal')
            ->add(
                'parentEvent',
                ChoiceType::class,
                [
                    'choices' => $globalEvents,
                    'choice_label' => 'title',
                    'required' => false,
                ]
            )
            ->add('categories')
            ->add('description', TextareaType::class, [
                'required' => false,
            ])
            ->add('links', CollectionType::class, [
                'required' => false,
                'by_reference' => false,
                'allow_add' => false,
                'allow_delete' => true,
                'entry_type' => LinkType::class,
                'entry_options' => ['label' => false],
                'empty_data' => 'qsd',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'global_events' => null,
        ]);
    }
}
