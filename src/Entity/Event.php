<?php

namespace App\Entity;

use App\Repository\EventRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $title;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $description;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'subEvents')]
    private $parentEvent;

    #[ORM\OneToMany(mappedBy: 'parentEvent', targetEntity: self::class)]
    private $subEvents;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'events')]
    private $categories;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Link::class)]
    private $links;

    #[ORM\Column(type: 'boolean')]
    private $isGlobal;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $end_date;

    public function __construct()
    {
        $this->subEvents = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParentEvent(): ?self
    {
        return $this->parentEvent;
    }

    public function setParentEvent(?self $parentEvent): self
    {
        $this->parentEvent = $parentEvent;

        return $this;
    }

    public function removeParentEvent(): self
    {
        $this->setParentEvent(null);

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSubEvents(): Collection
    {
        return $this->subEvents;
    }

    public function addSubEvent(self $subEvent): self
    {
        if (!$this->subEvents->contains($subEvent)) {
            $this->subEvents[] = $subEvent;
            $subEvent->setParentEvent($this);
        }

        return $this;
    }

    public function removeSubEvent(self $subEvent): self
    {
        if ($this->subEvents->removeElement($subEvent)) {
            // set the owning side to null (unless already changed)
            if ($subEvent->getParentEvent() === $this) {
                $subEvent->setParentEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        if (count($this->getCategories()) > 0) {
            return $this->getCategories()[0];
        } elseif ($this->getParentEvent() && count($this->getParentEvent()->getCategories()) > 0) {
            return $this->getParentEvent()->getCategories()[0];
        } else {
            return null;
        }
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function isPassed()
    {
        $now = new DateTime();
        return $this->getDate() < $now;
    }

    /**
     * Revoie vrai si l'événement s'est déroulé avant aujourd'hui.
     */
    public function beforeToday()
    {
        $now = new DateTime();

        return $this->getDate() < $now &&
            ($this->getDate()->format('Y') !== $now->format('Y')
                || $this->getDate()->format('n') !== $now->format('n')
                || $this->getDate()->format('j') !== $now->format('j')
            );
    }

    public function getAbsoluteDate()
    {
        return date('Y-m-d H:i', $this->getDate()->getTimestamp());
    }

    /**
     * Liste combinée des liens de l'événement et de ceux de ses parents.
     * @return Link[]
     */
    public function getLinks()
    {
        $res = [];
        foreach ($this->links as $link) {
            $res[] = $link;
        }
        // return array_merge($res, $this->getParentLinks());
        return $this->links;
    }

    /**
     * @return Link[]
     */
    public function getParentLinks()
    {
        if ($this->getParentEvent()) {
            $res = [];
            foreach ($this->getParentEvent()->getLinks() as $link) {
                $res[] = $link;
            }
            return array_merge($res, $this->getParentEvent()->getParentLinks());
        } else {
            return [];
        }
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setEvent($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->removeElement($link)) {
            // set the owning side to null (unless already changed)
            if ($link->getEvent() === $this) {
                $link->setEvent(null);
            }
        }

        return $this;
    }

    public function getIsGlobal(): ?bool
    {
        return $this->isGlobal;
    }

    public function setIsGlobal(bool $isGlobal): self
    {
        $this->isGlobal = $isGlobal;

        return $this;
    }

    public function getLabel()
    {
        if (count($this->getCategories()) > 0) {
            $cat = $this->getCategories()[0];
            return $this->getTitle() . " ($cat)";
        } elseif ($this->getParentEvent() && count($this->getParentEvent()->getCategories()) > 0) {
            $cat = $this->getParentEvent()->getCategories()[0];
            return $this->getTitle() . " ($cat)";
        } else {
            return $this->getTitle();
        }
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(?\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }
}
