<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Event::class, mappedBy: 'categories')]
    private $events;

    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'subCategories')]
    private $parentCategory;

    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'parentCategory')]
    private $subCategories;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->parentCategory = new ArrayCollection();
        $this->subCategories = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addCategory($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            $event->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getParentCategory(): Collection
    {
        return $this->parentCategory;
    }

    public function addParentCategory(self $parentCategory): self
    {
        if (!$this->parentCategory->contains($parentCategory)) {
            $this->parentCategory[] = $parentCategory;
        }

        return $this;
    }

    public function removeParentCategory(self $parentCategory): self
    {
        $this->parentCategory->removeElement($parentCategory);

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSubCategories(): Collection
    {
        return $this->subCategories;
    }

    public function addSubCategory(self $subCategory): self
    {
        if (!$this->subCategories->contains($subCategory)) {
            $this->subCategories[] = $subCategory;
            $subCategory->addParentCategory($this);
        }

        return $this;
    }

    public function removeSubCategory(self $subCategory): self
    {
        if ($this->subCategories->removeElement($subCategory)) {
            $subCategory->removeParentCategory($this);
        }

        return $this;
    }
}
