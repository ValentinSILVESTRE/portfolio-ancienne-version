<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/taquin')]
class TaquinController extends AbstractController
{
    #[Route('', name: 'taquin')]
    public function index(): Response
    {
        return $this->render('taquin/index.html.twig', [
            'controller_name' => 'TaquinController',
        ]);
    }

    #[Route('/game', name: 'taquin_game')]
    public function game(Request $request): Response
    {
        $size = 2;
        if ($request->get('difficulty')) {
            $size = $request->get('difficulty');
        }
        return $this->render('taquin/game.html.twig', [
            'controller_name' => 'TaquinController',
            'size' => $size,
        ]);
    }
}
