<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mines')]
class MineController extends AbstractController
{
    #[Route('', name: 'app_mines')]
    public function mines(): Response
    {
        return $this->render(
            'mines/index.html.twig'
        );
    }

    #[Route('/play', name: 'app_minesPlay')]
    public function minesPlay(): Response
    {
        return $this->render(
            'mines/play.html.twig'
        );
    }

    #[Route('/settings', name: 'app_minesSettings')]
    public function minesSettings(): Response
    {
        return $this->render(
            'mines/settings.html.twig'
        );
    }
}
