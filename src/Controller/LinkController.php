<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkType;
use App\Repository\EventRepository;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/link')]
class LinkController extends AbstractController
{
    #[Route('/new', name: 'link_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll();

        $link = new Link();
        $form = $this->createForm(linkType::class, $link);
        $form->handleRequest($request);
        $eventId = $_GET['eventId'];

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $eventRepository->find($eventId);
            $event->addLink($link);

            $entityManager->persist($link);
            $entityManager->flush();

            return $this->redirectToRoute('event_show', [
                'id' => $eventId,
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('calendar/link/new.html.twig', [
            'link' => $link,
            'form' => $form,
            'route' => 'new link',
            'allEvents' => $allEvents,
        ]);
    }

    #[Route('/{id}', name: 'link_show', methods: ['GET'])]
    public function show(Link $link): Response
    {
        return $this->render('calendar/link/show.html.twig', [
            'link' => $link,
        ]);
    }

    #[Route('/{id}/edit', name: 'link_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Link $link, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(linkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('calendar', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('link/edit.html.twig', [
            'link' => $link,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'link_delete', methods: ['POST'])]
    public function delete(Request $request, Link $link, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $link->getId(), $request->request->get('_token'))) {
            $entityManager->remove($link);
            $entityManager->flush();
        }

        return $this->redirectToRoute('calendar', [], Response::HTTP_SEE_OTHER);
    }
}
