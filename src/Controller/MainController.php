<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(): Response
    {
        return $this->render(
            'homepage/index.html.twig'
        );
    }

    #[Route('/mondrian', name: 'app_mondrian')]
    public function mondrian(): Response
    {
        return $this->render(
            'mondrian/index.html.twig'
        );
    }
}
