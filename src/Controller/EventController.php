<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Link;
use App\Form\EventType;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/event')]
class EventController extends AbstractController
{
    #[Route('/new', name: 'event_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll();

        $event = new Event();
        $link1 = new Link();
        $link2 = new Link();
        $link3 = new Link();
        $links = [$link1, $link2, $link3];
        foreach ($links as $link) {
            $event->addLink($link);
        }

        /** @var Array Liste des événements globaux */
        $globalEvents = $eventRepository->findGlobalEvents();

        // On tri les événements globaux par catégories
        $categories = [];
        foreach ($globalEvents as $globalEvent) {
            if ($globalEvent->getCategory()) {
                $category = $globalEvent->getCategory()->getName();
                $categories[$category][] = $globalEvent;
            }
        }
        ksort($categories);

        foreach ($categories as $key => $category) {
            if (count($category) > 0) {
                $categories[$key] = eventSort($categories[$key]);
            }
        }

        $form = $this->createForm(EventType::class, $event, ['global_events' => $categories]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // On persiste uniquement les liens qui ont un url non nul
            foreach ($links as $link) {
                if (!$link->getUrl()) {
                    $event->removeLink($link);
                    $link->removeEvent();
                } else {
                    $entityManager->persist($link);
                }
            }
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('calendar', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('calendar/event/new.html.twig', [
            'allEvents' => $allEvents,
            'event' => $event,
            'form' => $form,
            'route' => 'new event',
        ]);
    }

    #[Route('/{id}', name: 'event_show', methods: ['GET'])]
    public function show(Event $event, EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll();

        return $this->render('calendar/event/show.html.twig', [
            'event' => $event,
            'date' => date('l d F', $event->getDate()->getTimestamp()),
            'time' => date('g:i a', $event->getDate()->getTimestamp()),
            'route' => 'show event',
            'allEvents' => $allEvents,
        ]);
    }

    #[Route('/{id}/edit', name: 'event_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Event $event, EntityManagerInterface $entityManager, EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll();

        /** @var Array Liste des événements globaux */
        $globalEvents = $eventRepository->findGlobalEvents();

        // On tri les événements globaux par catégories
        $categories = [];
        foreach ($globalEvents as $globalEvent) {
            if ($globalEvent->getCategory()) {
                $category = $globalEvent->getCategory()->getName();
                $categories[$category][] = $globalEvent;
            }
        }
        ksort($categories);

        foreach ($categories as $key => $category) {
            if (count($category) > 0) {
                $categories[$key] = eventSort($categories[$key]);
            }
        }

        $form = $this->createForm(EventType::class, $event, ['global_events' => $categories]);
        $form->handleRequest($request);

        $originalLinks = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($event->getLinks() as $link) {
            $originalLinks->add($link);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('calendar', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('calendar/event/edit.html.twig', [
            'event' => $event,
            'form' => $form,
            'route' => 'edit event',
            'allEvents' => $allEvents,
        ]);
    }

    #[Route('/{id}', name: 'event_delete', methods: ['POST'])]
    public function delete(Request $request, Event $event, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $event->getId(), $request->request->get('_token'))) {
            // On supprime l'événement de la liste de ses parents
            if ($parent = $event->getParentEvent()) {
                $parent->removeSubEvent($event);
            }
            $event->removeParentEvent();

            // On supprime l'événement en tant que parent de ses enfants
            foreach ($event->getSubEvents() as $subEvent) {
                $subEvent->removeParentEvent();
                $event->removeSubEvent($subEvent);
            }

            // On supprime les liens associés
            foreach ($event->getLinks() as $link) {
                $entityManager->remove($link);
            }

            $entityManager->remove($event);
            $entityManager->flush();
        }

        return $this->redirectToRoute('calendar');
    }
}

function eventSort(array $array)
{
    $list = $array;
    // Pour i de 0 à max - 1
    for ($i = 0; $i < count($list) - 1; $i++) {
        // On met le min à la i° position
        $list = exchangeKey($list, $i, minKey($list, $i));
    }
    return $list;
}

function exchangeKey($array, $keyA, $keyB)
{
    $list = $array;
    if ($keyA != $keyB && array_key_exists($keyA, $list) && array_key_exists($keyB, $list)) {
        $tmp = $list[$keyA];
        $list[$keyA] = $list[$keyB];
        $list[$keyB] = $tmp;
    }
    return $list;
}

function minKey($array, $minIndex)
{
    if (count($array) <= $minIndex) {
    } else {
        $minKey = $minIndex;
        for ($i = $minIndex + 1; $i < count($array); $i++) {
            if (stringBefore($array[$i]->getTitle(), $array[$minKey]->getTitle())) {
                $minKey = $i;
            }
        }
        return $minKey;
    }
}

// todo: Ca bug
function stringBefore(String $stringA, String $stringB): bool
{
    return true;
    var_dump($stringA, $stringB);
    for ($i = 0; $i < strlen($stringA); $i++) {
        $a = strtoupper($stringA[$i]);
        $b = strtoupper($stringB[$i]);
        if (!$b) return false;
        if ($a != $b) return $a < $b;
    }

    // Ici stringA est une sous-chaîne de stringB alors on renvoie vrai
    return true;
}
