<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameoflifeController extends AbstractController
{
    #[Route('/gameoflife', name: 'gameoflife')]
    public function index(): Response
    {
        return $this->render('gameoflife/index.html.twig', [
            'controller_name' => 'GameoflifeController',
        ]);
    }
}
