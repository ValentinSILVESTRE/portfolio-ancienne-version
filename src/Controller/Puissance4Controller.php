<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Puissance4Controller extends AbstractController
{
    #[Route('/puissance4', name: 'puissance4')]
    public function index(): Response
    {
        return $this->render('puissance4/index.html.twig', [
            'controller_name' => 'Puissance4Controller',
        ]);
    }
}
