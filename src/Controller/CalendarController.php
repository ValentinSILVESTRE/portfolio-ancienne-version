<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/calendar')]
class CalendarController extends AbstractController
{
    #[Route('', name: 'calendar')]
    public function index(EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll();
        $nextEvents = [];
        for ($i = 0; $i < count($allEvents); $i++) {
            $event = $allEvents[$i];
            if (!$event->beforeToday()) {
                $nextEvents[] = $event;
            }
        }

        return $this->render('calendar/index.html.twig', [
            'controller_name' => 'CalendarController',
            'events' => $nextEvents,
            'allEvents' => $allEvents,
            'title' => 'Calendar',
            'route' => 'calendar',
        ]);
    }

    #[Route('/history', name: 'history')]
    public function history(EventRepository $eventRepository): Response
    {
        $allEvents = $eventRepository->findAll(['sort' => 'newer']);
        // $passedEvents = [];
        // for ($i = 0; $i < count($allEvents); $i++) {
        //     $event = $allEvents[$i];
        //     if ($event->beforeToday()) {
        //         $passedEvents[] = $event;
        //     }
        // }
        $passedEvents = $eventRepository->findPast(['sort' => 'newer']);
        // $passedEvents = [];
        // for ($i = 0; $i < count($allEvents); $i++) {
        //     $event = $allEvents[$i];
        //     if (!$event->beforeToday()) {
        //         $passedEvents[] = $event;
        //     }
        // }
        // dd($passedEvents, $allEvents);
        return $this->render('calendar/history.html.twig', [
            'controller_name' => 'CalendarController',
            'events' => $passedEvents,
            'allEvents' => $allEvents,
            'title' => 'History',
            'route' => 'history',
        ]);
    }
}
