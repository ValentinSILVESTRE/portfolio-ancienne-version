<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/results', name: 'results')]
    public function index(EventRepository $eventRepository): Response
    {
        $events = $eventRepository->findAll();

        return $this->render('calendar/search/index.html.twig', [
            'controller_name' => 'SearchController',
            'events' => $events,
            'allEvents' => $events,
            'route' => 'results',
        ]);
    }
}
