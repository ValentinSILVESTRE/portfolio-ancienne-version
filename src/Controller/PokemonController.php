<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/pokemons')]
class PokemonController extends AbstractController
{
    #[Route('', name: 'pokemon')]
    public function index(): Response
    {
        return $this->render('pokemon/index.html.twig', [
            'controller_name' => 'PokemonController',
        ]);
    }

    #[Route('/fight', name: 'pokemon-fight')]
    public function fight(): Response
    {
        return $this->render('pokemon/fight.html.twig', [
            'controller_name' => 'PokemonController',
        ]);
    }

    #[Route('/cover', name: 'pokemon-cover')]
    public function cover(): Response
    {
        return $this->render('pokemon/cover.html.twig', [
            'controller_name' => 'PokemonController',
        ]);
    }

    #[Route('/process', name: 'pokemon-process')]
    public function process(Request $request): Response
    {
        $team1 = $request->get('team1');
        $team2 = $request->get('team2');
        $tier = $request->get('tier');
        $file = $request->files->get('import-team');


        setcookie('team1import', false);
        setcookie('team1', $team1);
        setcookie('team2', $team2);
        setcookie('tier', $tier);

        return $this->redirectToRoute('pokemon-fight');
    }
}


function createTeamImport()
{
    // On ouvre le fichier txt en lecture
    $txtFile = fopen('../sources/teams/myTeam.txt', 'r');

    // On s'assure que l'ouverture s'est bien passée
    if ($txtFile) {
        // On créé les pokemons correspondant
        $pokemonList = [];

        do {
            $line = fgets($txtFile, 4096);
            if ($line !== false) {
                // On construit le pokemon
                $pokemon = [];

                $name = '';
                $item = '';
                $ability = '';
                $nature = '';
                $level = 100;
                $evs = [
                    'hp' => 0,
                    'atk' => 0,
                    'def' => 0,
                    'spa' => 0,
                    'spd' => 0,
                    'spe' => 0,
                ];
                $ivs = [
                    'hp' => 31,
                    'atk' => 31,
                    'def' => 31,
                    'spa' => 31,
                    'spd' => 31,
                    'spe' => 31,
                ];
                $moves = [];

                // $line est la première ligne du pokemon (nom ...)
                // echo "<p><b>Pokemon</b></p>";
                // var_dump($line);

                $name = explode(' ', trim($line))[0];
                $item = explode(' @ ', trim($line))[1];

                do {
                    // Pour les lignes suivantes
                    $line = trim(fgets($txtFile, 4096));

                    if (strlen($line) > 0) {
                        $firstWord = explode(' ', $line)[0];

                        switch ($firstWord) {
                            case 'Ability:':
                                $ability = explode('Ability: ', $line)[1];
                                break;
                            case 'EVs:':
                                $evList = explode(' / ', trim(explode('EVs: ', $line)[1]));
                                foreach ($evList as $ev) {
                                    $statname = explode(' ', $ev)[1];
                                    $value = explode(' ', $ev)[0];
                                    $evs[strtolower($statname)] = $value;
                                }
                                break;
                            case 'IVs:':
                                $ivList = explode(' / ', trim(explode('IVs: ', $line)[1]));
                                foreach ($ivList as $iv) {
                                    $statname = explode(' ', $iv)[1];
                                    $value = explode(' ', $iv)[0];
                                    $ivs[strtolower($statname)] = $value;
                                }
                                break;
                            case '-':
                                $move = trim(explode('- ', $line)[1]);
                                $moves[] = $move;
                                break;

                            default:
                                $nature = explode(' ', trim($line))[0];
                                break;
                        }
                    }
                } while (strlen($line) > 0);

                $pokemon['name'] = $name;
                $pokemon['item'] = $item;
                $pokemon['ability'] = $ability;
                $pokemon['nature'] = $nature;
                $pokemon['level'] = $level;
                $pokemon['ev'] = $evs;
                $pokemon['iv'] = $ivs;
                $pokemon['moves'] = $moves;

                $pokemonList[] = $pokemon;
            }
        } while ($line !== false);

        fclose($txtFile);

        // On écrit les pokemons dans le fichier js
        $jsFile = fopen('../js/myTeam.js', 'w');

        fwrite($jsFile, "const myTeam = [\n");
        foreach ($pokemonList as $pokemon) {
            $name = $pokemon['name'];
            $item = $pokemon['item'];
            $nature = $pokemon['nature'];
            $level = $pokemon['level'];
            $ability = $pokemon['ability'];
            $moves = $pokemon['moves'];
            $evs = $pokemon['ev'];
            $ivs = $pokemon['iv'];

            fwrite($jsFile, "\t($name = {\n");
            fwrite($jsFile, "\t\tname: '$name',\n");
            fwrite($jsFile, "\t\titem: '$item',\n");
            fwrite($jsFile, "\t\tlevel: '$level',\n");
            fwrite($jsFile, "\t\tnature: '$nature',\n");
            fwrite($jsFile, "\t\tability: '$ability',\n");

            fwrite($jsFile, "\t\tmoves: [");
            foreach ($moves as $move) {
                if ($move !== $moves[0]) {
                    fwrite($jsFile, ", ");
                }
                fwrite($jsFile, "'$move'");
            }
            fwrite($jsFile, "],\n");

            fwrite($jsFile, "\t\tiv: {\n");
            foreach ($ivs as $stat => $value) {
                fwrite($jsFile, "\t\t\t$stat: $value,\n");
            }
            fwrite($jsFile, "\t\t},\n");

            fwrite($jsFile, "\t\tev: {\n");
            foreach ($evs as $stat => $value) {
                fwrite($jsFile, "\t\t\t$stat: $value,\n");
            }
            fwrite($jsFile, "\t\t},\n");

            fwrite($jsFile, "\t}),\n");
        }
        fwrite($jsFile, "];\n");

        fclose($jsFile);

        return $pokemonList;
    }
}
