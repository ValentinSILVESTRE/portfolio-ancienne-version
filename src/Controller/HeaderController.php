<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeaderController extends AbstractController
{
    #[Route('/header', name: 'header')]
    public function index(EventRepository $eventRepository): Response
    {
        return $this->render('header.html.twig', [
            'controller_name' => 'HeaderController',
            'allEvents' => $eventRepository->findAll(),
        ]);
    }
}
