**<h1 align='center' id='Informations'>Informations</h1>**

[![LTS](https://img.shields.io/badge/Symfony-5.4-blue)](https://symfony.com/releases/5.4)
[![LTS](https://img.shields.io/badge/Composer-2.2.3-purple)](https://getcomposer.org/changelog/2.2.3)
[![LTS](https://img.shields.io/badge/PHP-8.0-oranged)](https://www.php.net/releases/8.0/en.php)

---

**<h1 align='center' id='Content table'>Content table</h1>**

1.[Initialization](#Initialization)

2.[To Do](#To_Do)

3.[Commands](#Commands)

4.[Content](#Content)

5.[Responsive](#Responsive)

**<h1 align='center' id='Initialization'>1. Initialization</h1>**

-   Création du projet symfony complet avec la version long term support: `symfony new project --version=lts --full`
-   Sécurisation https: `symfony server:ca:install`
-   Bundles supplémentaires
    -   [Security](https://symfony.com/doc/current/security.html): `composer require symfony/security-bundle`
    -   [CKEditor](https://symfony.com/bundles/FOSCKEditorBundle/current/index.html): `composer require friendsofsymfony/ckeditor-bundle`
    -   [Easy Admin](https://symfony.com/bundles/EasyAdminBundle/current/index.html): `composer require easycorp/easyadmin-bundle`
    -   [Fixtures](https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html): `composer require --dev orm-fixtures`
    -   [Encore](https://symfony.com/doc/current/frontend/encore/installation.html): `composer require symfony/webpack-encore-bundle`
-   Configuration de Sass
    [lien](https://symfony.com/doc/current/frontend/encore/simple-example.html#using-sass-less-stylus)
    , [Tuto Grafikart](https://www.youtube.com/watch?v=KjC8VdQUFbI)
-   Formatage Twig (pluggin [Melody](https://codeknight.co.uk/blog/getting-prettier-working-with-twig-craft-cms))
-   User:
    -   `php bin/console make:user` pour créér l'entité
    -   `php bin/console make:registration-form` pour créér un compte
    -   `php bin/console make:auth` pour se connecter

---

**<h1 align='center' id='To_Do'>2. To Do</h1>**

## `Bugs`

-   Calendar

    -   Evénements privés
    -   Faire de meilleurs requêtes (chercher les events à 48h pour ceux que l'on va notifier par exemple)
    -   Calcul écart 31 janvier -> 3 ou 4 février

## `Fonctionnalités`

-   Remember me [Symfony](https://symfony.com/doc/current/security/remember_me.html)
-   Mailing
-   Reset password [Youtube](https://www.youtube.com/watch?v=9RA3yAp4xw8)

---

**<h1 align='center' id='Commands'>3. Commands</h1>**

-   `npm run watch` pour compiler les fichiers de style et javascript
-   `symfony open:local` pour ouvrir le projet dans le navigateur
-   `composer dump-env dev` pour modifier l'environnement (dev, prod, local)

---

**<h1 align='center' id='Content'>4. Content</h1>**

| Projects     | HTML | CSS | JS  | PHP | SYMFONY | ANGULAR | EXTERNE | RESPONSIVE |
| :----------- | :--: | :-: | :-: | :-: | :-----: | :-----: | :-----: | :--------: |
| Stage MHB    |  ✅  | ✅  | ✅  | ✅  |   ✅    |         |   ✅    |            |
| Site         |  ✅  | ✅  | ✅  | ✅  |   ✅    |         |         |            |
| Calendar     |  ✅  | ✅  | ✅  | ✅  |   ✅    |         |         |            |
| Taquin       |  ✅  | ✅  | ✅  | ✅  |         |         |         |            |
| Puissance 4  |  ✅  | ✅  | ✅  | ✅  |         |         |         |            |
| Mines        |  ✅  | ✅  | ✅  | ✅  |         |         |         |            |
| Game of Life |  ✅  | ✅  | ✅  | ✅  |         |         |         |            |
| Pokemon      |  ✅  | ✅  | ✅  | ✅  |         |         |         |            |
| Mondrian     |  ✅  | ✅  |     |     |         |         |         |     ✅     |

---

**<h1 align='center' id='Responsive'>5. Responsive</h1>**

Tailles bootstrap

-   Extra-small : 320 ~ 575
-   Small : 576 ~ 767
-   Medium : 768 ~ 991
-   Large : 992 ~ 1199
-   Extra-large : >= 1200
