<?php
require_once('dbConnect.php');

function getBestTimes(String $imageName, int $number, int $difficulty)
{
    $dbh = dbConnect();
    $req = $dbh->prepare("SELECT * FROM times WHERE image_name = :image_name AND difficulty = :difficulty ORDER BY time ASC LIMIT $number");

    $req->execute(array(
        'image_name' => $imageName,
        'difficulty' => $difficulty,
    ));

    $list = [];
    while ($data = $req->fetch()) {
        $raw = [];
        $raw['user_id'] = $data['user_id'];
        $raw['image_name'] = $data['image_name'];
        $raw['time'] = $data['time'];
        $raw['difficulty'] = $data['difficulty'];
        $list[] = $raw;
    }

    return $list;
}

function getPlayerById(int $id)
{
    global $env;
    $dbh = dbConnect();
    $req = $dbh->prepare("SELECT name FROM user WHERE id = :id");

    $req->execute(array(
        'id' => $id,
    ));

    $data = $req->fetch();
    if ($data) {
        return $data['name'];
    }
    return false;
}


/**
 * timeToString
 *
 * @param  int $time - Nombre de millièmes de seconde
 * @return String
 */
function timeToString(int $time)
{
    $nbThousandths = $time % 1000;
    $nbSeconds = floor($time / 1000) % 60;
    $nbMinutes = floor($time / (1000 * 60)) % 60;
    $nbHours = floor($time / (1000 * 3600));

    if ($nbHours > 0) {
        return "$nbHours h " . ($nbMinutes < 10 ? "0" : "") . $nbMinutes;
    }
    if ($nbMinutes > 0) {
        return "$nbMinutes min " . ($nbSeconds < 10 ? "0" : "") . $nbSeconds;
    }
    return "$nbSeconds," . ($nbThousandths < 10 ? "00" : ($nbThousandths < 100 ? "0" : "")) . $nbThousandths . 's';
}
