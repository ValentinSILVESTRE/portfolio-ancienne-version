<?php
session_start();
require_once('dbConnect.php');
require_once('functions.php');

// On se connecte à la base de données
$bdd = dbConnect();

// On supprime tous les cookies existants
foreach ($_COOKIE as $key => $value) {
    unset($_COOKIE[$key]);
}

$_COOKIE['imageFolder'] = 'standart';

$firstImageNumber = 1;
$nbImagesStandart = 99;
$nbImagesOthers = 46;
if ($_COOKIE['imageFolder'] === 'others') {
    $lastImageNumber = $nbImagesOthers;
} else {
    $lastImageNumber = $nbImagesStandart;
}

setcookie('imageFolder', $_COOKIE['imageFolder']);
setcookie('firstImageNumber', $firstImageNumber);
setcookie('lastImageNumber', $lastImageNumber);


if (isset($_POST['valider'])) {
    if (isset($_COOKIE['difficulte'])) {
        unset($_COOKIE['difficulte']);
    }
    setcookie('difficulte', $_POST['difficulte']);
    if (isset($_COOKIE['nbJoueurs'])) {
        unset($_COOKIE['nbJoueurs']);
    }
    setcookie('nbJoueurs', $_POST['nbJoueurs']);
    if (isset($_COOKIE['typeDePartie'])) {
        unset($_COOKIE['typeDePartie']);
    }
    setcookie('typeDePartie', $_POST['typeDePartie']);
    if ($_POST['typeDePartie'] === 'partie_simple') {
        setcookie('region', rand($firstImageNumber, $lastImageNumber));
        header('Location: jeu.php');
    } else {
        header('Location: map.php');
    }
};
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Taquin</title>
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="header.css">
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="index.js"></script>
</head>

<body>

    <?php
    require_once("header.php");
    ?>
    <div class="main-container">
        <form action="" method="post">
            <div class="firstContainer">
                <div class="container1">
                    <h2>Type de partie</h2>
                    <section class="section">
                        <div class="container">
                            <input type="radio" id="partieSimple" name="typeDePartie" value="partie_simple" checked>
                            <label for="partieSimple"><span class='radio'>Partie simple</span></label>
                        </div>
                        <div class="container">
                            <input type="radio" id="campagne" name="typeDePartie" value="Campagne">
                            <label for="campagne"><span class="radio">Campagne<span></label>
                        </div>
                    </section>
                </div>
                <div class="container2">
                    <h2>Difficulté</h2>
                    <section class="section">
                        <div class="container">
                            <input type="radio" name="difficulte" id="tresfacile" value="2">
                            <label for="tresfacile"><span class="radio">Très facile<span></label>
                        </div>
                        <div class="container">
                            <input type="radio" name="difficulte" id="facile" value="3">
                            <label for="facile"><span class="radio">Facile<span></label>
                        </div>
                        <div class="container">
                            <input type="radio" name="difficulte" id="moyenne" value="4">
                            <label for="moyenne"><span class="radio">Moyenne<span></label>
                        </div>
                        <div class="container">
                            <input type="radio" name="difficulte" id="difficile" value="5">
                            <label for="difficile"><span class="radio">Difficile<span></label>
                        </div>
                        <div class="container">
                            <input type="radio" name="difficulte" id="tresdifficile" value="6" checked>
                            <label for="tresdifficile"><span class="radio">Très difficile<span></label>
                        </div>
                    </section>
                </div>
                <div class="container4">
                    <div class="submit">
                        <input class="submit1" type="submit" name="valider" value="Jouer">
                    </div>
                </div>
            </div>
        </form>

        <div class="france">
            <div class="imgFrance1">
                <img class="img1" src="./public/images/France_Flag_Map.svg.png" alt="image de la france" width="200px" height="200px">
            </div>
            <div class="imgFrance2">
                <img class="img2" src="./public/images/France_Flag_Map.svg.png" alt="image de la france" width="200px" height="200px">
            </div>
            <div class="imgFrance3">
                <img class="img3" src="./public/images/France_Flag_Map.svg.png" alt="image de la france" width="200px" height="200px">
            </div>
            <div class="imgFrance4">
                <img class="img4" src="./public/images/France_Flag_Map.svg.png" alt="image de la france" width="200px" height="200px">
            </div>
        </div>
    </div>
</body>

</html>