<?php

// On démarre la session
session_start();

// Si on est connecté alors on définit les cookies et on redirige vers listeScores
if (isset($_SESSION) && isset($_SESSION["name"])) {
    $name = $_SESSION["name"];
    setcookie('pseudonyme', $name);
    setcookie('timeIsAdded', 0);
    header('Location:listeScores.php');
    die();
}
// Sinon on redirige vers la page de connexion;
else {
    $_SESSION["previous_page"] = "scores.php";
    header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Scores</title>
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="scores.css">
</head>

<body>

    <p>Il faut se connecter pour pouvoir enregistrer votre score.</p>

</body>

</html>