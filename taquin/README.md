# `Jeu du taquin`

[gitlab](https://gitlab.com/ValentinSILVESTRE/le-taquin)

[youtube](https://www.youtube.com/watch?v=-3IsCOJieCc)

## `Descriptif`

Projet Hackathlon, pendant la formation à Alkas, sur le thème : `Retour à la normale`, en groupe avec [Jonathan](https://gitlab.com/jon.sautel07), [Nicolas](https://gitlab.com/Nico-81) et [Valentin](https://gitlab.com/ValentinSILVESTRE)

## `Objectifs`

Permettre à l'utilisateur de jouer au jeu du taquin, en saisissant son niveau de difficulté, d'enregistrer son score dans le livre des records (bdd).

On pourra choisir son image, ou juste afficher des numéros.

Permettre à un utilisateur de se connecter pour pouvoir enregistrer son score et uploader des images.

Ajouter une bande sonore ou vidéos quand on résout certainne images. (ex: JF de toute façon t'as pas ton titre pour la photo de JF ...).

Permettre de jouer à deux: chaqun son image, ses touches (zqsd, flèches directionnelles).

Ajouter un bouton start pour lancer le chronomètre et autoriser les déplacements.

Ajouter un chrono.

## `ToDo`

`Faire des dossiers`

Faire une page avec tous les meilleurs scores triés par difficulté, joueur, image, dossier et permettre de cliquer pour essayer de la battre.

`Pages`

-   Acceuil:

-   Jeu:

    -   Centrer l'image une fois finie

    -   Améliorer l'affichage:

        -   Agrandir l'image

        -   Diminuer le header

-   ListeScores:

    -   Quand on clique sur 'Autre image' on affiche un pop-up qui permet de changer la difficulté et le nombre de joueurs, puis on redirige

    -   Mettre en évidence notre score

-   Scores

    -   Afficher le temps, la position et permettre de ne pas voir la liste des scores

-   Jeu faire un bouton 'Voir les scores' mis en évidence
