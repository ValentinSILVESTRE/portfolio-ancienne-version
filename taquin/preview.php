<?php
session_start();
require_once('functions.php');

$typeDePartie = $_COOKIE['typeDePartie'];
$nbJoueurs = 1;
$path = ($typeDePartie === 'Campagne' ?
    'regions/' : 'standart/');
$region = $_COOKIE['region'];

$image_folder =
    ($typeDePartie === 'Campagne' ?
        'regions' : $_COOKIE['imageFolder']);

$image_name = "$image_folder/$region.jpg";
$image_number = $_COOKIE['region'];
$image_src = "public/images/game-images/$image_folder/$image_number.jpg";

$times2 = getBestTimes($image_name, 5, 2);
$times3 = getBestTimes($image_name, 5, 3);
$times4 = getBestTimes($image_name, 5, 4);
$times5 = getBestTimes($image_name, 5, 5);
$times6 = getBestTimes($image_name, 5, 6);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preview - <?php echo $_COOKIE['region']; ?></title>
    <link rel="shortcut icon" href="public/icones/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="preview.js"></script>
    <script src="functions.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="preview.css">
</head>

<body>

    <?php
    require_once('header.php');
    ?>

    <div class="main-container">
        <div class="image">

            <img id="image" src='data:image/png;base64, <?php echo base64_encode(file_get_contents($image_src)); ?>'>
        </div>
        <div class="side-bar">
            <div class="times">
                <h2>Meilleurs temps</h2>
                <div class="flex navbar navbar-expand">
                    <h5>Difficulté</h5>
                    <ul class="difficulties navbar-nav">
                        <li class="nav-item"><button class="difficulty-button 2">2</button></li>
                        <li class="nav-item"><button class="difficulty-button 3">3</button></li>
                        <li class="nav-item"><button class="difficulty-button 4">4</button></li>
                        <li class="nav-item"><button class="difficulty-button 5">5</button></li>
                        <li class="nav-item active"><button class="difficulty-button 6">6</button></li>
                    </ul>
                </div>
                <table class="score-table 2 hidden">
                    <tr>
                        <th>Joueur</th>
                        <!-- <th>Difficulté</th> -->
                        <th>Temps</th>
                    </tr>
                    <?php
                    if (!count($times2)) {
                        echo "<tr><td colspan='3'>Aucun score enregistré</td></tr>";
                    } else {
                        for ($i = 0; $i < count($times2); $i++) {
                            echo "<tr><td>" . getPlayerById($times2[$i]['user_id']) . "</td>";
                            // echo "<td>" . $times2[$i]['difficulty'] . "</td>";
                            echo "<td>" . timeToString($times2[$i]['time']) . "</td></tr>";
                        }
                    }
                    ?>
                </table>
                <table class="score-table 3 hidden">
                    <tr>
                        <th>Joueur</th>
                        <!-- <th>Difficulté</th> -->
                        <th>Temps</th>
                    </tr>
                    <?php
                    if (!count($times3)) {
                        echo "<tr><td colspan='3'>Aucun score enregistré</td></tr>";
                    } else {
                        for ($i = 0; $i < count($times3); $i++) {
                            echo "<tr><td>" . getPlayerById($times3[$i]['user_id']) . "</td>";
                            // echo "<td>" . $times3[$i]['difficulty'] . "</td>";
                            echo "<td>" . timeToString($times3[$i]['time']) . "</td></tr>";
                        }
                    }
                    ?>
                </table>
                <table class="score-table 4 hidden">
                    <tr>
                        <th>Joueur</th>
                        <!-- <th>Difficulté</th> -->
                        <th>Temps</th>
                    </tr>
                    <?php
                    if (!count($times4)) {
                        echo "<tr><td colspan='3'>Aucun score enregistré</td></tr>";
                    } else {
                        for ($i = 0; $i < count($times4); $i++) {
                            echo "<tr><td>" . getPlayerById($times4[$i]['user_id']) . "</td>";
                            // echo "<td>" . $times4[$i]['difficulty'] . "</td>";
                            echo "<td>" . timeToString($times4[$i]['time']) . "</td></tr>";
                        }
                    }
                    ?>
                </table>
                <table class="score-table 5 hidden">
                    <tr>
                        <th>Joueur</th>
                        <!-- <th>Difficulté</th> -->
                        <th>Temps</th>
                    </tr>
                    <?php
                    if (!count($times5)) {
                        echo "<tr><td colspan='3'>Aucun score enregistré</td></tr>";
                    } else {
                        for ($i = 0; $i < count($times5); $i++) {
                            echo "<tr><td>" . getPlayerById($times5[$i]['user_id']) . "</td>";
                            // echo "<td>" . $times5[$i]['difficulty'] . "</td>";
                            echo "<td>" . timeToString($times5[$i]['time']) . "</td></tr>";
                        }
                    }
                    ?>
                </table>
                <table class="score-table 6 hidden">
                    <tr>
                        <th>Joueur</th>
                        <!-- <th>Difficulté</th> -->
                        <th>Temps</th>
                    </tr>
                    <?php
                    if (!count($times6)) {
                        echo "<tr><td colspan='3'>Aucun score enregistré</td></tr>";
                    } else {
                        for ($i = 0; $i < count($times6); $i++) {
                            echo "<tr><td>" . getPlayerById($times6[$i]['user_id']) . "</td>";
                            // echo "<td>" . $times6[$i]['difficulty'] . "</td>";
                            echo "<td>" . timeToString($times6[$i]['time']) . "</td></tr>";
                        }
                    }
                    ?>
                </table>
            </div>

            <div class="play">
                <div class="input">
                    <label for="difficulty-input">Difficulté</label>
                    <input type="number" min="2" max="6" name="difficulty-input" id="difficulty-input" value="<?php echo $_COOKIE['difficulte']; ?>">
                </div>
                <a href="jeu.php">
                    <button id="play-button">Jouer</button>
                </a>
            </div>
        </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>