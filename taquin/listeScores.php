<?php

session_start();
require_once('dbConnect.php');
require_once('functions.php');

// On enregistre le score

// On se connecte à la base de données
$bdd = dbConnect();

// On récupère les infos

$user_name = $_COOKIE['pseudonyme'];

// On récupère l'id de l'utilisateur en connaissant son nom
$req = $bdd->prepare('SELECT id FROM `user` WHERE `name` = :name ');
$req->execute([
    'name' => $user_name,
]);
$user_id = $req->fetch()['id'];

$region = $_COOKIE['region'];

$typeDePartie = $_COOKIE['typeDePartie'];

$image_number = $region;

$imageFolder =
    ($typeDePartie === 'Campagne' ?
        'regions' : $_COOKIE['imageFolder']);

$image_src = "public/images/game-images/$imageFolder/$image_number.jpg";
$image_name = "$imageFolder/$image_number.jpg";

$time = stringTimeToThousandths($_COOKIE['lastTime']);

$difficulty = intval($_COOKIE['difficulte']);

$timeIsAdded = boolval($_COOKIE['timeIsAdded']);

// Si on n'a pas encore ajouté le score

if (!$timeIsAdded) {

    setcookie('timeIsAdded', 1);

    // On ajoute le score dans la table jeux_video
    echo ("<p>$timeIsAdded</p>");

    addTime($user_id, $image_name, $difficulty, $time, $bdd);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des scores</title>
    <link rel="stylesheet" href="listeScores.css">
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
    <script src="listeScores.js"></script>
    <script src="functions.js"></script>
</head>

<body>

    <div id="header">
        <a class="link" href="index.php">Acceuil</a>
        <?php
        if ($typeDePartie === 'Campagne') {
            echo '<a id="autreRegion" class="link" href="map.php">Autre région</a>';
        } else {
            echo '<a id="autreImage" class="link" href="jeu.php">Autre image</a>';
        }
        ?>
        <a class="link" href="jeu.php">Rejouer</a>
    </div>

    <div id="main-container">
        <img id="image" src="<?php echo $image_src; ?>" alt="Image">
        <table>
            <tr id="titles">
                <th>Classement</th>
                <th>Joueur</th>
                <th>Temps</th>
                <th>Difficulté</th>
            </tr>

            <?php

            // On récupère chaque score
            $reponse = $bdd->prepare('SELECT * FROM times WHERE image_name = :image_name ORDER BY difficulty DESC, time ASC');

            $reponse->execute(array(
                'image_name' => $image_name
            ));

            $classement = 0;
            while ($donnees = $reponse->fetch()) {
                $player_id = $donnees['user_id'];
                $reqName = $bdd->prepare('SELECT name FROM user WHERE id = :player_id');
                $reqName->execute([
                    'player_id' => $player_id,
                ]);
                $player_name = $reqName->fetch()['name'];
            ?>

                <tr>
                    <td><?php echo ++$classement; ?></td>
                    <td><?php echo $player_name; ?></td>
                    <td><?php echo " : " . timeToString(intval($donnees['time'])); ?></td>
                    <td><?php echo $donnees['difficulty']; ?></td>
                </tr>

            <?php
            }
            ?>

        </table>
    </div>
</body>

</html>

<?php

function stringTimeToThousandths(string $time)
{
    // 0:00:01:523 h:mm:ss:mmm
    $array = explode(':', $time);
    return intval(($array[0]) * 3600 + intval($array[1]) * 60 + intval($array[2])) * 1000 + intval($array[3]);
}

function hasTime($playerId, $imageName, $difficulty, $bdd)
{
    $req = $bdd->prepare('SELECT  * FROM `times` WHERE user_id = :playerId AND image_name = :imageName AND difficulty = :difficulty');
    $req->execute(array('playerId' => $playerId, 'imageName' => $imageName, 'difficulty' => $difficulty));

    return boolval($req->fetch());
}

function bestTime($playerId, $imageName, $difficulty, $bdd)
{
    if (hasTime($playerId, $imageName, $difficulty, $bdd)) {
        $req = $bdd->prepare('SELECT  * FROM `times` WHERE user_id = :playerId AND image_name = :imageName AND difficulty = :difficulty');
        $req->execute(array(
            'playerId' => $playerId,
            'imageName' => $imageName,
            'difficulty' => $difficulty,
        ));
        $bestTime = 0;
        while ($data = $req->fetch()) {
            $time = $data['time'];
            if (!$bestTime || $bestTime > $time) {
                $bestTime = $time;
            }
        }
        return $bestTime;
    }
    return false;
}

function replaceTime($playerId, $imageName, $difficulty, $newTime, $bdd)
{
    $req = $bdd->prepare('UPDATE times SET time = :newTime WHERE user_id = :playerId AND image_name = :imageName AND difficulty = :difficulty');

    $req->execute(array(
        'playerId' => $playerId,
        'imageName' => $imageName,
        'difficulty' => $difficulty,
        'newTime' => $newTime,
    ));
}

function addTime($playerId, $imageName, $difficulty, $time, $bdd)
{
    if (hasTime($playerId, $imageName, $difficulty, $bdd) && $time < bestTime($playerId, $imageName, $difficulty, $bdd)) {
        replaceTime($playerId, $imageName, $difficulty, $time, $bdd);
    } else {
        $req = $bdd->prepare('INSERT INTO times(user_id, image_name, time, difficulty) VALUES(:user_name, :image_name, :time, :difficulty)');

        $req->execute(array(
            'user_name' => $playerId,
            'image_name' => $imageName,
            'time' => $time,
            'difficulty' => $difficulty
        ));
    }
}
