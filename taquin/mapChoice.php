<?php

$firstImageNumber = $_COOKIE['firstImageNumber'];
$lastImageNumber = $_COOKIE['lastImageNumber'];
$typeDePartie = $_COOKIE['typeDePartie'];
$imagesFolder = "public/images/game-images/" . $_COOKIE['imageFolder'] . "/";
// $imagesFolder = 'public/images/game-images/others/';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Choix d'image</title>
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
    <script src="mapChoice.js"></script>
    <script src="functions.js"></script>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="mapChoice.css">
</head>

<body>

    <header>
        <?php
        echo '<a href="index.php">Accueil</a>';
        if ($typeDePartie && $typeDePartie === 'Campagne') {
            echo '<a href="map.php">Changer de région</a>';
        } else {
            echo '<a id="randomImage" href="jeu.php">Image aléatoire</a>';
            echo '<a id="previousImage" href="jeu.php">Image précédente</a>';
        }
        ?>
    </header>

    <ul>
        <?php

        for ($i = $firstImageNumber; $i <= $lastImageNumber; $i++) {
            echo "<li class=\"image-container\">";
            echo "<img src='$imagesFolder$i.jpg' class='image' loading='lazy'>
                    <div class=\"image-name\">$i</div>
                    </li>
                    ";
        }

        ?>
    </ul>

</body>

</html>