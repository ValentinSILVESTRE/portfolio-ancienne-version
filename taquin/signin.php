<?php
require_once('dbConnect.php');
$signinRedirection = 'index.php';
session_start();
$error = false;
$name = '';

// Si on a éssayé de s'inscrire
if (isset($_POST['name']) && isset($_POST['password'])) {
    $name = $_POST['name'];
    $password = $_POST['password'];
    $password_hash = password_hash($password, PASSWORD_DEFAULT);

    // Si les données sont valides
    if (strlen($name) && strlen($password)) {

        // On se connecte à la base de données
        $bdd = dbConnect();

        $req = $bdd->prepare('SELECT `id`, `name`, `password` FROM `user` WHERE `name` = :name ');
        $req->execute([
            'name' => $name,
        ]);

        // Si le nom n'est pas déjà pris
        if (!$req->fetch()) {
            // On ajoute l'utilisateur
            $req = $bdd->prepare('INSERT INTO `user`(`name`, `password`) VALUES (:name, :password)');
            $req->execute([
                'name' => $name,
                'password' => $password_hash,
            ]);
            $_SESSION['name'] = $name;
            header("Location: $signinRedirection");
        } else {
            $error = true;
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription- Taquin</title>
    <link rel="stylesheet" href="log.css">
    <link rel="shortcut icon" href="public/icones/favicon.ico" type="image/x-icon">
</head>

<body>
    <div class="main-content">
        <div class="card">
            <h2 class="card-header">Taquin</h2>

            <form action="" method="post" class="card-content">
                <?php
                if ($error) {
                    echo "<p>$name est déjà pris</p>";
                }
                ?>
                <input type="text" name="name" required placeholder="Pseudonyme">

                <input type="password" name="password" required placeholder="Mot de passe">

                <button type="submit" id="submit-button">Inscription</button>
            </form>
        </div>
        <div class="other">
            <p>Vous avez un compte ?</p>
            <a href="login.php">Connectez-vous</a>
        </div>
    </div>

</body>

</html>