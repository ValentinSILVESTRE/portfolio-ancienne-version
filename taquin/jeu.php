<?php
$typeDePartie = $_COOKIE['typeDePartie'];
$nbJoueurs = 1;

$region = $_COOKIE['region'];
$image_number = $region;

$imageFolder =
    ($typeDePartie === 'Campagne' ?
        'regions' : $_COOKIE['imageFolder']);

$image_src = "public/images/game-images/$imageFolder/$image_number.jpg";


if ($_COOKIE['difficulte'] < 2) {
    setcookie('difficulte', 2);
}
if ($_COOKIE['difficulte'] > 6) {
    setcookie('difficulte', 6);
}

// On traite le formulaire de changement de difficulté
$diff = $_COOKIE['difficulte'];
if (isset($_POST['difficulty'])) {
    $diff = intval($_POST['difficulty']);
    if (2 <= $diff && $diff <= 6) {
        setcookie('difficulte', $diff);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $_COOKIE['region']; ?> - Taquin</title>
    <!-- <link rel="stylesheet" href="jeu.css"> -->
    <link rel="stylesheet" href="jeu.css">
    <link rel="stylesheet" href="chrono.css">
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
</head>

<body>

    <div class="header">
        <?php
        echo '<a href="index.php">Accueil</a>';
        if ($typeDePartie && $typeDePartie === 'Campagne') {
            echo '<a href="map.php">Changer de région</a>';
        } else {
            echo '<a id="boutonChangerImage" href="">Image aléatoire</a>';
            echo '<a href="mapChoice.php">Choisir une image</a>';
        }
        ?>
    </div>

    <img id="image" src='data:image/png;base64, <?php echo base64_encode(file_get_contents("$image_src")); ?>'>

    <img src="public/images/numbers.jpg" class="noDisplay" id="numberImage" alt="">

    <div id="orange">
        <canvas id="canvas-0"></canvas>
        <div id="board1">

            <img id="image" src='data:image/png;base64, <?php echo base64_encode(file_get_contents("$image_src")); ?>'>

        </div>
        <div id="side-bar">
            <img id="img" src="<?php echo $image_src; ?>">
            <div id="displayNumbers">
                <input type="checkbox" name="displayNumbers" id="displayNumbersInput">
                <label for="displayNumbersInput">Numéros</label>
            </div>
            <form action="" class="difficulty-change-form" method="POST">
                <label for="difficulty">Difficulté :</label>
                <input type="number" min="2" max="6" name="difficulty" id="difficulty-input" value="<?php echo $diff; ?>">
                <button type="submit">Valider</button>
            </form>
            <form class="chronoForm" name="chronoForm">
                <div class="formClock">
                    <div class="clock">
                        <div class="clock-face">
                            <div class="hand min-hand"></div>
                            <div class="hand second-hand"></div>
                            <div class="hand miliSecond-hand"></div>
                        </div>
                    </div>
                </div>
                <div class="chrono">
                    <input class="textChrono" type="text" name="chronotime" id="chronotime" value="0:00:00:00" />
                    <input class="stopChrono" type="button" name="startstop" value="start!" onClick="chronoStart()" hidden='true' />
                    <input class="resetChrono" type="button" name="reset" value="reset!" onClick="chronoReset()" hidden='true' />
                </div>
            </form>
        </div>
    </div>
    <script src="jeu.js"></script>
</body>

</html>