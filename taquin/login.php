<?php
session_start();
require_once('dbConnect.php');

$loginRedirection = (isset($_SESSION["previous_page"]) ? $_SESSION["previous_page"] : '../taquin');
$error = false;

// Si on a éssayé de se connecter
if (isset($_POST['name']) && isset($_POST['password'])) {
    $name = $_POST['name'];
    $password = $_POST['password'];

    // On se connecte à la base de données
    $bdd = dbConnect();

    $req = $bdd->prepare('SELECT `id`, `name`, `password` FROM `user` WHERE `name` = :name ');
    $req->execute([
        'name' => $name,
    ]);

    // Si le nom et le mot de passe correspondent alors on se connecte
    $requete = $req->fetch();
    if ($requete) {
        $bddName = $requete['name'];
        $password_hash = $requete['password'];
        if ($name === $bddName && password_verify($password, $password_hash)) {
            $_SESSION['name'] = $name;
            header("Location: $loginRedirection");
        } else {
            $error = true;
        }
    } else {
        $error = true;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion - Taquin</title>
    <link rel="stylesheet" href="log.css">
    <link rel="shortcut icon" href="public/icones/favicon.ico" type="image/x-icon">
</head>

<body>
    <div class="main-content">
        <div class="card">
            <h2 class="card-header">Taquin</h2>

            <form action="" method="post" class="card-content">
                <?php
                if ($error) {
                    echo "<p>Le pseudonyme ou le mot de passe n'est pas bon.</p>";
                }
                ?>
                <input type="text" name="name" required placeholder="Pseudonyme">

                <input type="password" name="password" required placeholder="Mot de passe">

                <button type="submit" id="submit-button">Connexion</button>
            </form>
        </div>

        <div class="other">
            <p>Vous n'avez pas de compte ?</p>
            <a href="signin.php">Inscrivez-vous</a>
        </div>
    </div>
</body>

</html>