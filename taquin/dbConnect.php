<?php

function dbConnect($env = 'local')
{
    //* Base de données locale 
    $localDB = [
        'host' => 'localhost',
        'dbname' => 'taquin',
        'username' => 'root',
        'password' => ''
    ];

    //* Base de données distante
    $remoteDB = [
        'host' => 'db5003825786.hosting-data.io',
        'dbname' => 'dbs3126154',
        'username' => 'dbu1668047',
        'password' => 'i;9()ceA{unf"vHD,TRP'
    ];

    $host = '';
    $dbname = '';
    $username = '';
    $password = '';
    $s = "";
    switch ($env) {
        case 'local':
            $host = $localDB['host'];
            $dbname = $localDB['dbname'];
            $username = $localDB['username'];
            $password = $localDB['password'];
            break;
        case 'remote':
            $host = $remoteDB['host'];
            $dbname = $remoteDB['dbname'];
            $username = $remoteDB['username'];
            $password = $remoteDB['password'];
            break;

        default:
            echo "dbConnect($env) est impossible";
            break;
    }
    try {
        $dbh = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8;", $username, $password);
    } catch (PDOException $exception) {
        die("Erreur connexion à la base de données");
    }
    return $dbh;
}
