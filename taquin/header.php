<?php
if (!isset($_SESSION)) {
    session_start();
}
?>

<header>
    <div class="header-first">
        <a href="../.." class="icon">
            <img src="../../public/favicon.ico" alt="" class="icon-img" />
        </a>
    </div>
    <div class="header-second">
        <div class="h1">Site de Valentin SILVESTRE</div>
        <?php
        if (isset($_SESSION["name"])) {
        ?>
            <a href="logout.php" class="material-icons">
                logout
            </a>
        <?php
        } else {
        ?>
            <a href="login.php" class="material-icons">
                login
            </a>
        <?php
        }
        ?>
    </div>
</header>