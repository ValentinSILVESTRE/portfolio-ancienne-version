document.addEventListener('DOMContentLoaded', () => {
	/** @type{HTMLImageElement} - L'image source */
	const image = document.getElementById('source-image');
	/** @type{HTMLInputElement} - L'input de difficulté */
	const difficultyInput = document.getElementById('difficulty');
	/** @type{HTMLParagraphElement} - La valeur affichée de la difficulté */
	const difficultyOutput = document.getElementById('difficulty-output');

	/** @type{HTMLDivElement} - Div contenant les classes canvas */
	const canvasList = document.getElementById('canvas-list');
	/** @type{number} - Difficulté (nombre de lignes/colonnes) */
	let size = +difficultyInput.value;
	/** @type{Grid} - Grille */
	const grid = new Grid(size);
	/** @type{HTMLDivElement} - Élément de classe game */
	const game = document.getElementsByClassName('game')[0];
	/** @type{HTMLElement} - Aside de classe options */
	const optionsAside = document.getElementsByClassName('options')[0];
	/** @type{HTMLElement} - Aside de classe end-game */
	const endGameAside = document.getElementsByClassName('end-game')[0];

	difficultyOutput.innerText = size;
	difficultyInput.addEventListener('input', () => {
		difficultyOutput.innerText = +difficultyInput.value;
	});

	// ? - Initialisation

	// ? - On attend que l'image se charge
	setTimeout(() => {
		// On mélange la grille
		grid.mix();

		// On définit le nombre de colonnes du canvas
		canvasList.style.gridTemplateColumns = 'auto';
		for (let i = 1; i < size; i++) {
			canvasList.style.gridTemplateColumns += ' auto';
		}

		// On dessine les canvas
		image.style.position = 'static';
		createCanvas(image, canvasList, grid.size);
		draw(grid, canvasList.childNodes);
		image.style.position = 'absolute';

		// ? - Gestion des déplacements par cliques
		/** @type{HTMLCollection} - Collection HTML des éléments de classe canvas-container */
		const canvasContainer = canvasList.getElementsByClassName(
			'canvas-container'
		);

		for (let i = 0; i < canvasContainer.length; i++) {
			const canvas = canvasContainer[i];
			canvas.addEventListener('click', () => {
				// Si la partie n'est pas finie
				if (!grid.isFinish()) {
					grid.moveCell(+canvas.getAttribute('number'));
					draw(grid, canvasList.childNodes);

					// Si on finie l'image
					if (grid.isFinish()) {
						finish(game, optionsAside, endGameAside);
					}
				}
			});
		}

		// ? - Gestion des déplacements au clavier
		document.addEventListener('keydown', (e) => {
			if (
				['ArrowUp', 'ArrowLeft', 'ArrowDown', 'ArrowRight'].includes(
					e.key
				) &&
				!grid.isFinish()
			) {
				const direction = e.key.substring(5).toLowerCase();
				grid.move(direction);
				draw(grid, canvasList.childNodes);

				// Si on finie l'image
				if (grid.isFinish()) {
					finish(game, optionsAside, endGameAside);
				}
			}
		});
	}, 50);

	// ? - On redessine les canvas quand la fenêtre change de taille
	window.addEventListener('resize', () => {
		image.style.position = 'static';
		createCanvas(image, canvasList, grid.size);
		image.style.position = 'absolute';
		draw(grid, canvasList.childNodes);
	});
});
