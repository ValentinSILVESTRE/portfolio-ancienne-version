/**
 * - Créé les canvas et les définis comme enfants de leur container
 * @param {HTMLImageElement} image
 * @param {HTMLDivElement} canvasContainer
 * @param {number} size
 */
function createCanvas(image, canvasContainer, size) {
	canvasContainer.innerHTML = '';
	// On créé les canvas
	for (let iRow = 0; iRow < size; iRow++) {
		for (let iColumn = 0; iColumn < size; iColumn++) {
			const div = document.createElement('div');
			canvasContainer.appendChild(div);
			div.classList.add('canvas-container');
			div.setAttribute('number', iRow * size + iColumn);
			const canvas = document.createElement('canvas');
			div.appendChild(canvas);

			// On masque la case vide
			if (iRow === size - 1 && iColumn === size - 1) {
				canvas.id = 'empty-canvas';
			}

			div.style.height = `${Math.floor(image.height / size)}px`;
			div.style.width = `${Math.floor(image.width / size)}px`;
			canvas.height = image.height / size;
			canvas.width = image.width / size;

			const ctx = canvas.getContext('2d');

			ctx.drawImage(
				image,
				(iColumn * image.naturalWidth) / size,
				(iRow * image.naturalHeight) / size,
				image.naturalWidth / size,
				image.naturalHeight / size,
				0,
				0,
				image.width / size,
				image.height / size
			);
		}
	}
}

/**
 * - Définit l'ordre de chaque canvas dans la liste en fonction de celui qui lui est attribué dans la grille
 * @param {Grid} grid - La grille
 * @param {HTMLCollection} canvasList - La liste des canvas représentants l'image à reconstituer
 */
function draw(grid, canvasList) {
	/** @type{number} - Difficulté de la grille */
	const size = grid.size;

	for (let iRow = 0; iRow < size; iRow++) {
		for (let iColumn = 0; iColumn < size; iColumn++) {
			/** @type{number} - Position du canvas dans la grille */
			const order = iRow * size + iColumn;
			canvasList[grid.grid[iRow][iColumn]].style.order = order;
		}
	}
}

/**
 * - Fait l'affichage de fin de partie
 * @param {HTMLDivElement} game - La div de classe game
 */
function finish(game, optionsAside, endGameAside) {
	game.classList.add('finish');
	document.getElementById('empty-canvas').removeAttribute('id');
	optionsAside.style.display = 'none';
	endGameAside.style.display = 'block';
	console.log(endGameAside);
}
