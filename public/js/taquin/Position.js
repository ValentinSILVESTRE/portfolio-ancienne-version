/**
 * @typedef {Object} Position
 * @property {number} row
 * @property {number} column
 */
