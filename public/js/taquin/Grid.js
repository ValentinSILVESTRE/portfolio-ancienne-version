class Grid {
	/**
	 * @param {number} size - La hauteur et largeure de la grille
	 */
	constructor(size = 3) {
		this.size = size;
		this.grid = this.getInitGrid();

		// Debug
		let a = [0, 1, 2, 3, 4, 5, 6, 7, 8];
		for (let iRow = 0; iRow < size; iRow++) {
			for (let iColumn = 0; iColumn < size; iColumn++) {
				this.grid[iRow][iColumn] = a[iRow * size + iColumn];
			}
		}
	}

	/**
	 * - Renvoie la valeur correspondant à la position donnée
	 * @param {Position} position
	 * @returns {number}
	 */
	getValue(position) {
		if (this.positionIsValid(position)) {
			return this.grid[position.row][position.column];
		} else {
			console.error(`Impossible de faire getValue(${position})`);
		}
	}

	/**
	 * - Définit la valeur correspondant à la position donnée
	 * @param {Position} position
	 * @param {number} value
	 */
	setValue(position, value) {
		if (this.positionIsValid(position) && this.valueIsValid(value)) {
			this.grid[position.row][position.column] = value;
		} else {
			console.error(
				`Impossible de faire setValue() pour les données: `,
				position,
				value
			);
		}
	}

	/**
	 * - Renvoie un tableau à deux dimensions des entiers de 0 à size**2 - 1 triés
	 * @returns {number[][]}
	 */
	getInitGrid() {
		const grid = [];
		for (let iRow = 0; iRow < this.size; iRow++) {
			const line = [];
			for (let iColumn = 0; iColumn < this.size; iColumn++) {
				line.push(this.size * iRow + iColumn);
			}
			grid.push(line);
		}
		return grid;
	}

	/**
	 * - Renvoie vrai si aucune cases adjacentes ne doit l'être au final
	 * @returns {Boolean}
	 */
	isPerfectlyMixed() {
		// Ligne
		for (let iRow = 0; iRow < this.size; iRow++) {
			for (let iColumn = 0; iColumn < this.size - 1; iColumn++) {
				const caseValue = this.getValue({ row: iRow, column: iColumn });
				const rightValue = this.getValue({
					row: iRow,
					column: iColumn + 1,
				});
				if (
					caseValue + 1 === rightValue &&
					rightValue % this.size !== 0
				) {
					return false;
				}
			}
		}

		// Colonne
		for (let iRow = 0; iRow < this.size - 1; iRow++) {
			for (let iColumn = 0; iColumn < this.size; iColumn++) {
				const caseValue = this.getValue({ row: iRow, column: iColumn });
				const bottomValue = this.getValue({
					row: iRow + 1,
					column: iColumn,
				});
				if (caseValue + this.size === bottomValue) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * - Fonction qui mélange la grille, en laissant le dernier élément à la fin
	 */
	mix() {
		if (this.size === 2) {
			const randomBoolean = Boolean(Math.floor(Math.random() * 2));

			if (randomBoolean) {
				this.grid = [
					[1, 2],
					[0, 3],
				];
			} else {
				this.grid = [
					[2, 0],
					[1, 3],
				];
			}
		} else {
			// On mélange tant que la grille n'est pas solvable ou est déjà finie
			do {
				/** @type {number[]} - Liste triée des entiers de 0 à n*n - 1 */
				let integers = [];
				for (let i = 0; i < this.size ** 2 - 1; i++) {
					integers.push(i);
				}

				/** @type {number[]} - Liste mélangées des entiers */
				const unorderedIntegers = [];

				// Tant que la liste triée n'est pas vide, on sors un entier aléatoirement qu'on met à la fin de la liste mélangée
				while (integers.length > 0) {
					const i = Math.floor(Math.random() * integers.length);
					unorderedIntegers.push(integers.splice(i, 1)[0]);
				}

				// On met le dernier entier à la fin
				unorderedIntegers.push(this.size ** 2 - 1);

				// On place les élément mélangés dans la grille
				for (let iRow = 0; iRow < this.size; iRow++) {
					for (let iColumn = 0; iColumn < this.size; iColumn++) {
						this.setValue(
							{ row: iRow, column: iColumn },
							unorderedIntegers[iRow * this.size + iColumn]
						);
					}
				}
			} while (
				!this.isSolvable() ||
				this.isFinish() ||
				!this.isPerfectlyMixed()
			);
		}
	}

	/**
	 * - Renvoie vrai si la grille est totalement triée, faux sinon
	 * @returns {Boolean}
	 */
	isFinish() {
		for (let iRow = 0; iRow < this.size; iRow++) {
			for (let iColumn = 0; iColumn < this.size; iColumn++) {
				if (
					iRow * this.size + iColumn !==
					this.getValue({ row: iRow, column: iColumn })
				) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * - Renvoie vrai si la grille est solvable, sont niveau de mélange est pair
	 * @returns {Boolean}
	 */
	isSolvable() {
		return this.mixinLevel() % 2 === 0;
	}

	/**
	 *  - Renvoie le niveau de mélange de la grille (nombre de paires inversées)
	 * @returns {number}
	 */
	mixinLevel() {
		let res = 0;
		// Pour chaque paire d'éléments (i,j) inversés on ajoute 1 au résultat
		for (let i = 0; i < this.size ** 2 - 1; i++) {
			for (let j = i + 1; j < this.size ** 2; j++) {
				if (!this.sontOrdonnees(i, j)) {
					res++;
				}
			}
		}
		return res;
	}

	/**
	 *  - renvoie vrai si i et j sont dans le bon ordre, sinon faux
	 * @param {number} i
	 * @param {number} j
	 * @returns {boolean} renvoie vrai si i et j sont dans le bon ordre, sinon faux
	 */
	sontOrdonnees(i, j) {
		if (0 <= i && i < this.size ** 2 && 0 <= j && j < this.size ** 2) {
			if (j < i) {
				return this.sontOrdonnees(j, i);
			}
			// ici i <= j
			return this.pos(i) <= this.pos(j);
		}
	}

	/**
	 * - Renvoie la position de i dans la grille (entre 0 et n²-1)
	 * @param {number} i - valeur recherchée dans la grille (entre 0 et n²-1)
	 * @returns {number} position de i dans la grille (entre 0 et n²-1)
	 */
	pos(i) {
		if (0 <= i && i < this.size ** 2) {
			for (let iRow = 0; iRow < this.size; iRow++) {
				for (let iColumn = 0; iColumn < this.size; iColumn++) {
					if (this.getValue({ row: iRow, column: iColumn }) === i) {
						return iRow * this.size + iColumn;
					}
				}
			}
		}
	}

	/**
	 * - Échange les éléments de place dans la grille
	 * @param {Position} positionA
	 * @param {Position} positionB
	 */
	switch(positionA, positionB) {
		if (
			this.positionIsValid(positionA) &&
			this.positionIsValid(positionB)
		) {
			const valueA = this.getValue(positionA);
			this.setValue(positionA, this.getValue(positionB));
			this.setValue(positionB, valueA);
		}
	}

	/**
	 * - Renvoie vrai si la position est valide (row et column compris entre 0 et size), faux sinon
	 * @param {Position} position
	 * @returns {Boolean}
	 */
	positionIsValid(position) {
		return (
			0 <= position.row &&
			position.row < this.size &&
			0 <= position.column &&
			position.column < this.size
		);
	}

	/**
	 * - Renvoie vrai si la position est valide (row et column compris entre 0 et size ** 2 - 1), faux sinon
	 * @param {Position} position
	 * @returns {Boolean}
	 */
	valueIsValid(value) {
		return 0 <= value && value < this.size ** 2;
	}

	/**
	 * - Renvoie la valeur de la case vide (size**2 - 1)
	 * @returns {number}
	 */
	getEmptyCaseValue() {
		return this.size ** 2 - 1;
	}

	/**
	 * - Renvoie la position de la case (vide par défaut) recherchée
	 * @param {number} value - Valeur recherchée, par défaut la case vide (8 pour une grille 3×3)
	 * @returns {Position}
	 */
	getPosition(value = this.getEmptyCaseValue()) {
		for (let iRow = 0; iRow < this.size; iRow++) {
			for (let iColumn = 0; iColumn < this.size; iColumn++) {
				const position = { row: iRow, column: iColumn };
				if (this.getValue(position) === value) {
					return position;
				}
			}
		}
	}

	/**
	 * - Déplace la case vide vers la direction demandée
	 * @param {string} direction - up, down, left or right
	 */
	move(direction) {
		/** @type {Position} - Position de la case vide */
		const emptyCasePosition = this.getPosition();

		switch (direction) {
			case 'up':
				if (this.getPosition().row > 0) {
					this.switch(emptyCasePosition, {
						row: emptyCasePosition.row - 1,
						column: emptyCasePosition.column,
					});
				}
				break;

			case 'down':
				if (this.getPosition().row < this.size - 1) {
					this.switch(emptyCasePosition, {
						row: emptyCasePosition.row + 1,
						column: emptyCasePosition.column,
					});
				}
				break;

			case 'left':
				if (this.getPosition().column > 0) {
					this.switch(emptyCasePosition, {
						row: emptyCasePosition.row,
						column: emptyCasePosition.column - 1,
					});
				}
				break;

			case 'right':
				if (this.getPosition().column < this.size - 1) {
					this.switch(emptyCasePosition, {
						row: emptyCasePosition.row,
						column: emptyCasePosition.column + 1,
					});
				}
				break;

			default:
				console.error(`Impossible de faire move(${direction})`);
				break;
		}
	}

	moveCell(number) {
		const cellPosition = this.getPosition(number);
		const emptyPosition = this.getPosition();

		// S'il ne s'agit pas de la case vide
		if (number !== this.size ** 2 - 1) {
			// Si la case est sur la même ligne
			if (cellPosition.row === emptyPosition.row) {
				// Si la case est à droite de la case vide
				if (emptyPosition.column < cellPosition.column) {
					// On échange la case vide avec toutes celles entre les deux, jusqu'à la case
					for (
						let iColumn = emptyPosition.column + 1;
						iColumn <= cellPosition.column;
						iColumn++
					) {
						const position = {
							row: emptyPosition.row,
							column: iColumn,
						};
						this.switch(this.getPosition(), position);
					}
				}

				// Si la case est à gauche de la case vide
				else {
					// On échange la case vide avec toutes celles entre les deux, jusqu'à la case
					for (
						let iColumn = emptyPosition.column - 1;
						iColumn >= cellPosition.column;
						iColumn--
					) {
						const position = {
							row: emptyPosition.row,
							column: iColumn,
						};
						this.switch(this.getPosition(), position);
					}
				}
			}

			// Si la case est sur la même colonne
			else if (cellPosition.column === emptyPosition.column) {
				// Si la case est en dessous de la case vide
				if (emptyPosition.row < cellPosition.row) {
					// On échange la case vide avec toutes celles entre les deux, jusqu'à la case
					for (
						let iRow = emptyPosition.row + 1;
						iRow <= cellPosition.row;
						iRow++
					) {
						const position = {
							row: iRow,
							column: emptyPosition.column,
						};
						this.switch(this.getPosition(), position);
					}
				}

				// Si la case est au dessus de la case vide
				else {
					// On échange la case vide avec toutes celles entre les deux, jusqu'à la case
					for (
						let iRow = emptyPosition.row - 1;
						iRow >= cellPosition.row;
						iRow--
					) {
						const position = {
							row: iRow,
							column: emptyPosition.column,
						};
						this.switch(this.getPosition(), position);
					}
				}
			}
		}
	}
}
