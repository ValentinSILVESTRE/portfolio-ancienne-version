/**
 * - Joue le son
 * @param {String} url - lien du son
 * @param {number|null} duration - durée du son à jouer
 * @param {number} startAt - nombre de secondes à ignorer avant de jouer le son
 */
function playSound(url, duration = null, startAt = 0) {
	const audio = new Audio(url);
	audio.addEventListener('loadeddata', () => {
		if (0 <= startAt && startAt < audio.duration) {
			audio.currentTime = startAt;
		}
		console.log(audio.volume);
		audio.volume = 0.1;
		audio.play();

		if (duration > 0 && startAt + duration <= audio.duration) {
			setInterval(() => {
				audio.pause();
			}, 1000 * duration);
		}
	});
}

/**
 * - Renvoie un nombre entier aléatoire compris dans [min, max]
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function random(min, max) {
	const gap = Math.floor(+max) - Math.floor(+min) + 1;
	const r = Math.floor(Math.random() * gap);
	return r + Math.floor(+min);
}

/**
 * - Renvoie une nouvelle chaîne de caractère avec une majuscule au début
 * @param {String} string - Chaîne de caractères
 * @returns {String}
 */
function capitalize(string) {
	let res = string;
	let t = res.split('');
	t[0] = t[0].toUpperCase();
	res = t.join('');
	return res;
}
