document.addEventListener('DOMContentLoaded', () => {
	if (+getCookie('difficulte') < 2) {
		setCookie('difficulte', 2);
	}
	if (+getCookie('difficulte') > 6) {
		setCookie('difficulte', 6);
	}
	let difficulty = +getCookie('difficulte');
	const scoreTables = document.getElementsByClassName('score-table');
	const difficultyButtons = document.getElementsByClassName(
		'difficulty-button'
	);
	let activeDifficulty = difficulty;
	const playButton = document.getElementById('play-button');
	const difficultyInput = document.getElementById('difficulty-input');

	// On affiche les temps de la difficulté actuelle
	for (const scoreTable of scoreTables) {
		scoreTable.classList.add('hidden');
		if (scoreTable.classList.contains(activeDifficulty)) {
			scoreTable.classList.remove('hidden');
		}
	}

	for (const difficultyButton of difficultyButtons) {
		difficultyButton.addEventListener('click', () => {
			// On récupère la difficulté choisie
			for (let i = 2; i <= 6; i++) {
				if (difficultyButton.classList.contains(i)) {
					activeDifficulty = i;
				}
			}

			// On affiche les temps de la difficulté actuelle
			for (const scoreTable of scoreTables) {
				scoreTable.classList.add('hidden');
				if (scoreTable.classList.contains(activeDifficulty)) {
					scoreTable.classList.remove('hidden');
				}
			}
		});
	}

	difficultyInput.addEventListener('change', () => {
		let d = difficultyInput.value;
		console.log(d);
	});

	playButton.addEventListener('click', () => {
		setCookie('difficulte', +difficultyInput.value);
	});
});
