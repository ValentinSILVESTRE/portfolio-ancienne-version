document.addEventListener('DOMContentLoaded', () => {
	const regions = [
		'Hauts-de-France',
		'Normandie',
		'Île-de-France',
		'Grand Est',
		'Bretagne',
		'Pays de la Loire',
		'Centre-Val de Loire',
		'Bourgogne-Franche-Comté',
		'Nouvelle-Aquitaine',
		'Auvergne-Rhône-Alpes',
		'Occitanie',
		"Provence-Alpes-Côte d'Azur",
		'Corse',
	];

	const lettres = [
		'A',
		'B',
		'C',
		'D',
		'E',
		'F',
		'G',
		'H',
		'I',
		'J',
		'K',
		'L',
		'M',
	];

	// On récupère les cookies de temps
	regions.forEach((region) => {
		// S'il y a un cookie avec le temps de la région
		if (getCookie(region)) {
			const letter = regionToLetter(region);
			const nouveauTemps = properTime2(getCookie(region));
			// On récupère le temps de la région (2nd p)
			// S'il y a déjà un temps d'écrit
			console.log(document.getElementById(`list-${letter}`));
			console.log(document.getElementById(`list-${letter}`).innerHTML);
			if (
				document.getElementById(`list-${letter}`).innerHTML[0].isInteger
			) {
				const ancientTemps = properTime2(
					document.getElementById(`list-${letter}`).children.item(1)
						.innerHTML
				);
				console.log(`
                ancientTemps: ${ancientTemps}
                nouveauTemps: ${nouveauTemps}
                `);
				if (tempsPlusPetit(nouveauTemps, ancientTemps)) {
					document
						.getElementById(`list-${letter}`)
						.children.item(1).innerHTML = ` : ${nouveauTemps}`;
				}
			} else {
				const tempsRegion = document.createElement('p');
				tempsRegion.innerHTML = ` : ${nouveauTemps}`;
				document
					.getElementById(`list-${letter}`)
					.appendChild(tempsRegion);
			}
		}
	});

	/** - Div dont l'id est map */
	let map = document.getElementById('map');

	/** - Liste des path */
	let paths = map.getElementsByClassName('path');

	// Gestion des cliques sur les régions
	for (let path of paths) {
		path.addEventListener('mouseenter', function (e) {
			let id = e.target.id.replace('region-', '');
			map.querySelectorAll('.is-active').forEach(function (item) {
				item.classList.remove('is-active');
			});
			document.querySelector('#list-' + id).classList.add('is-active');
			// document.querySelector('region-'+ id).classList.add('is-active')
		});

		path.addEventListener('mouseleave', (e) => {
			let id = e.target.id.replace('region-', '');
			map.querySelectorAll('.is-active').forEach(function (item) {
				item.classList.remove('is-active');
			});
			document.querySelector('#list-' + id).classList.remove('is-active');
		});

		// Quand on clique sur une région
		path.addEventListener('click', function (e) {
			/** - La lettre correspondant à la région */
			let id = e.target.id.replace('region-', '');

			// Si l'id est plus long qu'un seul caractère c'est qu'on a cliqué sur un smiley
			if (id.length > 1) {
				// On modifie donc l'id
				id = id[id.length - 1];
			}
			console.log(`Click région ${id}`);
			redirectToPuzzle(id);
		});
	}

	// Fonctions ---------------------------------------------------------------------------------------------

	function properTime2(temps) {
		// h:mm:ss:mmm
		const heures = +temps.substr(0, 1);
		const minutes = +temps.substr(2, 2);
		const secondes = +temps.substr(5, 2);
		const milliemes = +temps.substr(8, 2);
		let res = '';
		if (heures) {
			return heures + 'h' + (minutes ? minutes : '00');
		}
		if (minutes) {
			return minutes + "'" + (secondes ? secondes : '00');
		}
		return secondes + '"' + (milliemes ? milliemes : '00');
	}

	function redirectToPuzzle(id) {
		const path = document.getElementById(`region-${id}`);
		const title = path.getAttribute('title');
		console.log(`title: ${title}`);
		document.cookie += `region=${title};`;
		setCookie('region', title);
		console.log(`Les cookies: ${document.cookie}`);
		console.log(`getCookie('nbJoueurs'): ${getCookie('nbJoueurs')}`);
		window.location.replace('./jeu.php');
	}

	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}

	function setCookie(cname, cvalue) {
		document.cookie = cname + '=' + cvalue + ';';
	}

	regions.forEach((region) => {
		if (getCookie(region) && getCookie(region) !== '0:00:00:000') {
			console.log(region);
			// Pour chaque path
			lettres.forEach((lettre) => {
				if (
					document
						.getElementById(`region-${lettre}`)
						.getAttribute('title') === region
				) {
					document
						.getElementById(`region-${lettre}`)
						.classList.add('finie');
				}
			});
		}
	});

	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return null;
	}

	function setCookie(cname, cvalue) {
		document.cookie = cname + '=' + cvalue + ';';
	}

	function regionToLetter(region) {
		switch (region) {
			case 'Hauts-de-France':
				return 'A';
			case 'Normandie':
				return 'B';
			case 'Île-de-France':
				return 'C';
			case 'Grand Est':
				return 'D';
			case 'Bretagne':
				return 'E';
			case 'Pays de la Loire':
				return 'F';
			case 'Centre-Val de Loire':
				return 'G';
			case 'Bourgogne-Franche-Comté':
				return 'H';
			case 'Nouvelle-Aquitaine':
				return 'I';
			case 'Auvergne-Rhône-Alpes':
				return 'J';
			case 'Occitanie':
				return 'K';
			case "Provence-Alpes-Côte d'Azur":
				return 'L';
			case 'Corse':
				return 'M';
			default:
		}
	}

	function estFini(region) {
		if (!getCookie(region)) {
			return false;
		}
		return (
			getCookie(region).length === 11 &&
			getCookie(region) !== '0:00:00:000' &&
			getCookie(region) != null
		);
	}

	function carteFinie() {
		let m = 0;
		regions.forEach((region) => {
			if (!estFini(region)) {
				return false;
			} else {
				m++;
			}
		});
		return m === 13;
	}

	if (carteFinie()) {
		document.getElementsByTagName('h1')[0].innerHTML =
			'Bravo vous avez terminé la campagne  !';
	}
});
