document.addEventListener('DOMContentLoaded', () => {
	const nbJoueurs = +getCookie('nbJoueurs');
	const buttonAutreImage = document.getElementById('autreImage');
	const buttonAutreRegion = document.getElementById('autreRegion');
	const image = document.getElementById('image');
	const submitButton = document.getElementById('submitButton');
	const imageName = getCookie('region');

	const min = +getCookie('firstImageNumber');
	const max = +getCookie('lastImageNumber');

	getImagePath(image);

	buttonAutreImage.addEventListener('click', () => {
		let newImageName;
		do {
			newImageName = randomInt(min, max);
		} while (newImageName === imageName);
		setCookie('region', newImageName);
	});

	document.addEventListener('keydown', (event) => {
		if (event.code === 'Escape') {
			deletePopup();
		}
	});

	if (buttonAutreRegion) {
		buttonAutreRegion.addEventListener('click', () => {
			window.location.href = './map.php';
		});
	}
});

function createPopup() {
	if (!document.getElementById('popup')) {
		const buttonAutreImage = document.getElementById('autreImage');
		let folder = +getCookie('nbJoueurs') === 1 ? 'other' : 'imag';

		const popup = document.createElement('div');
		popup.id = 'popup';

		const difficultyInput = document.createElement('input');
		difficultyInput.id = 'difficulty';
		difficultyInput.setAttribute('type', 'number');
		difficultyInput.setAttribute('name', 'difficulty');
		difficultyInput.setAttribute('min', 2);
		difficultyInput.setAttribute('max', 6);
		difficultyInput.value = +getCookie('difficulte');

		const difficultyLabel = document.createElement('label');
		difficultyLabel.setAttribute('for', 'difficulty');
		difficultyLabel.innerHTML = 'Difficulté';

		const folderSelect = document.createElement('select');
		folderSelect.id = 'folder';

		const otherOption = document.createElement('option');
		otherOption.value = 'other';
		otherOption.innerHTML = 'other';

		const imagOption = document.createElement('option');
		imagOption.value = 'imag';
		imagOption.innerHTML = 'imag';

		if (folder === 'other') {
			otherOption.setAttribute('selected', true);
		} else {
			imagOption.setAttribute('selected', true);
		}

		folderSelect.appendChild(otherOption);
		folderSelect.appendChild(imagOption);

		const folderLabel = document.createElement('label');
		folderLabel.setAttribute('for', 'folder');
		folderLabel.innerHTML = 'Dossier';

		const submitButton = document.createElement('button');
		submitButton.id = 'submitButton';
		submitButton.setAttribute('type', 'submit');
		submitButton.textContent = 'Valider';

		submitButton.addEventListener('click', () => {
			// On modifie les cookies
			setCookie('difficulte', difficultyInput.value);
			setCookie('nbJoueurs', folderSelect.value === 'other' ? 1 : 2);

			const imageId = getCookie('region');
			let newImageId = imageId;

			const nbJoueurs = +getCookie('nbJoueurs');
			const min =
				nbJoueurs === 1
					? +getCookie('firstImageNumber')
					: +getCookie('firstImageBisNumber');
			const max =
				nbJoueurs === 1
					? +getCookie('lastImageNumber')
					: +getCookie('lastImageBisNumber');

			while (newImageId === imageId) {
				newImageId = randomInt(min, max);
			}
			setCookie('region', newImageId);

			// On redirige vers jeu.php
			window.location.href = 'jeu.php';
		});

		const rect = buttonAutreImage.getBoundingClientRect();

		popup.style.top = `${rect.bottom}px`;
		popup.style.left = `${rect.left}px`;

		popup.appendChild(difficultyLabel);
		popup.appendChild(difficultyInput);
		popup.appendChild(folderLabel);
		popup.appendChild(folderSelect);
		popup.appendChild(submitButton);
		document.body.appendChild(popup);
	}
}

function deletePopup() {
	if ((popup = document.getElementById('popup'))) {
		popup.remove();
	}
}

function randomInt(min, max) {
	if (min > max) {
		return randomInt(max, min);
	}
	const gap = max - min;
	return Math.floor(Math.random() * (gap + 1) + min);
}
