// COOKIES ---------------------------------------------------------------------------------------

function getCookie(cname) {
	let name = cname + '=';
	let ca = document.cookie.split(';');
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

function setCookie(cname, cvalue) {
	document.cookie = cname + '=' + cvalue + ';';
}

function finDePartie() {
	const region = getCookie('region');
	const chronotime = document.getElementById('chronotime');
	const temps = chronotime.value;
	const heures = +temps.substr(0, 1);
	const minutes = +temps.substr(2, 2);
	const secondes = +temps.substr(5, 2);
	const milliemes = +temps.substr(8, 3);

	document.cookie = `lastTime=${temps}`;

	orange.innerHTML = '';
	orange.appendChild(divFinDePartie(temps));

	// Si le cookie region existe et est plus petit on ne fait rien
	if (getCookie(region)) {
		const ancienTemps = properTime2(getCookie(region));
		const nouveauTemps = properTime2(temps);
		if (tempsPlusPetit(nouveauTemps, ancienTemps)) {
			setCookie(region, temps);
		}
	} else {
		setCookie(region, temps);
	}

	// On ajoute un bouton Scores dans le header
	const scoresButton = document.createElement('a');
	scoresButton.id = 'scores';
	scoresButton.innerHTML = 'scores';
	scoresButton.href = 'scores.php';
	document.getElementsByClassName('header')[0].appendChild(scoresButton);
}

function properTime(temps) {
	const t = properTime2(temps);
	let res = t.replace('h', ' heures ');
	res = res.replace("'", ' minutes ');
	return (res = res.replace('"', ' secondes '));
}

function properTime2(temps) {
	// h:mm:ss:mmm
	// temps = '0:00:02:015';
	const heures = +temps.substr(0, 1);
	const minutes = +temps.substr(2, 2);
	const secondes = +temps.substr(5, 2);
	const milliemes = +temps.substr(8, 2);
	let res = '';
	if (heures) {
		return (
			heures +
			'h' +
			(minutes ? (minutes >= 10 ? minutes : '0' + minutes) : '00')
		);
	}
	if (minutes) {
		return (
			minutes +
			"'" +
			(secondes ? (secondes >= 10 ? secondes : '0' + secondes) : '00')
		);
	}
	return (
		secondes +
		'"' +
		(milliemes ? (milliemes >= 10 ? milliemes : '0' + milliemes) : '00')
	);
}

function divFinDePartie(temps) {
	// temps = '1:26:35:455';
	const image = document.getElementById('image');
	const newDiv = document.createElement('div');
	newDiv.id = 'lastDiv';
	newDiv.appendChild(image);
	image.style.display = 'block';

	if ((region = 'Bretagne')) {
		image.style.height = '' + 0.6 * window.innerHeight + 'px';
		const newP = document.createElement('P');
		newP.setAttribute('id', 'texte-fin-de-partie');
		newP.innerHTML = `Bravo, vous avez résolu l'image en ${properTime(
			temps
		)}`;
		newDiv.appendChild(newP);
		return newDiv;
	}
	if (image.width > image.height) {
		image.style.width = '' + 0.6 * window.innerWidth + 'px';
	} else {
		image.style.height = '' + 0.6 * window.innerHeight + 'px';
	}
	if ((region = 'Pays de la Loire')) {
		image.style.width = '' + 0.4 * window.innerWidth + 'px';
	}
	const newP = document.createElement('P');
	newP.innerHTML = `Bravo, vous avez résolu l'image en ${properTime2(temps)}`;
	newDiv.appendChild(newP);

	return newDiv;
}

function getDivScores() {
	// function getDivScores(imageName, difficulty) {
	const newDiv = document.createElement('div');
	let score = '';
}

function getScores() {
	return fetch('public/scores/Bretagne-facile.txt')
		.then((response) => response.text())
		.then((text) => {
			return text;
		});
}

function tempsPlusPetit(temps1, temps2) {
	return timeToSeconds(temps1) <= timeToSeconds(temps2);
}

/**
 * Convertit un temps donné string par le html en nombre de secondes
 * @param {string} temps - forme : 8h12 ou 24'58 ou 54"98
 * @returns {number}
 */
function timeToSeconds(temps) {
	const n = temps.length;
	// Il faut la position de lunit" de temps (h, ', ou ")
	const unites = ['h', "'", '"'];
	let unite;
	let partieEntiere;
	let partieDecimale;
	let indiceUnite;
	for (let i = 0; i < n; i++) {
		if (unites.includes(temps[i])) {
			unite = temps[i];
			indiceUnite = i;
		}
	}
	partieEntiere = +temps.substring(0, indiceUnite);
	partieDecimale = +temps.substring(indiceUnite + 1, n);
	switch (unite) {
		case 'h':
			return 3600 * partieEntiere + 60 * partieDecimale;
		case "'":
			return 60 * partieEntiere + partieDecimale;
		case '"':
			if (partieDecimale < 10) {
				return partieEntiere + partieDecimale / 10;
			}
			return partieEntiere + partieDecimale / 100;

		default:
			break;
	}
}

function randomInt(min, max) {
	if (min > max) {
		return randomInt(max, min);
	}
	const gap = max - min;
	return Math.floor(Math.random() * (gap + 1) + min);
}

/**
 * - If img src is "public/images/game-images/imag/9.jpg" return "imag/9.jpg"
 * @param {HTMLElement} img
 */
function getImagePath(img) {
	return img.src.split('/game-images/')[1];
}

function getImageId(img) {
	const imagePath = getImagePath(img);
	let array = imagePath.split('/');
	let res = null;
	array.forEach((elem) => {
		if (elem.includes('jpg')) {
			res = elem.split('.')[0];
		}
	});
	return res;
}
