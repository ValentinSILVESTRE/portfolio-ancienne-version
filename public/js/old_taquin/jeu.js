// On attend le chargement de la page
document.addEventListener('DOMContentLoaded', () => {
	if (+getCookie('difficulte') < 2) {
		setCookie('difficulte', 2);
	}
	if (+getCookie('difficulte') > 6) {
		setCookie('difficulte', 6);
	}
	const imageName = getCookie('region');
	const firstImageNumber = +getCookie('firstImageNumber');
	const lastImageNumber = +getCookie('lastImageNumber');
	const displayNumbers = document.getElementById('displayNumbersInput');
	/**
	 * @constant Le temps d'attente en millisecondes avant d'afficher l'image
	 * @type {number}
	 */
	const TIMEOUT = 10;

	const img = document.getElementById('img');
	const board1 = document.getElementById('board1');
	const image = document.getElementById('image');
	const numberImage = document.getElementById('numberImage');

	/** La longueure de la grille: nombre de cases par ligne et colonne */
	let longueure = +getCookie('difficulte');
	const directions = ['haut', 'bas', 'gauche', 'droite'];
	let canvaList;

	// On attend le chargement de l'image puis on l'affiche
	setTimeout(() => {
		if (img.width > img.height) {
			setCookie('format', 'paysage');
			format = 'paysage';
		} else {
			setCookie('format', 'portrait');
			format = 'portrait';
		}
		dessineCanvas();
		// Il faut trier les éléments de fragments dans l'ordre de la grille
		afficherCanvas();
		triFragments();
		afficherCanvas();
		canvaList = board1.getElementsByClassName('canvas');

		// ? Gestion des cliques
		for (const canva of canvaList) {
			if (canva.classList.contains('vide')) {
				caseVide = canva;
			}
			canva.addEventListener('click', () => {
				// Si la partie n'a pas encore démarée, on la démarre
				if (!partieEnCours) {
					partieEnCours = true;
					chronoStart();
				}
				// On récupère la position de la case
				const position = getCanvaPositionById(canva.id);
				// Si elle est adjacente à la case vide, alors  on les échange
				deplacerCaseVide(position[0], position[1]);
				// On traite la fin de partie
				// On actualise l'image
				afficherCanvas();

				// On exécute imageEstResolue()
				imageEstResolue();
				// Si l'image est terminée, on la remplie entièrement
				if (imageEstResolue()) {
					dessineCanvas();
					afficherCanvas();
					finDePartie();
				}
			});
		}

		chronoReset();

		// ? Gestion du clic et déplacement de la case vide
		// caseVide.onmousedown = console.log(caseVide);

		// * Affichage des numéros
		displayNumbers.addEventListener('change', () => {
			if (displayNumbers.checked) {
				for (let i = 1; i < longueure ** 2; i++) {
					canva = document.getElementById(`canvas-${i}`);
					canva.classList.add('number');
				}
				printNumbers();
			} else {
				dessineCanvas();
				afficherCanvas();
				triFragments();
				afficherCanvas();
				for (let i = 1; i < longueure ** 2; i++) {
					canva = document.getElementById(`canvas-${i}`);
					canva.classList.remove('number');
				}
			}
		});
		image.style.display = 'none';
	}, TIMEOUT);

	// Logique -------------------------------------------------------------------------------------------

	/** - Grille de nombres, grille[ligne][colonne] */
	let grille = Array();

	// On initialise la grille dans l'ordre 123, 456, 789
	for (let ligneIndex = 0; ligneIndex < longueure; ligneIndex++) {
		const ligne = Array();
		for (let colonneIndex = 0; colonneIndex < longueure; colonneIndex++) {
			ligne.push(longueure * ligneIndex + colonneIndex + 1);
		}
		grille.push(ligne);
	}

	/** @type{number} : n = longueure pour simplifier l'écriture */
	const n = longueure;

	do {
		melange(grille);
	} while (!estSolvable() || !melangeEstParfait());

	// Variable qui sert à lancer le chronomètre
	let partieEnCours = false;

	// Affichage -----------------------------------------------------------------------------------------

	// On récupère l'image entière, et le canvas
	const canvas = document.getElementById('canvas-0');

	// On récupère les dimension de la div orange
	const orange = document.getElementById('orange');
	const largeurOrange = orange.clientWidth;
	const hauteurOrange = orange.clientHeight;

	// On récupère les dimension de l'image
	let largeurImage = image.width;
	let hauteurImage = image.height;

	// On définit les dimensions du canvas en fonction de celles de l'image
	canvas.width = largeurImage;
	canvas.height = hauteurImage;

	// On récupère les dimension du canvas
	const largeurCanvas = canvas.width;
	const hauteurCanvas = canvas.height;

	const ctx = canvas.getContext('2d');

	// Au chargement de l'image on dessine le canvas-0
	image.addEventListener('load', () => {
		// ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
		ctx.drawImage(
			image,
			0,
			0,
			largeurImage,
			hauteurImage,
			0,
			0,
			largeurCanvas,
			hauteurCanvas
		);
	});

	// On lui donne les dimension de l'image
	board1.width = 0.6 * window.innerWidth;
	board1.height = 0.6 * window.innerHeight;

	// On créé la liste des fragments d'image
	let fragments = Array();
	let largeurFragment = board1.width / longueure;
	let hauteurFragment = board1.height / longueure;

	dessineCanvas();
	afficherCanvas();
	// Il faut trier les éléments de fragments dans l'ordre de la grille
	triFragments();
	afficherCanvas();

	imageEstResolue();

	// ? Gestions des touches directionnellesdocument.onkeydown = checkKey;
	document.onkeydown = reactionDirection;

	// ? Boutton changer d'image
	if (document.getElementById('boutonChangerImage')) {
		document
			.getElementById('boutonChangerImage')
			.addEventListener('click', () => {
				let newImageName;
				do {
					newImageName = randomInt(firstImageNumber, lastImageNumber);
				} while (newImageName === imageName);
				console.log(firstImageNumber, lastImageNumber);
				setCookie('region', newImageName);
				setCookie('aze', newImageName);
			});
	}

	redimensionnement();
	image.style.display = 'none';

	// FONCTIONS ---------------------------------------------------------------------------------------------------------------

	/**
	 * - Fonction qui dessine le canvas dans le tableau fragments, créé et ordonne le tableau fragments
	 */
	function dessineCanvas() {
		fragments = Array();
		for (let ligneIndex = 0; ligneIndex < longueure; ligneIndex++) {
			const ligne = Array();
			for (
				let colonneIndex = 0;
				colonneIndex < longueure;
				colonneIndex++
			) {
				// On créé le canvas, on définit son id, ses dimensions
				let _canvas = document.createElement('canvas');
				const id = longueure * ligneIndex + colonneIndex + 1;
				_canvas.id = `canvas-${id}`;
				_canvas.classList.add('canvas');
				_canvas.width = largeurFragment;
				_canvas.height = hauteurFragment;

				largeurImage = document.getElementById('image').width;
				hauteurImage = document.getElementById('image').height;

				// On calcule la position dans l'image (sx, sy)
				let sx =
					(colonneIndex * document.getElementById('image').width) /
					longueure;
				let sy =
					(ligneIndex * document.getElementById('image').height) /
					longueure;

				// On remplit le canvas
				const ctx = _canvas.getContext('2d');

				// Si ce n'est pas le dernier canvas, alors on dessine l'image, sinon on lui ajoute la classe 'vide'
				if (
					!(
						ligneIndex === longueure - 1 &&
						colonneIndex === longueure - 1
					)
				) {
					if (displayNumbers.checked) {
						_canvas.classList.add('number');
						const values = getNumberImage(id);
						ctx.drawImage(
							numberImage,
							values[0],
							values[1],
							values[2],
							values[3],
							0,
							0,
							largeurFragment,
							hauteurFragment
						);
					} else {
						ctx.drawImage(
							image,
							sx,
							sy,
							largeurImage / longueure,
							hauteurImage / longueure,
							0,
							0,
							largeurFragment,
							hauteurFragment
						);
					}
				} else {
					if (imageEstResolue()) {
						ctx.drawImage(
							image,
							sx,
							sy,
							largeurImage / longueure,
							hauteurImage / longueure,
							0,
							0,
							largeurFragment,
							hauteurFragment
						);
					} else {
						_canvas.classList.add('vide');
					}
				}

				// On ajoute le canvas à la liste et à la div #board
				ligne.push(_canvas);
			}
			fragments.push(ligne);
		}
	}

	function getNumberImage(number) {
		switch (number) {
			case 1:
				return [48, 48, 50, 50];
			case 2:
				return [118, 48, 50, 50];
			case 3:
				return [189, 47, 50, 50];
			case 4:
				return [260, 47, 50, 50];
			case 5:
				return [332, 47, 50, 50];
			case 6:
				return [404, 48, 50, 50];
			case 7:
				return [45, 118, 50, 50];
			case 8:
				return [118, 119, 50, 50];
			case 9:
				return [188, 119, 50, 50];
			case 10:
				return [260, 120, 50, 50];
			case 11:
				return [332, 120, 50, 50];
			case 12:
				return [403, 118, 50, 50];
			case 13:
				return [45, 190, 50, 50];
			case 14:
				return [120, 190, 50, 50];
			case 15:
				return [190, 190, 50, 50];
			case 16:
				return [260, 190, 50, 50];
			case 17:
				return [332, 190, 50, 50];
			case 18:
				return [405, 190, 50, 50];
			case 19:
				return [45, 262, 50, 50];
			case 20:
				return [117, 262, 50, 50];
			case 21:
				return [187, 262, 50, 50];
			case 22:
				return [260, 262, 50, 50];
			case 23:
				return [330, 262, 50, 50];
			case 24:
				return [403, 262, 50, 50];
			case 25:
				return [47, 333, 50, 50];
			case 26:
				return [117, 333, 50, 50];
			case 27:
				return [188, 333, 50, 50];
			case 28:
				return [260, 333, 50, 50];
			case 29:
				return [332, 333, 50, 50];
			case 30:
				return [403, 333, 50, 50];
			case 31:
				return [47, 404, 50, 50];
			case 32:
				return [120, 404, 50, 50];
			case 33:
				return [190, 404, 50, 50];
			case 34:
				return [260, 404, 50, 50];
			case 35:
				return [332, 404, 50, 50];
			case 36:
				return [400, 404, 50, 50];

			default:
				break;
		}
	}

	/**
	 * - Fonction qui renvoie le numéro de la case recherchée entre 0 et longueure² - 1
	 * @param {number} ligne
	 * @param {number} colonne
	 * @returns {number}
	 */
	function caseAIndex(ligne, colonne) {
		if (
			!(
				ligne < 0 ||
				longueure <= ligne ||
				colonne < 0 ||
				longueure <= colonne
			)
		) {
			return longueure * ligne + colonne;
		}
		console.error(`caseAIndex(${ligne}, ${colonne}) n'existe pas`);
	}

	/**
	 * - Fonction qui renvoie les coordonnées de la case recherchée, exemple grille 3x3, 7 -> [2,1]
	 * @param {number} n - Nombre entre 1 et n²
	 * @returns {number[]}
	 */
	function IndexACase(n) {
		if (1 <= n && n <= longueure ** 2) {
			const colonne = (n - 1) % longueure;
			const ligne = Math.floor((n - 1) / longueure);
			return [ligne, colonne];
		}
		console.error(`IndexACase(${n}) n'existe pas`);
	}

	/**
	 * - Renvoie le tableau [ligne, colonne] du canvas recherché correspondant à sa position dans fragments
	 * @param {number} id - L'id du canvas recherché
	 * @returns {number[]}
	 */
	function getCanvaPositionById(id) {
		const canva = document.getElementById(id);
		for (let ligne = 0; ligne < longueure; ligne++) {
			for (let colonne = 0; colonne < longueure; colonne++) {
				if (canva === fragments[ligne][colonne]) {
					return Array(ligne, colonne);
				}
			}
		}
	}

	/**
	 * - Renvoie haut si la case est juste au dessus de la case vide, idem pour bas, gauche et droite. Et null sinon.
	 * @param {number} ligne - numéro de ligne de la case passée en paramètres
	 * @param {number} colonne - numéro de colonne de la case passée en paramètres
	 * @returns {string | null}
	 */
	function position(ligne, colonne) {
		if (ligne === ligneCaseVide()) {
			if (colonne === colonneCaseVide() - 1) {
				return 'gauche';
			}
			if (colonne === colonneCaseVide() + 1) {
				return 'droite';
			}
		}
		if (colonne === colonneCaseVide()) {
			if (ligne === ligneCaseVide() - 1) {
				return 'haut';
			}
			if (ligne === ligneCaseVide() + 1) {
				return 'bas';
			}
		}
		return null;
	}

	/**
	 * - Fonction qui déplace la case vide quand on appuie sur une touche directionelle, et actualise l'image
	 * @param {???} touche - Évenement ?
	 */
	function reactionDirection(touche) {
		const numeroTouche = +touche.keyCode;
		if (37 <= numeroTouche && numeroTouche <= 40 && !imageEstResolue()) {
			// Si la partie n'a pas encore démarée, on la démarre
			if (!partieEnCours) {
				partieEnCours = true;
				chronoStart();
			}
			if (numeroTouche === 38) {
				// haut
				deplacement('haut');
			} else if (numeroTouche === 40) {
				// bas
				deplacement('bas');
			} else if (numeroTouche === 37) {
				// gauche
				deplacement('gauche');
			} else if (numeroTouche === 39) {
				// droite
				deplacement('droite');
			}
			// On actualise l'image
			afficherCanvas();

			// On exécute imageEstResolue()
			imageEstResolue();
			// Si l'image est terminée, on la remplie entièrement
			if (imageEstResolue()) {
				dessineCanvas();
				afficherCanvas();
				finDePartie();
			}
		}
	}

	/**
	 * Déplace la case vide vers la direction demandé si c'est haut, bas, gauche ou droite
	 * @param {string} direction
	 */
	function deplacement(direction) {
		switch (direction) {
			case 'haut':
				deplacerCaseVide(ligneCaseVide() - 1, colonneCaseVide());
				return true;
			case 'bas':
				deplacerCaseVide(ligneCaseVide() + 1, colonneCaseVide());
				return true;
			case 'gauche':
				deplacerCaseVide(ligneCaseVide(), colonneCaseVide() - 1);
				return true;
			case 'droite':
				deplacerCaseVide(ligneCaseVide(), colonneCaseVide() + 1);
				return true;

			default:
				return false;
		}
	}

	/**
	 * - Déplace la case vide vers la case passée en paramètre si elles se situent sur la même ligne ou même colonne
	 * @param {number} ligne - Numéro de ligne de la case
	 * @param {number} colonne - Numéro de colonne de la case
	 */
	function deplacerCaseVide(ligne, colonne) {
		if (ligne === ligneCaseVide()) {
			if (colonne < colonneCaseVide()) {
				for (let c = colonneCaseVide() - 1; c >= colonne; c--) {
					echangerCases(ligne, c, ligne, c + 1);
				}
			} else {
				for (let c = colonneCaseVide() + 1; c <= colonne; c++) {
					echangerCases(ligne, c, ligne, c - 1);
				}
			}
		} else if (colonne === colonneCaseVide()) {
			if (ligne < ligneCaseVide()) {
				for (let l = ligneCaseVide() - 1; l >= ligne; l--) {
					echangerCases(l, colonne, l + 1, colonne);
				}
			} else {
				for (let l = ligneCaseVide() + 1; l <= ligne; l++) {
					echangerCases(l, colonne, l - 1, colonne);
				}
			}
		}
	}

	/**
	 * - Renvoie le numéro de ligne de la case vide
	 * @returns {number}
	 */
	function ligneCaseVide() {
		return positionCaseVide()[0];
	}

	/**
	 * - Renvoie le numéro de colonne de la case vide
	 * @returns {number}
	 */
	function colonneCaseVide() {
		return positionCaseVide()[1];
	}

	/**
	 * - Fonction qui renvoie les coordonnées de la case vide sous cette forme [ligneCaseVide, colonneCaseVide]
	 * @returns [ligneCaseVide, colonneCaseVide]
	 */
	function positionCaseVide() {
		// On teste chaque case de fragments, jusqu'à trouver la case vide
		for (let ligneIndex = 0; ligneIndex < longueure; ligneIndex++) {
			for (
				let colonneIndex = 0;
				colonneIndex < longueure;
				colonneIndex++
			) {
				if (
					fragments[ligneIndex][colonneIndex].classList.contains(
						'vide'
					)
				) {
					return Array(ligneIndex, colonneIndex);
				}
			}
		}
	}

	/**
	 * - Fonction qui échange fragments[ligne1][colonne1] avec fragments[ligne2][colonne2]
	 * @param {number} ligne1 - Numéro de ligne de la case 1
	 * @param {number} colonne1 - Numéro de colonne de la case 1
	 * @param {number} ligne2 - Numéro de ligne de la case 2
	 * @param {number} colonne2 - Numéro de colonne de la case 2
	 * @returns Vrai s'il y a eu un échange, faux sinon
	 */
	function echangerCases(ligne1, colonne1, ligne2, colonne2) {
		// Si les données sont valides (chaque indice est compris entre 0 et longueure - 1)
		if (
			0 <= ligne1 &&
			ligne1 < longueure &&
			0 <= ligne2 &&
			ligne2 < longueure &&
			0 <= colonne1 &&
			colonne1 < longueure &&
			0 <= colonne2 &&
			colonne2 < longueure
		) {
			// Si les coordonnées sont différentes
			if (!(ligne1 === ligne2 && colonne1 === colonne2)) {
				// On échange les cases
				let tmp = fragments[ligne1][colonne1];
				fragments[ligne1][colonne1] = fragments[ligne2][colonne2];
				fragments[ligne2][colonne2] = tmp;
				tmp = grille[ligne1][colonne1];
				grille[ligne1][colonne1] = grille[ligne2][colonne2];
				grille[ligne2][colonne2] = tmp;
				return true;
			}
		}
		return false;
	}

	/**
	 * - On ordonne le tableau fragments comme la grille
	 */
	function triFragments() {
		const tab = Array();
		for (let l = 0; l < n; l++) {
			const ligne = Array();
			for (let c = 0; c < n; c++) {
				// On ajoute à ligne le canvas dont l'id est grille[l][c]
				const id = grille[l][c];
				const canvas = document.getElementById(`canvas-${id}`);
				ligne.push(canvas);
			}
			tab.push(ligne);
		}
		fragments = tab;
	}

	/**
	 * - Efface puis remplit la div #board avec les canvas du tableau fragments
	 */
	function afficherCanvas() {
		// On supprime tous les noeuds enfant de board
		board1.innerHTML = '';

		const g = document.createElement('div');
		g.id = 'div';
		format = getCookie('format');
		g.classList.add(format);

		// On ajoute un à un les canvas dans l'ordre de la liste fragments
		fragments.forEach(function (ligne) {
			const row = document.createElement('div');
			row.classList.add('ligne');
			ligne.forEach(function (canva) {
				row.appendChild(canva);
			});
			g.appendChild(row);
		});
		board1.appendChild(g);
	}

	function estTriee(grille) {
		const hauteur = grille.length;
		for (let l = 0; l < hauteur; l++) {
			const longueure = grille[l].length;
			for (let c = 0; c < longueure; c++) {
				const numeroCase = caseAIndex(l, c);
				if (numeroCase + 1 !== grille[l][c]) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * - Renvoie vrai si l'image est résolue, faux sinon.
	 * @returns {boolean}
	 */
	function imageEstResolue() {
		// Pour chaque case de tab, si sa valeur correspond à son numéro alors on renvoie vrai, faux sinon
		for (let l = 0; l < n; l++) {
			for (let c = 0; c < n; c++) {
				const id = grille[l][c];
				const numeroCase = caseAIndex(l, c);
				if (id !== numeroCase + 1) {
					return false;
				}
			}
		}
		chronoStop();
		return true;
	}

	function melangeEstParfait() {
		if (n === 2) {
			return true;
		}
		let answer = true;
		for (let ligne = 0; ligne < n; ligne++) {
			for (let colonne = 0; colonne < n; colonne++) {
				directions.forEach((direction) => {
					if (getCaseDir(direction, ligne, colonne)) {
						const val1 = grille[ligne][colonne];
						const val2 =
							grille[getCaseDir(direction, ligne, colonne)[0]][
								getCaseDir(direction, ligne, colonne)[1]
							];
						switch (direction) {
							case 'haut':
								if (val2 === val1 - n) {
									answer = false;
								}
								break;
							case 'bas':
								if (val2 === val1 + n) {
									answer = false;
								}
								break;
							case 'gauche':
								if (val2 === val1 - 1) {
									answer = false;
								}
								break;
							case 'droite':
								if (val2 === val1 + 1) {
									answer = false;
								}
								break;

							default:
								break;
						}
					}
				});
			}
		}
		return answer;
	}

	/**
	 * - Renvoie la case adjacente située dans la direction indiquée par rapport à la case passée en paramètre, faux si cette case est hors limites.
	 * @param {string} direction
	 * @param {number} ligne
	 * @param {number} colonne
	 * @returns
	 */
	function getCaseDir(direction, ligne, colonne) {
		let l = ligne,
			c = colonne;
		switch (direction) {
			case 'haut':
				l--;
				break;
			case 'bas':
				l++;
				break;
			case 'gauche':
				c--;
				break;
			case 'droite':
				c++;
				break;

			default:
				console.error(
					`getCaseDir(${direction}, ${ligne}, ${colonne}) n'existe pas`
				);
				break;
		}
		// Cas où la case recherchée n'est pas dans les limites de la grille
		if (l < 0 || n <= l || c < 0 || n <= c) {
			return false;
		} else {
			return [l, c];
		}
	}

	/**
	 * - Fonction qui mélange la grille
	 */
	function melange() {
		if (longueure === 2) {
			/** nombre aléatoire qui vaut 1 ou 2 */
			const rand = Math.floor(Math.random() * 2) + 1;
			if (rand === 1) {
				grille = [
					[2, 3],
					[1, 4],
				];
			} else {
				grille = [
					[3, 1],
					[2, 4],
				];
			}
		} else {
			const tab = [];
			const res = [];
			for (let i = 1; i < n ** 2; i++) {
				tab.push(i);
			}
			let positionMax = n ** 2;
			for (let i = 1; i <= n ** 2; i++) {
				/** - Nombre entier aléatoire dans [1, positionMax] */
				let positionAleatoire = Math.floor(
					Math.random() * positionMax + 1
				);
				let indexAleatoire = 1;
				while (
					indexAleatoire < positionAleatoire ||
					res.includes(tab[indexAleatoire - 1])
				) {
					if (res.includes(tab[indexAleatoire - 1])) {
						positionAleatoire++;
					}
					indexAleatoire++;
				}
				res.push(positionAleatoire);
				positionMax--;
			}
			for (let ligne = 0; ligne < n; ligne++) {
				for (let colonne = 0; colonne < n; colonne++) {
					grille[ligne][colonne] = res[n * ligne + colonne];
				}
			}

			// On place le dernier élément à la dernière case
			for (let ligne = 0; ligne < n; ligne++) {
				for (let colonne = 0; colonne < n; colonne++) {
					if (!(ligne === n - 1 && colonne === n - 1)) {
						if (grille[ligne][colonne] === n ** 2) {
							grille[ligne][colonne] = grille[n - 1][n - 1];
							grille[n - 1][n - 1] = n ** 2;
						}
					}
				}
			}
		}
	}

	/**
	 * - Renvoie si oui ou non la grille est solvable
	 * @returns {boolean}
	 */
	function estSolvable() {
		if (n === 2) {
			return true;
		}
		if (3 <= n) {
			return niveauDeMelange() % 2 === 0;
		}
	}

	/**
	 *  - Renvoie le niveau de mélange de la grille (nombre de paires inversées)
	 * @returns {number}
	 */
	function niveauDeMelange() {
		let res = 0;
		for (let i = 1; i < n ** 2; i++) {
			for (let j = i; j <= n ** 2; j++) {
				if (!sontOrdonnees(i, j)) {
					res++;
				}
			}
		}
		return res;
	}

	/**
	 *  - renvoie vrai si i et j sont dans le bon ordre, sinon faux
	 * @param {number} i
	 * @param {number} j
	 * @returns {boolean} renvoie vrai si i et j sont dans le bon ordre, sinon faux
	 */
	function sontOrdonnees(i, j) {
		if (0 < i && i <= n ** 2 && 0 < j && j <= n ** 2) {
			if (j < i) {
				return sontOrdonnees(j, i);
			}
			// ici i <= j
			if (pos(i) > pos(j)) {
			}
			return pos(i) <= pos(j);
		}
	}

	/**
	 * - Renvoie la position de i dans la grille (entre 1 et n²)
	 * @param {number} i - valeur recherchée dans la grille (entre 1 et n²)
	 * @returns {number} position de i dans la grille (entre 1 et n²)
	 */
	function pos(i) {
		if (0 < i && i <= n ** 2) {
			for (let ligne = 0; ligne < n; ligne++) {
				for (let colonne = 0; colonne < n; colonne++) {
					if (grille[ligne][colonne] === i) {
						return ligne * n + colonne + 1;
					}
				}
			}
		}
	}

	function redimensionnement() {
		let version = 2;

		if (version === 1) {
			// On récupère l'image entière, et le canvas
			const image = document.getElementById('image');
			const canvas = document.getElementById('canvas-0');

			// On récupère les dimension de la div orange
			const orange = document.getElementById('orange');
			const largeurOrange = orange.clientWidth;
			const hauteurOrange = orange.clientHeight;

			// On récupère les dimension de l'image
			const longueurImage = image.width;
			const hauteurImage = image.height;

			// On définit les dimensions du canvas en fonction de celles de l'image
			canvas.width = largeurImage;
			canvas.height = hauteurImage;

			// On récupère les dimension du canvas
			const largeurCanvas = canvas.width;
			const hauteurCanvas = canvas.height;

			dessineCanvas();
			// Il faut trier les éléments de fragments dans l'ordre de la grille
			afficherCanvas();
			triFragments();
			afficherCanvas();
			// On lui donne les dimension de l'image
			board1.width = 0.6 * window.innerWidth;
			board1.height = 0.6 * window.innerHeight;

			const longueurBoard = document.getElementById('board1').clientWidth;
			const hauteurBoard = document.getElementById('board1').clientHeight;

			// On calcule leurs ratios h / l
			const ratioBoard = hauteurBoard / longueurBoard;
			const ratioImage = hauteurImage / longueurImage;

			// On détermine le coefficient multiplicatif
			let coefficient;
			if (ratioBoard < ratioImage) {
				coefficient = hauteurBoard / hauteurImage;
			} else {
				coefficient = longueurBoard / longueurImage;
			}
		} else if (version === 2) {
			// ANCIENNE VERSION

			const board1 = document.getElementById('board1');

			// On récupère les dimensions de #board1
			const longueurBoard = document.getElementById('board1').clientWidth;
			const hauteurBoard = document.getElementById('board1').clientHeight;

			// On récupère les dimensions de #image
			let longueurImage = document.getElementById('image').width;
			let hauteurImage = document.getElementById('image').height;

			// On calcule leurs ratios h / l
			const ratioBoard = hauteurBoard / longueurBoard;
			const ratioImage = hauteurImage / longueurImage;

			// On détermine le coefficient multiplicatif
			let coefficient;
			if (ratioBoard < ratioImage) {
				coefficient = hauteurBoard / hauteurImage;
			} else {
				coefficient = longueurBoard / longueurImage;
			}

			hauteurImage *= coefficient * 0.7;
			longueurImage *= coefficient * 0.7;

			const hauteurCanvas = hauteurImage / n;
			const longueurCanvas = longueurImage / n;

			hauteurFragment = hauteurCanvas;
			largeurFragment = longueurCanvas;

			dessineCanvas();
			// Il faut trier les éléments de fragments dans l'ordre de la grille
			afficherCanvas();
			triFragments();
			afficherCanvas();
		}
	}

	function printNumbers() {
		for (let id = 1; id < longueure * longueure; id++) {
			let canva = document.getElementById(`canvas-${id}`);
			const values = getNumberImage(id);
			canva
				.getContext('2d')
				.drawImage(
					numberImage,
					values[0],
					values[1],
					values[2],
					values[3],
					0,
					0,
					largeurFragment,
					hauteurFragment
				);
		}
	}
});

// Chronomètre ------------------------------------------------------------------------------------------

let startTime = 0;
let start = 0;
let end = 0;
let diff = 0;
let timerID = 0;

const secondDiv = document.querySelector('.second-hand');
const minDiv = document.querySelector('.min-hand');
const hourDiv = document.querySelector('.hour-hand');
const miliSecondDiv = document.querySelector('.miliSecond-hand');

function chrono() {
	end = new Date();
	diff = end - start;
	diff = new Date(diff);
	let msec = diff.getMilliseconds();
	let sec = diff.getSeconds();
	let min = diff.getMinutes();
	let hr = diff.getHours() - 1;
	if (min < 10) {
		min = '0' + min;
	}
	if (sec < 10) {
		sec = '0' + sec;
	}
	if (msec < 10) {
		msec = '00' + msec;
	} else if (msec < 100) {
		msec = '0' + msec;
	}
	document.getElementById('chronotime').value =
		hr + ':' + min + ':' + sec + ':' + msec;
	timerID = setTimeout('chrono()', 10);

	const miliSeconds = msec;
	const miliDeg = (miliSeconds / 1000) * 360 + 90;
	miliSecondDiv.style.transform = `rotate(${miliDeg}deg)`;

	const seconds = sec;
	const secDeg = (seconds / 60) * 360 + 90;
	secondDiv.style.transform = `rotate(${secDeg}deg)`;

	const mins = min;
	const minDeg = (mins / 60) * 360 + (seconds / 60) * 6 + 90;
	minDiv.style.transform = `rotate(${minDeg}deg)`;
}

function chronoStart() {
	document.chronoForm.startstop.value = 'stop!';
	document.chronoForm.startstop.onclick = chronoStop;
	document.chronoForm.reset.onclick = chronoReset;
	start = new Date();
	chrono();
}

function chronoContinue() {
	document.chronoForm.startstop.value = 'stop!';
	document.chronoForm.startstop.onclick = chronoStop;
	document.chronoForm.reset.onclick = chronoReset;
	start = new Date() - diff;
	start = new Date(start);
	chrono();
}

function chronoReset() {
	document.getElementById('chronotime').value = '0:00:00:000';
	start = new Date();
}

function chronoStopReset() {
	document.getElementById('chronotime').value = '0:00:00:000';
	document.chronoForm.startstop.onclick = chronoStart;
}

function chronoStop() {
	document.chronoForm.startstop.value = 'start!';
	document.chronoForm.startstop.onclick = chronoContinue;
	document.chronoForm.reset.onclick = chronoStopReset;
	clearTimeout(timerID);
}

// COOKIES ---------------------------------------------------------------------------------------

function getCookie(cname) {
	let name = cname + '=';
	let ca = document.cookie.split(';');
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

function setCookie(cname, cvalue) {
	document.cookie = cname + '=' + cvalue + ';';
}

function finDePartie() {
	const region = getCookie('region');
	const chronotime = document.getElementById('chronotime');
	const temps = chronotime.value;
	const heures = +temps.substr(0, 1);
	const minutes = +temps.substr(2, 2);
	const secondes = +temps.substr(5, 2);
	const milliemes = +temps.substr(8, 3);

	document.cookie = `lastTime=${temps}`;

	orange.innerHTML = '';
	orange.appendChild(divFinDePartie(temps));

	// Si le cookie region existe et est plus petit on ne fait rien
	if (getCookie(region)) {
		const ancienTemps = properTime2(getCookie(region));
		const nouveauTemps = properTime2(temps);
		if (tempsPlusPetit(nouveauTemps, ancienTemps)) {
			setCookie(region, temps);
		}
	} else {
		setCookie(region, temps);
	}

	// On ajoute un bouton Scores dans le header
	const scoresButton = document.createElement('a');
	scoresButton.id = 'scores';
	scoresButton.innerHTML = 'Enregistrer le temps';
	scoresButton.href = 'scores.php';
	document
		.getElementsByClassName('endgame-sidebar')[0]
		.appendChild(scoresButton);
}

function properTime(temps) {
	const t = properTime2(temps);
	let res = t.replace('h', ' heures ');
	res = res.replace("'", ' minutes ');
	return (res = res.replace('"', ' secondes '));
}

function properTime2(temps) {
	// h:mm:ss:mmm
	// temps = '0:00:02:015';
	const heures = +temps.split(':')[0];
	const minutes = +temps.split(':')[1];
	const secondes = +temps.split(':')[2];
	const milliemes = +temps.split(':')[3];
	let res = '';
	if (heures) {
		// 2h05 ou 12h00
		return (
			heures +
			'h' +
			(minutes >= 10 ? minutes : '0' + minutes) +
			'm' +
			(secondes >= 10 ? secondes : '0' + secondes) +
			's'
		);
	}
	if (minutes) {
		// 2'05
		return (
			minutes +
			' minute' +
			(minutes > 1 ? 's' : '') +
			' ' +
			(secondes ? (secondes >= 10 ? secondes : '0' + secondes) : '00')
		);
	}
	return (
		secondes +
		',' +
		(milliemes ? (milliemes >= 10 ? milliemes : '0' + milliemes) : '00') +
		' seconde' +
		(secondes > 1 ? 's' : '')
	);
}

function divFinDePartie(temps) {
	// temps = '1:26:35:455';
	const image = document.getElementById('image');
	const newDiv = document.createElement('div');
	const endgameSidebar = document.createElement('div');
	endgameSidebar.classList.add('endgame-sidebar');
	newDiv.id = 'lastDiv';
	newDiv.appendChild(image);
	image.style.display = 'block';

	// if ((region = 'Bretagne')) {
	//     image.style.height = '' + 0.6 * window.innerHeight + 'px';
	//     const newP = document.createElement('P');
	//     newP.setAttribute('id', 'texte-fin-de-partie');
	//     newP.innerHTML = `Bravo, vous avez <br>résolu l'image en <br>${properTime(
	//         temps
	//     )}`;
	//     newDiv.appendChild(newP);
	//     return newDiv;
	// }
	if (image.width > image.height) {
		// image.style.width = '' + 0.6 * window.innerWidth + 'px';
	} else {
		image.style.height = '' + 0.6 * window.innerHeight + 'px';
	}
	if ((region = 'Pays de la Loire')) {
		image.style.width = '' + 0.4 * window.innerWidth + 'px';
	}
	const newP = document.createElement('P');
	newP.innerHTML = `Bravo, vous avez <br> résolu l'image en <br>${properTime2(
		temps
	)}`;
	endgameSidebar.appendChild(newP);
	newDiv.appendChild(endgameSidebar);

	return newDiv;
}

function getDivScores() {
	// function getDivScores(imageName, difficulty) {
	const newDiv = document.createElement('div');
	let score = '';
}

function getScores() {
	return fetch('public/scores/Bretagne-facile.txt')
		.then((response) => response.text())
		.then((text) => {
			return text;
		});
}

function tempsPlusPetit(temps1, temps2) {
	return timeToSeconds(temps1) <= timeToSeconds(temps2);
}

/**
 * Convertit un temps donné string par le html en nombre de secondes
 * @param {string} temps - forme : 8h12 ou 24'58 ou 54"98
 * @returns {number}
 */
function timeToSeconds(temps) {
	const n = temps.length;
	// Il faut la position de lunit" de temps (h, ', ou ")
	const unites = ['h', "'", '"'];
	let unite;
	let partieEntiere;
	let partieDecimale;
	let indiceUnite;
	for (let i = 0; i < n; i++) {
		if (unites.includes(temps[i])) {
			unite = temps[i];
			indiceUnite = i;
		}
	}
	partieEntiere = +temps.substring(0, indiceUnite);
	partieDecimale = +temps.substring(indiceUnite + 1, n);
	switch (unite) {
		case 'h':
			return 3600 * partieEntiere + 60 * partieDecimale;
		case "'":
			return 60 * partieEntiere + partieDecimale;
		case '"':
			if (partieDecimale < 10) {
				return partieEntiere + partieDecimale / 10;
			}
			return partieEntiere + partieDecimale / 100;

		default:
			break;
	}
}

function randomInt(min, max) {
	if (min > max) {
		return randomInt(max, min);
	}
	const gap = max - min;
	return Math.floor(Math.random() * (gap + 1) + min);
}
