document.addEventListener('DOMContentLoaded', () => {
	const images = document.getElementsByClassName('image');
	const imagesName = document.getElementsByClassName('image-name');
	const nbJoueurs = +getCookie('nbJoueurs');
	const randomImage = document.getElementById('randomImage');
	const previousImage = document.getElementById('previousImage');
	const imageName = getCookie('region');

	for (const image of images) {
		image.addEventListener('click', () => {
			if (nbJoueurs === 1) {
				index = 80;
			} else {
				index = 79;
			}
			setCookie('region', getImageName(image));
			// window.location.href = 'jeu.php';
			window.location.href = 'preview.php';
		});
	}

	for (const name of imagesName) {
		name.addEventListener('click', () => {
			if (nbJoueurs === 1) {
				index = 80;
			} else {
				index = 79;
			}
			let image = name.parentNode.getElementsByClassName('image')[0];
			setCookie('region', getImageName(image));
			// window.location.href = 'jeu.php';
			window.location.href = 'preview.php';
		});
	}

	randomImage.addEventListener('click', () => {
		let newImageName;
		do {
			newImageName =
				nbJoueurs === 1
					? randomInt(firstImageNumber, lastImageNumber)
					: randomInt(firstImageBisNumber, lastImageBisNumber);
		} while (newImageName === imageName);
		setCookie('region', newImageName);
	});
});

function getCookie(cname) {
	var name = cname + '=';
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

function setCookie(cname, cvalue) {
	document.cookie = cname + '=' + cvalue + ';';
}

function eraseCookie(name) {
	document.cookie = name + '=; Max-Age=-99999999;';
}

function getImageName(img) {
	// image src is "http://localhost/projects/jeux/taquin/public/images/game-images/other/1.jpg"
	const src = img.src;
	return src.split('/')[src.split('/').length - 1].split('.')[0];
}
