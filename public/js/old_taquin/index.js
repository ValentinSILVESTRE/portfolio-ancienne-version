document.addEventListener('DOMContentLoaded', () => {
	const regions = [
		'Hauts-de-France',
		'Normandie',
		'Île-de-France',
		'Grand Est',
		'Bretagne',
		'Pays de la Loire',
		'Centre-Val de Loire',
		'Bourgogne-Franche-Comté',
		'Nouvelle-Aquitaine',
		'Auvergne-Rhône-Alpes',
		'Occitanie',
		"Provence-Alpes-Côte d'Azur",
		'Corse',
	];

	const lettres = [
		'A',
		'B',
		'C',
		'D',
		'E',
		'F',
		'G',
		'H',
		'I',
		'J',
		'K',
		'L',
		'M',
	];

	// On récupère les cookies de temps
	regions.forEach((region) => {
		if (getCookie(region)) {
			eraseCookie(region);
		}
	});

	function eraseCookie(name) {
		document.cookie = name + '=; Max-Age=-99999999;';
	}

	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}
});
