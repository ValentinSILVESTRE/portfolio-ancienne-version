const h2 = document.getElementsByTagName('h2')[0];

for (let i = 0; i < 3; i++) {
	setTimeout(() => {
		let nbSecondesLeft = 3 - i;
		h2.textContent = `Vous allez être déconnecté dans ${nbSecondesLeft} seconde${
			nbSecondesLeft > 1 ? 's' : ''
		}.`;
	}, i * 1000);
}
