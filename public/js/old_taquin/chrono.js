document.addEventListener('DOMContentLoaded', () => {
	// Chronomètre ------------------------------------------------------------------------------------------

	var startTime = 0;
	var start = 0;
	var end = 0;
	var diff = 0;
	var timerID = 0;

	const secondDiv = document.querySelector('.second-hand');
	const minDiv = document.querySelector('.min-hand');
	const hourDiv = document.querySelector('.hour-hand');
	const miliSecondDiv = document.querySelector('.miliSecond-hand');

	chronoReset();

	board1.addEventListener('click', chronoStart, { once: true });

	// On actualise l'image quand on clique sur $
	document.addEventListener('keydown', (touche) => {
		const numeroTouche = +touche.keyCode;
		if (37 <= numeroTouche && numeroTouche <= 40) {
			chronoStart();
		}
		if (document.getElementById('board1').classList.contains('fini')) {
			chronoStop();
		}
	});

	function chrono() {
		end = new Date();
		diff = end - start;
		diff = new Date(diff);
		var msec = diff.getMilliseconds();
		var sec = diff.getSeconds();
		var min = diff.getMinutes();
		var hr = diff.getHours() - 1;
		if (min < 10) {
			min = '0' + min;
		}
		if (sec < 10) {
			sec = '0' + sec;
		}
		if (msec < 10) {
			msec = '00' + msec;
		} else if (msec < 100) {
			msec = '0' + msec;
		}
		document.getElementById('chronotime').value =
			hr + ':' + min + ':' + sec + ':' + msec;
		timerID = setTimeout(chrono, 10);

		const miliSeconds = msec;
		const miliDeg = (miliSeconds / 1000) * 360 + 90;
		miliSecondDiv.style.transform = `rotate(${miliDeg}deg)`;

		const seconds = sec;
		const secDeg = (seconds / 60) * 360 + 90;
		secondDiv.style.transform = `rotate(${secDeg}deg)`;

		const mins = min;
		const minDeg = (mins / 60) * 360 + (seconds / 60) * 6 + 90;
		minDiv.style.transform = `rotate(${minDeg}deg)`;
	}

	function chronoStart() {
		document.chronoForm.startstop.value = 'stop!';
		document.chronoForm.startstop.onclick = chronoStop;
		document.chronoForm.reset.onclick = chronoReset;
		start = new Date();
		chrono();
	}

	function chronoContinue() {
		document.chronoForm.startstop.value = 'stop!';
		document.chronoForm.startstop.onclick = chronoStop;
		document.chronoForm.reset.onclick = chronoReset;
		start = new Date() - diff;
		start = new Date(start);
		chrono();
	}

	function chronoReset() {
		document.getElementById('chronotime').value = '0:00:00:000';
		start = new Date();
	}

	function chronoStopReset() {
		document.getElementById('chronotime').value = '0:00:00:000';
		document.chronoForm.startstop.onclick = chronoStart;
	}

	function chronoStop() {
		document.chronoForm.startstop.value = 'start!';
		document.chronoForm.startstop.onclick = chronoContinue;
		document.chronoForm.reset.onclick = chronoStopReset;
		clearTimeout(timerID);
	}
});
