const pokemonList = pokedex[0];
let tier = 'ru';

if (getCookie('tier')) {
	tier = getCookie('tier').toLowerCase();
}
const setsList = sets[0][`gen8${tier}`]['dex'];
const movesList = moves[0];
const learnSetsList = BattleLearnsets;
const abilitiesList = abilities[0];
const itemsList = items[0];

const team1 = getCookie('team1').split('%20%2F%20');
const team2 = getCookie('team2').split('%20%2F%20');

/**
 * - Retourne une instance de la classe Pokemon ou rien
 * @param {string} pokemonName - Le nom du pokemon avec des Majuscules
 * @returns {Pokemon}
 */
function getPokemon(pokemonName) {
	let searchName = formatName(pokemonName);

	for (const pkmn in pokemonList) {
		const pokemon = pokemonList[pkmn];
		if (pokemon['name'] === searchName) {
			return pokemon;
		}
	}
	console.error(`getPokemon(${searchName}) n'a rien trouvé.`);
}

function calcStat(stat, base, iv, ev, lvl, nature) {
	if (stat !== 'hp') {
		return Math.floor(
			Math.floor(5 + ((2 * base + iv + Math.floor(ev / 4)) * lvl) / 100) *
				coefNature(nature, stat)
		);
	} else {
		return (
			Math.floor(((2 * base + iv + Math.floor(ev / 4)) * lvl) / 100) +
			lvl +
			10
		);
	}
}

/**
 * - Renvoie le coefficient multiplicateur de la nature sur la statistique (0.9, 1 ou 1.1)
 * @param {string} nature
 * @param {string} stat
 * @returns {number}
 */
function coefNature(nature, stat) {
	if (
		!['hp', 'atk', 'def', 'spa', 'spd', 'spe'].includes(stat) ||
		![
			'Bold',
			'Quirky',
			'Brave',
			'Calm',
			'Quiet',
			'Docile',
			'Mild',
			'Rash',
			'Gentle',
			'Hardy',
			'Jolly',
			'Lax',
			'Impish',
			'Sassy',
			'Naughty',
			'Modest',
			'Naive',
			'Hasty',
			'Careful',
			'Bashful',
			'Relaxed',
			'Adamant',
			'Serious',
			'Lonely',
			'Timid',
		].includes(nature)
	) {
		console.error(`Impossible de faire coefNature(${nature}, ${stat})`);
	} else {
	}
	if (stat === 'hp') {
		return 1;
	}
	return natureList[nature][stat];
}

/**
 * - Renvoie le nombre de PV perdus
 * @param {number} level
 * @param {number} attack
 * @param {number} power
 * @param {number} defense
 * @param {number} multiplier
 * @returns {number}
 */
function damageCalculator(level, attack, power, defense, multiplier) {
	return Math.floor(
		(((level * 0.4 + 2) * attack * power) / (defense * 50) + 2) * multiplier
	);
}

/**
 * - Calcul le pourcentage de PV perdu
 * @param {Pokemon} offensivePokemon
 * @param {Pokemon} defensivePokemon
 * @param {object} move
 * @param {Array} options - hp en option donne le nombre de pv perdu et non le pourcentage
 * @returns {number}
 */
function calcDamages(offensivePokemon, defensivePokemon, move, options = []) {
	// ? Données ==============================================================================================

	/** @type{string} - Le climat */
	const climate = document.getElementById('climate').value;

	/** @type{number} - Niveau du pokemon qui attaque */
	const level = +offensivePokemon.level;

	/** @type{number} - Attaque ou attaque spécial du pokemon qui attaque */
	let attack;

	// Unaware ignore les boosts offensifs
	if (defensivePokemon.getAbility() === 'Unaware') {
		attack = +offensivePokemon.getStat(
			move['category'] === 'Special' ? 'spa' : 'atk',
			climate,
			['no-boost']
		);
	} else {
		attack = +offensivePokemon.getStat(
			move['category'] === 'Special' ? 'spa' : 'atk',
			climate
		);
	}

	/** @type{number} - Puissance de l'attaque */
	let power = getBasePower(move, offensivePokemon);

	// Stored Power: 20 + 20 par boost positif
	if (move.name === 'Stored Power') {
		power = 20;
		statsList.forEach((stat) => {
			if (offensivePokemon.getBoost(stat) > 0) {
				power += 20 * offensivePokemon.getBoost(stat);
			}
		});
	}
	// Fishious Rend: power×2 si l'attaquant est plus rapide que le défenseur
	if (
		move.name === 'Fishious Rend' &&
		offensivePokemon.getStat('spe') > defensivePokemon.getStat('spe')
	) {
		power *= 2;
	}

	/** @type{number} - Défense du pokemon */
	let defense = defensivePokemon.getStat(
		move['category'] === 'Special' ? 'spd' : 'def'
	);

	/** @type{number} - Stab de l'attaque (1 ou 1.5) */
	let stab = offensivePokemon.getStab(move);
	if (move.name === 'Multi-Attack') {
		stab = 1.5;
	}

	/** @type{number} - Efficacité de l'attaque (0, 0.25, 0.5, 1, 2 ou 4) */
	let effectiveness = 1;
	effectiveness = getEffectiveness(offensivePokemon, defensivePokemon, move);

	/** @type{number} - Le coefficient multiplicatif (stab * abilityChange * itemChange * moveChange) */
	let multiplier = 1;

	/** @type{string} - Le type de l'attaque */
	const type = move.type;

	/** @type{string} - La category de l'attaque (Physical, Special ou Status) */
	const category = move.category;

	// ?    1. Climats ==============================================================================================

	switch (climate) {
		case 'rain':
			if (type === 'Water') {
				power *= 1.5;
			}
			if (type === 'Fire') {
				power *= 0.5;
			}
			break;

		case 'sun':
			if (type === 'Water') {
				power *= 0.5;
			}
			if (type === 'Fire') {
				power *= 1.5;
			}
			break;

		case 'sand':
			if (category === 'Special' && defensivePokemon.isType('Rock')) {
				defense *= 1.5;
			}
			break;

		default:
			break;
	}

	// ?    1. Terrain ==============================================================================================

	const terrain = document.getElementById('terrain').value;
	switch (terrain) {
		case 'Electric':
			if (type === 'Electric' && offensivePokemon.isGrounded()) {
				power *= 1.3;
			}
			break;

		case 'Grassy':
			if (type === 'Grass' && offensivePokemon.isGrounded()) {
				power *= 1.3;
			}
			if (
				['Earthquake', 'Bulldoze', 'Magnitude'].includes(move.name) &&
				defensivePokemon.isGrounded()
			) {
				power *= 0.5;
			}
			break;

		case 'Psychic':
			if (type === 'Psychic' && offensivePokemon.isGrounded()) {
				power *= 1.3;
			}
			if (defensivePokemon.isGrounded() && move.priority > 0) {
				return 0;
			}
			break;

		case 'Misty':
			if (type === 'Dragon' && defensivePokemon.isGrounded()) {
				power *= 0.5;
			}

			break;

		default:
			break;
	}

	// ?    1. Objets ==============================================================================================

	const offensiveItem = offensivePokemon.getItem();
	const defensiveItem = defensivePokemon.getItem();

	// Objets défensifs
	switch (defensiveItem) {
		case 'Assault Vest':
			// Défense spéciale augmenté de 50%
			if (category === 'Special') {
				defense *= 1.5;
			}
			break;

		case 'Eviolite':
			// Statistiques de défenses augmentées de 50%
			defense *= 1.5;
			break;

		case 'Babiri Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Steel
			if (type === 'Steel' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Charti Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Rock
			if (type === 'Rock' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Chilan Berry':
			// Dégats diminués de 50% pour les attaques de types Normal
			if (type === 'Normal' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Chople Berry':
			// Dégats diminués de 50% pour les attaques de types Fighting
			if (type === 'Fighting' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Coba Berry':
			// Dégats diminués de 50% pour les attaques de types Flying
			if (type === 'Flying' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Colbur Berry':
			// Dégats diminués de 50% pour les attaques de types Dark
			if (type === 'Dark' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Occa Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Fire
			if (type === 'Fire' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Haban Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Dragon
			if (type === 'Dragon' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Kasib Berry':
			// Dégats diminués de 50% pour les attaques de types Ghost
			if (type === 'Ghost' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Kebia Berry':
			// Dégats diminués de 50% pour les attaques de types Poison
			if (type === 'Poison' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Passo Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Water
			if (type === 'Water' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Payapa Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Psychic
			if (type === 'Psychic' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Rindo Berry':
			// Dégats diminués de 50% pour les attaques super efficaces de types Grass
			if (type === 'Grass' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Roseli Berry':
			// Dégats diminués de 50% pour les attaques de types Fairy
			if (type === 'Fairy' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Shuca Berry':
			// Dégats diminués de 50% pour les attaques de types Ground
			if (type === 'Ground' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Wacan Berry':
			// Dégats diminués de 50% pour les attaques de types Electric
			if (type === 'Electric' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Yache Berry':
			// Dégats diminués de 50% pour les attaques de types Ice
			if (type === 'Ice' && effectiveness > 1) {
				multiplier *= 0.5;
			}
			break;

		default:
			break;
	}

	// Objets offensifs
	switch (offensiveItem) {
		case 'Choice Band':
			// Attaque augmenté de 50%
			if (category === 'Physical') {
				attack *= 1.5;
			}
			break;

		case 'Choice Specs':
			// Attaque spécial augmenté de 50%
			if (category === 'Special') {
				attack *= 1.5;
			}
			break;

		case 'Life Orb':
			// Attaques augmenté de 30%
			attack *= 1.3;
			break;

		case 'Thick Club':
			// Attaque doublée pour Marowak
			if (category === 'Physical') {
				attack *= 2;
			}
			break;

		case 'Silk Scarf':
			// Attaques normal +20%
			if (category === 'Physical') {
				attack *= 1.2;
			}
			break;

		case 'Spell Tag':
			// Attaques spectres +20%
			if (move.type === 'Ghost') {
				power *= 1.2;
			}
			break;

		default:
			break;
	}

	// ?    2. Talents ==============================================================================================

	const offensiveAbility = offensivePokemon.ability;
	const defensiveAbility = defensivePokemon.ability;

	// Défensifs
	switch (defensiveAbility) {
		case 'Dry Skin':
			// Immunité eau et faiblesse feu (puissance augmenté de 25%)
			if (type === 'Water') {
				effectiveness = 0;
			}
			if (type === 'Fire') {
				multiplier *= 1.25;
			}
			break;

		case 'Flash Fire':
			// Immunité feu
			if (type === 'Fire') {
				effectiveness = 0;
			}
			break;

		case 'Fluffy':
			// Dégats diminués par 50% pour les attaques de contact et doublés pour les attaques feu
			if (move.flags.contact && move.flags.contact === 1) {
				multiplier *= 0.5;
			}
			if (type === 'Fire') {
				multiplier *= 2;
			}
			break;

		case 'Fur Coat':
			// Défense doublée
			if (category === 'Physical') {
				defense *= 2;
			}
			break;

		case 'Heatproof':
			// Puissance des attaques feu diminuée de 50%
			if (type === 'Fire') {
				power /= 2;
			}
			break;

		case 'Ice Scales':
			// Dégats des attaques spréciales divisés par 2
			if (category === 'Special') {
				multiplier /= 2;
			}
			break;

		case 'Levitate':
			// Immunité sol
			if (type === 'Ground' && !defensivePokemon.roosted) {
				effectiveness = 0;
			}
			break;

		case 'Lightning Rod':
			// Immunité électrique
			if (type === 'Electric') {
				effectiveness = 0;
			}
			break;

		case 'Motor Drive':
			// Immunité électrique
			if (type === 'Electric') {
				effectiveness = 0;
			}
			break;

		case 'Prism Armor':
			// Attaque super efficace : dégats diminués de 25%
			if (effectiveness > 1) {
				multiplier *= 0.75;
			}
			break;

		case 'Punk Rock':
			// Attaques sonores : dégats diminués de 50%
			if (move.flags && move.flags.sound && move.flags.sound === 1) {
				multiplier *= 0.5;
			}
			break;

		case 'Sap Sipper':
			// Immunité plante
			if (type === 'Grass') {
				effectiveness = 0;
			}
			break;

		case 'Solid Rock':
			// Attaque super efficace : dégats diminués de 25%
			if (effectiveness > 1) {
				multiplier *= 0.75;
			}
			break;

		case 'Soundproof':
			// Immunisé aux attaques sonores
			if (move.flags && move.flags.sound && move.flags.sound === 1) {
				effectiveness = 0;
			}
			break;

		case 'Storm Drain':
			// Immunité eau
			if (type === 'Water') {
				effectiveness = 0;
			}
			break;

		case 'Thick Fat':
			// Les attaques de type feu ou glace provoquent une réduction de l'attaque de 50%
			if (type === 'Fire' || type === 'Ice') {
				attack /= 2;
			}
			break;

		case 'Volt Absorb':
			// Immunité électrique
			if (type === 'Electric') {
				effectiveness = 0;
			}
			break;

		case 'Water Absorb':
			// Immunité électrique
			if (type === 'Water') {
				effectiveness = 0;
			}
			break;

		case 'Water Bubble':
			// Attaques de type feu : puissance diminué de 50%
			if (type === 'Fire') {
				power /= 2;
			}
			break;

		case 'Wonder Guard':
			// Ne peut être touché que par les attaques super efficaces
			if (effectiveness < 2) {
				effectiveness = 0;
			}
			break;
	}

	// Offensifs
	switch (offensiveAbility) {
		case 'Adaptability':
			// Si l'attaque est stabbée alors son stab vaut 2 au lieu de 1.5
			if (offensivePokemon.getStab(move) === 1.5) {
				stab *= 4 / 3;
			}
			break;

		case "Dragon's Maw":
			// Si l'attaque est de type dragon, alors la statistique d'attaque du pokemon est augmenté de 50%
			if (type === 'Dragon') {
				attack *= 1.5;
			}
			break;

		case 'Iron Fist':
			// Attaques de poing : puissance augmenté de 20%
			if (move.flags && move.flags.punch && move.flags.punch === 1) {
				power *= 1.2;
			}
			break;

		case 'Pixilate':
			// Si l'attaque est de type normal, elle devient de type fée et augmente sa puissance de 20%
			if (type === 'Normal') {
				power *= 1.2;
				// On recalcule l'efficacité pour une attaque glace
				effectiveness = 1;
				defensivePokemon.types.forEach((typeDef) => {
					effectiveness *= types[typeDef.toLowerCase()]['fairy'];
				});

				// Si je suis de type fée et que l'attaque n'était pas stabbé, alors elle le deviens
				if (stab === 1 && offensivePokemon.isType('Fairy')) {
					multiplier *= 1.5;
				}
				// Sinon si je ne suis pas de type fée et que l'attaque était stabbé, alors elle ne l'est plus
				else if (stab === 1.5 && !offensivePokemon.isType('Fairy')) {
					multiplier *= 2 / 3;
				}
			}
			break;

		case 'Punk Rock':
			// Attaques sonores : dégats diminués de 50%
			if (move.flags && move.flags.sound && move.flags.sound === 1) {
				multiplier *= 1.3;
			}
			break;

		case 'Refrigerate':
			// Si l'attaque est de type normal, elle devient de type glace et sa puissance augmente de 20%
			if (type === 'Normal') {
				power *= 1.2;
				// On recalcule l'efficacité pour une attaque glace
				effectiveness = 1;
				defensivePokemon.types.forEach((typeDef) => {
					effectiveness *= types[typeDef.toLowerCase()]['ice'];
				});

				// Si je suis de type glace et que l'attaque n'était pas stabbé, alors elle le deviens
				if (stab === 1 && offensivePokemon.isType('Ice')) {
					multiplier *= 1.5;
				}
				// Sinon si je ne suis pas de type glace et que l'attaque était stabbé, alors elle ne l'est plus
				else if (stab === 1.5 && !offensivePokemon.isType('Ice')) {
					multiplier *= 2 / 3;
				}
			}
			break;

		case 'Sheer Force':
			// Attaques avec effets secondaires : puissance augmenté de 30%
			if (move.secondary) {
				power *= 1.3;
			}
			break;

		case 'Steelworker':
			// Si l'attaque est de type acier, alors la statistique d'attaque du pokemon est augmenté de 50%
			if (type === 'Steel') {
				attack *= 1.5;
			}
			break;

		case 'Technician':
			// Les attaques de puissance maximum 60 sont augmentés de 50%
			if (power <= 60) {
				power *= 1.5;
			}
			break;

		case 'Tough Claws':
			// Les attaques de contact ont puissance augmenté de 30%
			if (move.flags && move.flags.contact && move.flags.contact === 1) {
				power *= 1.3;
			}
			break;

		case 'Transistor':
			// Si l'attaque est de type électrique, alors la statistique d'attaque du pokemon est augmenté de 50%
			if (type === 'Electric') {
				attack *= 1.5;
			}
			break;

		case 'Water Bubble':
			// Attaques de type eau : puissance doublée
			if (type === 'Water') {
				power *= 2;
			}
			break;

		default:
			break;
	}

	// ?    3. Moves ==============================================================================================

	switch (move.name) {
		case 'Body Press':
			attack = offensivePokemon.getStat('def');
			break;

		case 'Darkest Lariat':
			defense *=
				defensivePokemon.getStat('def', '', ['no-boost']) /
				defensivePokemon.getStat('def');
			break;

		case 'Facade':
			if (
				['burned', 'paralysed', 'toxic', 'poisoned'].includes(
					offensivePokemon.status
				)
			) {
				power = 140;
			}
			break;

		case 'Foul Play':
			attack = defensivePokemon.getStat('atk');
			break;

		case 'Freeze-Dry':
			if (defensivePokemon.isType('Water')) {
				effectiveness *= 4;
			}
			break;

		case 'Gyro Ball':
			power = Math.min(
				Math.floor(
					(25 * defensivePokemon.getStat('spe')) /
						offensivePokemon.getStat('spe') +
						1
				),
				150
			);
			break;

		case 'Heavy Slam':
			switch (
				Math.floor(
					offensivePokemon.weightkg / defensivePokemon.weightkg
				)
			) {
				case 4:
					power = 100;
					break;
				case 3:
					power = 80;
					break;
				case 2:
					power = 60;
					break;

				default:
					if (
						Math.floor(
							offensivePokemon.weightkg /
								defensivePokemon.weightkg
						) >= 5
					) {
						power = 120;
					} else {
						power = 20;
					}
					break;
			}
			break;

		case 'Hex':
			if (defensivePokemon.getStatus().name !== 'healthy') {
				power *= 2;
			}
			break;

		case 'Knock Off':
			if (defensivePokemon.getItem().toLowerCase() !== 'no item') {
				multiplier *= 1.5;
			}
			break;

		case 'Meteor Beam':
			attack *= 1.5;
			break;

		case 'Psyshock':
			defense = defensivePokemon.getStat('def');
			break;
	}

	// ? Finalisation ==============================================================================================

	multiplier *= effectiveness * stab;

	/** @type{number} - Nombre de PV perdus */
	let pvLoose = damageCalculator(level, attack, power, defense, multiplier);

	// ? Attaques à dégats fixes
	switch (move.name) {
		case 'Seismic Toss':
			pvLoose = offensivePokemon.level;
			break;
		case 'Night Shade':
			pvLoose = offensivePokemon.level;
			break;

		default:
			break;
	}

	if (options.includes('hp')) {
		return pvLoose;
	}

	// On retourne le pourcentage de dégats
	const percentageLoose =
		Math.floor((1000 * pvLoose) / defensivePokemon.getStat('hp')) / 10;
	return percentageLoose;
}

/**
 * - Renvoie la valeur de la statistique en prenant en compte les ev, iv
 * @param {Pokemon} pokemon
 * @param {string} stat
 * @returns {number}
 */
function getStat(pokemon, stat) {
	const nature = pokemon.nature;
	const base = +pokemon.baseStats[stat];
	const ev = +pokemon.ev[stat];
	const iv = +pokemon.iv[stat];
	const level = +pokemon.level;
	return calcStat(stat, base, iv, ev, level, nature);
}

/**
 * - Renvoie le set le plus populaire du tier correspondant au pokemon
 * @param {string} pokemonName - Nom du pokemon
 * @returns {Object[]} - Liste de move
 */
function getSet(pokemonName, tier = 'ru') {
	let name = formatName(pokemonName);

	// ? - Mauvais sets
	switch (name) {
		case 'Cloyster':
			console.log(1);
			set = {
				ability: 'Skill Link',
				evs: { atk: 252, spe: 252, spa: 4 },
				item: "King's Rock",
				level: 100,
				moves: [
					'Shell Smash',
					'Rock Blast',
					'Icicle Spear',
					'Hydro Pump',
				],
				nature: 'Naive',
			};
			return set;

		case 'Grapploct':
			set = {
				ability: 'Technician',
				evs: { hp: 252, def: 252, atk: 4 },
				item: 'Leftovers',
				level: 100,
				moves: ['Octolock', 'Protect', 'Drain Punch', 'Liquidation'],
				nature: 'Impish',
			};
			return set;

		case 'Kingdra':
			set = {
				ability: 'Swift Swim',
				evs: { spa: 252, spe: 252, spd: 4 },
				item: 'Choice Specs',
				level: 100,
				moves: ['Hydro Pump', 'Draco Meteor', 'Hurricane', 'Ice Beam'],
				nature: 'Modest',
			};
			return set;

		case 'Lopunny':
			set = {
				ability: 'Limber',
				evs: { atk: 252, spe: 252, spd: 4 },
				item: 'Life Orb',
				level: 100,
				moves: ['Fake Out', 'Close Combat', 'Return', 'U-turn'],
				nature: 'Jolly',
			};
			return set;

		case 'Togekiss':
			set = {
				ability: 'Serene Grace',
				evs: { hp: 248, spe: 252, spd: 8 },
				item: 'Leftovers',
				level: 100,
				moves: ['Air Slash', 'Roost', 'Defog', 'Thunder Wave'],
				nature: 'Timid',
			};
			return set;

		case 'Tyrantrum':
			set = {
				ability: 'Rock Head',
				evs: { atk: 252, spe: 252, spd: 4 },
				item: 'Choice Scarf',
				level: 100,
				moves: ['Head Smash', 'Close Combat', 'Earthquake', 'Outrage'],
				nature: 'Jolly',
			};
			return set;
	}

	// Cas spéciaux
	if (pokemonName === 'Polteageist-Antique') {
		name = formatName('Polteageist');
	}
	if (pokemonName === 'Toxtricity-Low-Key') {
		name = formatName('Toxtricity-Low-Key');
	}

	/** @type {number} - L'index du tier joué dans la liste (0 pour uber, 1 pour overused ...) */
	let tierIndex;
	for (let i = 0; i < mainTiers.length; i++) {
		if (tier === mainTiers[i]) {
			tierIndex = i;
		}
	}

	// Pour chaque tier, si on trouve un set alors on le renvoie
	let sets;
	for (let i = tierIndex; i >= 0; i--) {
		const _tier = mainTiers[i];
		switch (_tier) {
			case 'ubers':
				sets = ubersSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
			case 'ou':
				sets = ouSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
			case 'uu':
				sets = uuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
			case 'ru':
				sets = ruSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
			case 'nu':
				sets = nuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
			case 'zu':
				sets = zuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];
					return setCopy;
				}
				break;
		}
	}

	// Pour chaque tier
	for (let i = tierIndex + 1; i < mainTiers.length; i++) {
		const _tier = mainTiers[i];
		let sets;
		switch (_tier) {
			case 'ubers':
				sets = ubersSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
			case 'ou':
				sets = ouSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
			case 'uu':
				sets = uuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
			case 'ru':
				sets = ruSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
			case 'nu':
				sets = nuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
			case 'zu':
				sets = zuSets[0].stats[name];
				if (sets) {
					let setCopy = sets['Showdown Usage'];

					return setCopy;
				}
				break;
		}
	}

	// Si on n'a rien trouvé on renvoie un set créé personnelement

	const pokemon = getPokemon(pokemonName);

	// ? - Par défaut on renvoit le set avec les plus puissantes attaques en un tour
	let moves = [];

	// Pour chaque type
	typesList.forEach((type) => {
		// Si la meilleur attaque de ce type fais des dégats
		if ((bestAttack = getBestAttack(pokemon, type))) {
			// Si on n'a pas encore 4 moves alors on l'ajoute
			if (moves.length < 4) {
				moves.push(bestAttack);
			}
			// Sinon, si elle fais plus de dégats que la pire des 4 autres on les échange
			else {
				let minIndex = 0;
				// On récupère la pire des 4
				for (let i = 1; i < 4; i++) {
					if (pokemon.types.includes(moves[i].type)) {
						stabI = 1.5;
					} else {
						stabI = 1;
					}
					if (pokemon.types.includes(moves[minIndex].type)) {
						stabMinIndex = 1.5;
					} else {
						stabMinIndex = 1;
					}

					if (
						stabI * getBasePower(moves[i], pokemon) <
						stabMinIndex * getBasePower(moves[minIndex], pokemon)
					) {
						minIndex = i;
					}
				}

				// On gère le stab
				if (pokemon.types.includes(bestAttack.type)) {
					stabBA = 1.5;
				} else {
					stabBA = 1;
				}
				if (pokemon.types.includes(moves[minIndex].type)) {
					stabMinIndex = 1.5;
				} else {
					stabMinIndex = 1;
				}

				if (
					stabMinIndex * getBasePower(moves[minIndex], pokemon) <
					stabBA * getBasePower(bestAttack, pokemon)
				) {
					moves[minIndex] = bestAttack;
				}
			}
		}
	});

	// On enregistre juste le nom de chaque move
	moves = moves.map((move) => move.name);

	console.info("getSet n'a rien trouvé pour : ", pokemon);

	// ? - Les sets manquants
	let defautSet;
	switch (name.toLowerCase()) {
		case 'abomasnow':
			defautSet = {
				ability: 'Snow Warning',
				evs: { spa: 252, spe: 4, hp: 252 },
				item: 'Life Orb',
				level: 100,
				moves: ['Blizzard', 'Wood Hammer', 'Focus Blast', 'Ice Shard'],
				nature: 'Modest',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'aromatisse':
			defautSet = {
				ability: 'Aroma Veil',
				evs: { hp: 252, spd: 4, def: 252 },
				item: 'Leftovers',
				level: 100,
				moves: ['Moonblast', 'Wish', 'Protect', 'Heal Bell'],
				nature: 'Bold',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'drampa':
			defautSet = {
				ability: 'Berserk',
				evs: { spa: 252, def: 4, hp: 252 },
				item: 'Assault Vest',
				level: 100,
				moves: [
					'Draco Meteor',
					'Hyper Voice',
					'Fire Blast',
					'Hydro Pump',
				],
				nature: 'Bold',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'Gourgeist':
			defautSet = {
				ability: 'Frisk',
				evs: { atk: 252, def: 4, spe: 252 },
				item: 'Life Orb',
				level: 100,
				moves: [
					'Power Whip',
					'Poltergeist',
					'Shadow Sneak',
					'Fire Blast',
				],
				nature: 'Jolly',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'Liepard':
			defautSet = {
				ability: 'Prankster',
				evs: { hp: 252, spd: 4, spe: 252 },
				item: 'Leftovers',
				level: 100,
				moves: ['Knock Off', 'U-turn', 'Foul Play', 'Copycat'],
				nature: 'Jolly',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'mesprit':
			defautSet = {
				ability: 'Levitate',
				evs: { spa: 252, spe: 252, def: 4 },
				item: 'Choice Scarf',
				level: 100,
				moves: ['Psychic', 'U-turn', 'Dazzling Gleam', 'Healing Wish'],
				nature: 'Timid',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'palossand':
			defautSet = {
				ability: 'Water Compaction',
				evs: { def: 252, spd: 4, hp: 252 },
				item: 'Colbur Berry',
				level: 100,
				moves: [
					'Stealth Rock',
					'Earth Power',
					'Shadow Ball',
					'Shore Up',
				],
				nature: 'Bold',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'regirock':
			defautSet = {
				ability: 'Clear Body',
				evs: { hp: 252, def: 232, spd: 24 },
				item: 'Leftovers',
				level: 100,
				moves: [
					'Stealth Rock',
					'Stone Edge',
					'Body Press',
					'Thunder Wave',
				],
				nature: 'Timid',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'scyther':
			defautSet = {
				ability: 'Technician',
				evs: { atk: 252, spe: 252, def: 4 },
				item: 'Heavy-Duty Boots',
				level: 100,
				moves: ['Swords Dance', 'U-turn', 'Dual Wingbeat', 'Roost'],
				nature: 'Jolly',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'sidrfetch’d':
			defautSet = {
				ability: 'Scrappy',
				evs: { atk: 252, def: 4, spe: 252 },
				item: 'Choice Band',
				level: 100,
				moves: [
					'Close Combat',
					'Knock Off',
					'First Impression',
					'Brave Bird',
				],
				nature: 'Adamant',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;

		case 'vanilluxe':
			defautSet = {
				ability: 'Snow Warning',
				evs: { spa: 252, def: 4, spe: 252 },
				item: 'Focus Sash',
				level: 100,
				moves: ['Aurora Veil', 'Blizzard', 'Freeze-Dry', 'Toxic'],
				nature: 'Timid',
			};
			console.info(`Set par défaut de ${pokemonName} :`, defautSet);
			return defautSet;
	}

	// Par défaut on renvoie un set avec les restes qui favorise sa meilleure attaque et sa vitesse
	if (pokemon.baseStats.atk >= pokemon.baseStats.spa) {
		const defautSet = {
			ability: pokemon.abilities[0],
			evs: { atk: 252, def: 4, spe: 252 },
			item: 'Leftovers',
			level: 100,
			moves: moves,
			nature: 'Jolly',
		};
		console.info(`Set par défaut de ${pokemonName} :`, defautSet);
		return defautSet;
	} else {
		const defautSet = {
			ability: pokemon.abilities[0],
			evs: { spa: 252, def: 4, spe: 252 },
			item: 'Leftovers',
			level: 100,
			moves: moves,
			nature: 'Timid',
		};
		console.info(`Set par défaut de ${pokemonName} :`, defautSet);
		return defautSet;
	}
}

/**
 * - Renvoie l'attaque la plus puissante qu'apprend le pokemon pour le type demandé, on ignore les attaques en plusieurs tours ainsi que celles dont la précision ne dépasse pas les 50% et les moves suicides.
 * @param {Pokemon} pokemon
 * @param {string} type
 * @returns {Move}
 */
function getBestAttack(pokemon, type) {
	let bestMove;
	let name = formatName(pokemon.name);

	// Pour chaque move apprenable
	getLearnSet(name).forEach((move) => {
		// S'il est du type désiré, fait des dégats, est en 1 tour,  a une précision > 50% ne tue pas l'utilisateur
		if (
			move.type.toLowerCase() === type.toLowerCase() &&
			move.category !== 'Status' &&
			isOneTurn(move) &&
			move.accuracy > 50 &&
			!move.selfdestruct &&
			move.name !== 'Dream Eater'
		) {
			// Si on a déjà un move
			if (bestMove) {
				// Si les dégats sont supérieurs
				if (
					getBasePower(move, pokemon) >
					getBasePower(bestMove, pokemon)
				) {
					// Alors on les échange
					bestMove = move;
				}
			} else {
				bestMove = move;
			}
		}
	});
	return bestMove;
}

function getSetOld(pokemonName, tier) {
	let name = formatName(pokemonName);

	/** @type {number} - L'index du tier joué dans la liste (0 pour uber, 1 pour overused ...) */
	let tierIndex;
	for (let i = 0; i < mainTiers.length; i++) {
		if (tier === mainTiers[i]) {
			tierIndex = i;
		}
	}

	let set;
	for (let i = tierIndex; i >= 0; i--) {
		if (sets[`gen8${mainTiers[i]}`]['dex'][name]) {
			set = sets[`gen8${mainTiers[i]}`]['dex'][name];
		}
	}
	for (let i = tierIndex + 1; i < mainTiers.length; i++) {
		if (sets[`gen8${mainTiers[i]}`]['dex'][name]) {
			set = sets[`gen8${mainTiers[i]}`]['dex'][name];
		}
	}
	if (!set) {
	} else {
		const pokemon = getPokemon(name);
		let stratname;
		for (const key in set) {
			stratName = key;
		}
		return set[stratName];
	}
}

function getLearnSet(pokemonName) {
	let formatName = pokemonName;
	if (pokemonName === 'Sirfetch’d') {
		formatName = 'sirfetchd';
	}

	let pokemonMoveList;
	for (const name in learnSetsList) {
		if (
			formatName
				.replaceAll('-', '')
				.replaceAll("'", '')
				.replaceAll("'", '')
				.toLowerCase() === name
		) {
			pokemonMoveList = learnSetsList[name]['learnset'];
		}
	}
	const res = [];
	let moveIsAdded = false;
	for (const moveName in pokemonMoveList) {
		// Moves manquants
		if (
			pokemonName === 'Steelix' &&
			moveName.toUpperCase() > 'Head Smash'.toUpperCase() &&
			!moveIsAdded
		) {
			res.push(getMove('Head Smash'));
			moveIsAdded = true;
		}

		if (
			pokemonName === 'Espeon' &&
			moveName.toUpperCase() > 'Dark Pulse'.toUpperCase() &&
			!moveIsAdded
		) {
			res.push(getMove('Dark Pulse'));
			moveIsAdded = true;
		}

		res.push(movesList[moveName]);
	}

	return res;
}

function getAbility(abilityName) {
	const name = abilityName
		.replaceAll('-', '')
		.replaceAll("'", '')
		.replaceAll("'", '')
		.replaceAll(' ', '')
		.toLowerCase();

	if (BattleAbilities[name]) {
		return BattleAbilities[name];
	}
	console.error(`getAbility(${abilityName}) n'a rien trouvé`);
}

/**
 * - Renvoie la puissance de base de l'attaque utilisé par le pokemon (pour les attaque à coups multiples)
 * @param {Move} move
 * @param {Pokemon} pokemon
 * @returns {number}
 */
function getBasePower(move, pokemon) {
	switch (move.name) {
		case 'Triple Axel':
			return 120;

		default:
			// Si c'est un move à plusieurs coups
			if (move.multihit) {
				// Si le nombre de coup est fixe
				if (typeof move.multihit === 'number') {
					return move.basePower * move.multihit;
				}
				// Sinon
				else {
					// On regarde si le pokemon peut avoir la capacité Skill Link (max coups)
					let hasSkillLink = false;
					if (pokemon.abilities) {
						for (const key in pokemon.abilities) {
							hasSkillLink =
								hasSkillLink ||
								pokemon.abilities[key] === 'Skill Link';
						}
					} else if (pokemon.abilitiesList) {
						hasSkillLink = pokemon.abilitiesList.includes(
							'Skill Link'
						);
					}

					if (hasSkillLink) {
						return move.basePower * Math.max(...move.multihit);
					} else {
						return (
							move.basePower * 3.1
							// (2 * 0.35 + 3 * 0.35 + 4 * 0.15 + 5 * 0.15)
						);
					}
				}
			} else {
				return move.basePower;
			}
	}
}

/**
 * - Renvoie l'objet demandé
 * @param {string|object} item
 * @returns {Item}
 */
function getItem(item) {
	if (typeof item === 'object') {
		return getItem(item.name);
	}
	if (typeof item === 'undefined') {
		return getItem('No item');
	}

	for (const name in itemsList) {
		if (
			item
				.toLowerCase()
				.replaceAll(' ', '-')
				.replaceAll("'", '')
				.replaceAll('-', '')
				.replaceAll("'", '') ===
			name
				.toLowerCase()
				.replaceAll(' ', '-')
				.replaceAll("'", '')
				.replaceAll('-', '')
				.replaceAll("'", '')
		) {
			return itemsList[name];
		}
	}
	console.error(`getItem(${item}) n'a rien trouvé pour:`, item);
}

function getMove(moveName) {
	if (typeof moveName === 'object') {
		_name = moveName.name;
	} else {
		_name = moveName;
	}
	if (_name) {
		for (const name in movesList) {
			if (
				_name
					.replaceAll('-', '')
					.replaceAll(' ', '')
					.replaceAll("'", '')
					.toLowerCase() === name
			) {
				return movesList[name];
			}
		}
	}
	console.error(`getMove(${_name}) n'a rien trouvé`, _name);
}

function displayMove(move) {
	const moveDiv = document.createElement('div');
	moveDiv.classList.add('move');

	const moveName = document.createElement('a');
	moveName.classList.add('move-name');
	moveName.innerText = move.name;
	moveName.href = `https://www.smogon.com/dex/ss/moves/${move.name.toLowerCase()}/`;
	moveName.setAttribute('target', '_blank');

	const movePower = document.createElement('p');
	movePower.classList.add('move-power');
	if (move.basePower > 0) {
		movePower.innerText = move.basePower;
	}

	// On affiche la couleur correspondant au type du move
	moveDiv.classList.add(
		'type',
		'first-type',
		'last-type',
		move['type'].toLowerCase()
	);

	// On affiche l'icône correspondant au move
	const moveIcon = document.createElement('img');
	moveIcon.classList.add('move-icon');
	moveIcon.src = `sources/${move['category'].toLowerCase()}.svg`;

	moveDiv.appendChild(moveName);
	moveDiv.appendChild(movePower);
	moveDiv.appendChild(moveIcon);
	return moveDiv;
}

function selectText(HTMLElement) {
	HTMLElement.focus();
	HTMLElement.select();
}

function statToUpperCase(statName) {
	switch (statName) {
		case 'hp':
			return 'Hp';
		case 'atk':
			return 'Atk';
		case 'def':
			return 'Def';
		case 'spa':
			return 'SpA';
		case 'spd':
			return 'SpD';
		case 'spe':
			return 'Spe';

		default:
			break;
	}
}

function formatDesc(desc) {
	let res = '';
	for (let i = 0; i < desc.length; i++) {
		if (desc[i] === ';') {
			res += `.<br/>${desc[i + 2].toUpperCase()}`;
			i += 2;
		} else {
			res += desc[i];
		}
	}
	return res;
}

/**
 * - Renvoie le nom passé en paramètre ou légèrement pour les cas spéciaux (sylvally)
 * @param {string} pokemonName
 * @returns {string}
 */
function formatName(pokemonName) {
	if (pokemonName === 'Gastrodon-East') {
		return 'Gastrodon';
	}
	if (pokemonName === 'Kommo-o') {
		return 'Kommoo';
	}
	if (pokemonName === 'Polteageist-Antique') {
		return 'Polteageist';
	}
	if (pokemonName === 'Silvally-*' || pokemonName === 'Silvally-%2A') {
		return 'Silvally';
	}
	if (pokemonName.substring(0, 8) === 'Sirfetch') {
		return 'Sirfetch’d';
	}
	if (pokemonName === 'Toxtricity-Low-Key') {
		return 'Toxtricity';
	}
	return capitalize(pokemonName);
}

/**
 * - Renvoie un objet comportant name , type , desc et display. Tous des strings.
 * @param {string} statusName
 * @returns {object}
 */
function getStatus(statusName) {
	for (let i = 0; i < statusList.length; i++) {
		const status = statusList[i];
		if (status.name.toLowerCase() === statusName.toLowerCase()) {
			return status;
		}
	}
	console.error(`Impossible de faire getStatus(${statusName})`);
}

/**
 * - Renvoie vrai si l'objet est à utilisation unique, faux sinon.
 * @param {object} item
 * @returns {boolean}
 */
function isSingleUse(item) {
	let _item = item;
	if (typeof item === 'string') {
		_item = getItem(item);
	}
	return item.desc.toLowerCase().includes('single use');
}

/**
 * - Return boost in an object
 * @param {Pokemon} pokemon
 * @returns {Boost} - Un objet de type boost
 */
function getAbilityBoost(pokemon) {
	const ability = pokemon.ability;
	const boost = {
		atk: 0,
		def: 0,
		spa: 0,
		spd: 0,
		spe: 0,
	};

	switch (ability) {
		case 'Beast Boost':
			boost[pokemon.getBestStat()] = 1;
			break;

		case 'Competitive':
			boost.spa = 2;
			break;

		case 'Justified':
			boost.atk = 1;
			break;

		case 'Moxie':
			boost.atk = 1;
			break;

		case 'Sap Sipper':
			boost.atk = 1;
			break;

		case 'Speed Boost':
			boost.spe = 1;
			break;

		case 'Storm Drain':
			boost.spa = 1;
			break;

		case 'Unburden':
			boost.spe = 2;
			break;

		case 'Weak Armor':
			boost.def = -1;
			boost.spe = 2;
			break;
	}

	return boost;
}

/**
 * - Return boost in an object
 * @param {string} item - item's name
 * @returns {Boost} - Un objet de type boost
 */
function getItemBoost(item) {
	const boost = {
		atk: 0,
		def: 0,
		spa: 0,
		spd: 0,
		spe: 0,
	};

	switch (item) {
		case 'Weakness Policy':
			boost.atk = 2;
			boost.spa = 2;
			console.log(1);
			break;
	}

	return boost;
}

/**
 *
 * @param {string|object} item - nom de l'item
 * @returns
 */
function getItemStatus(item) {
	let name = item;
	if (typeof item === 'object') {
		name = item.name;
	}

	switch (name) {
		case 'Flame Orb':
			return 'burned';

		default:
			return 'healthy';
	}
	// burn
	// poisoned
	// toxic
	// burned
	// paralysed
	// asleep
	// frozen
}

function getEffectiveness(offensivePokemon, defensivePokemon, move) {
	/** @type{string} - Le type de l'attaque (Fire, ...) */
	let moveType =
		move.name === 'Multi-Attack'
			? offensivePokemon.types[0].toLowerCase()
			: move['type'].toLowerCase();

	let effectiveness = 1;
	defensivePokemon.types.forEach((typeDef) => {
		if (
			offensivePokemon.ability === 'Scrappy' &&
			['Fighting', 'Normal'].includes(move.type) &&
			typeDef === 'Ghost'
		) {
			effectiveness *= 1;
		} else {
			effectiveness *= types[typeDef.toLowerCase()][moveType];
		}
	});
	return effectiveness;
}

/**
 * - Renvoie vrai si l'attaque ne nécéssite qu'un seul tour, faux sinon.
 * @param {Move} move
 * @returns {boolean}
 */
function isOneTurn(move) {
	// Renvoie vrai si le move n'a ni le flag charge, ni recharge, ni le status volatile lockedmove
	return !(
		(move.flags && (move.flags.charge || move.flags.recharge)) ||
		(move.self && move.self.volatileStatus === 'lockedmove') ||
		(move.condition && move.condition['duration'])
	);
}
