document.addEventListener('DOMContentLoaded', () => {
	const clearButtons = document.getElementsByClassName('clear-button');
	const importCheckbox = document.getElementById('import-checkbox');
	importCheckbox.checked = false;
	const importInput = document.getElementById('import-input');
	const submitButton = document.getElementById('submit-button');

	// ? - Quand on clique sur clear on vide l'équipe
	for (const clearButton of clearButtons) {
		clearButton.addEventListener('click', () => {
			const teamId = getTeamId(clearButton);
			const team = document.getElementById(`team-${teamId}`);
			const textArea = team.getElementsByTagName('textarea')[0];
			const importDiv = team.getElementsByClassName('import')[0];

			// On efface le texte
			textArea.value = '';

			// On efface le fichier sélectionné s'il y en a un
			if (importDiv) {
				importInput.value = '';
			}

			// On met le focus sur le textarea
			textArea.focus();
		});
	}

	// ? - Quand on clique sur import on masque un input et ajoute required à l'autre
	importCheckbox.addEventListener('change', () => {
		const textArea = document
			.getElementById('team-1')
			.getElementsByTagName('textarea')[0];

		// Si on a coché
		if (importCheckbox.checked) {
			importInput.required = true;
			importInput.classList.remove('no-display');
			textArea.required = false;
			textArea.classList.add('no-display');
		}
		// Si on a décoché
		else {
			importInput.required = false;
			importInput.classList.add('no-display');
			textArea.required = true;
			textArea.classList.remove('no-display');
		}
	});
});

/**
 * - Retourne l'id de l'équipe du bouton
 * @param {HTMLButtonElement} button
 * @returns {number}
 */
function getTeamId(button) {
	for (let i = 1; i <= 2; i++) {
		if (button.classList.contains(i)) {
			return i;
		}
	}
	console.error(`getTeamId(button) n'a rien trouvé pour:`, button);
}

function trimForm() {
	const textareas = document.getElementsByTagName('textarea');
	for (let i = 0; i < textareas.length; i++) {
		const textarea = textareas[i];
		textarea.value = textarea.value.replaceAll(' ', '');
		textarea.value = textarea.value.replaceAll('\n', '');
		textarea.value = textarea.value.replaceAll('/', ' / ');
	}
}
