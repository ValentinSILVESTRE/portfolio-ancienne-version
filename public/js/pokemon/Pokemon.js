/** class representing a pokemon */
class Pokemon {
	/**
	 * - Constructeur de pokemon
	 * @param {string} pokemonName - Le nom du pokemon avec majuscules
	 * @param {string} tier - le tier joué
	 * @param {PokemonSet} importData - les données que l'on souhaite importer
	 */
	constructor(pokemonName, tier, importData = {}) {
		/** @type {string} - Nom du pokemon */
		this.name = formatName(pokemonName);
		// this.name = pokemonName;

		/** @type {boolean} - Vrai si le pokemon a utilisé roost à ce tour, faux sinon */
		this.roosted = false;

		/** @type {boolean} - Vrai si le pokemon a toujours son objet, faux sinon */
		this._hasItem = true;

		/** @type {string[]} - Liste des types du pokemon */
		this.types = getPokemon(this.name)['types'];

		/** @type {Object} - Objet contenant les statistiques de base du pokemon */
		this.baseStats = getPokemon(this.name)['baseStats'];

		/** @type {number} - Taille du pokemon */
		this.heightm = getPokemon(this.name)['heightm'];

		/** @type {number} - Poids du pokemon */
		this.weightkg = getPokemon(this.name)['weightkg'];

		/** @type {string} - Tier du pokemon */
		this.tier = tier;

		/** @type {string[]} - Liste des capacités du pokemon */
		this.abilitiesList = [];
		for (const ab in getPokemon(this.name)['abilities']) {
			this.abilitiesList.push(getPokemon(this.name)['abilities'][ab]);
		}

		/** @type {Move[]} - Liste des attaques que peut apprendre pokemon */
		this.learnMoves = getLearnSet(this.name);

		/** @type {PokemonSet} - Set du pokemon */
		this.set = getSet(this.name, tier);

		/** @type {String[]} - Liste des attaques du set du pokemon */
		this.moves = this.set.moves;
		// this.moves = [...this.set.moves];

		/** @type {string} - Capacité du pokemon */
		this.ability = this.set.ability;

		/** @type {Stats} - Ev du pokemon */
		this.ev = this.set.evs;
		this.ev = {
			hp: this.ev.hp > 0 ? this.ev.hp : 0,
			atk: this.ev.atk > 0 ? this.ev.atk : 0,
			def: this.ev.def > 0 ? this.ev.def : 0,
			spa: this.ev.spa > 0 ? this.ev.spa : 0,
			spd: this.ev.spd > 0 ? this.ev.spd : 0,
			spe: this.ev.spe > 0 ? this.ev.spe : 0,
		};

		/** @type {string} - Objet du pokemon */
		this.item = this.set.item;

		/** @type {string} - Nature du pokemon */
		this.nature = this.set.nature;

		/** @type {Stats} - Iv du pokemon */
		this.iv = {
			hp: 31,
			atk: 31,
			def: 31,
			spa: 31,
			spd: 31,
			spe: 31,
		};

		/** @type {number} - Niveau du pokemon */
		this.level = 100;

		/** @type {Boost} - Boost du pokemon */
		this.boosts = {
			atk: 0,
			def: 0,
			spa: 0,
			spd: 0,
			spe: 0,
		};

		// A sa création, le pokemon n'a pas de problème de status
		this.setStatus('healthy');

		// ? - Sets spéciaux
		this.setException();

		// ? Importation
		for (const key in importData) {
			const value = importData[key];

			switch (key) {
				case 'item':
					this.item = value;
					break;

				case 'ability':
					this.ability = value;
					break;

				case 'nature':
					this.nature = value;
					break;

				case 'moves':
					this.moves = value;
					break;

				case 'iv':
					this.iv = value;
					break;

				case 'ev':
					this.ev = value;
					break;

				default:
					break;
			}
		}
	}

	/**
	 * - Modifie les données pour certains cas spéciaux
	 */
	setException() {
		switch (this.name.toLowerCase()) {
			case 'blastoise':
				this.moves[2] = 'Dark Pulse';
				break;

			case 'drifblim':
				this.item = 'Sitrus Berry';
				break;

			case 'flygon':
				this.moves[2] = 'Dragon Dance';
				break;

			case 'heracross':
				this.moves[2] = 'Stone Edge';
				break;

			case 'gastrodon':
				this.moves[3] = 'Ice Beam';
				break;

			case 'gigalith':
				this.moves[3] = 'Stone Edge';
				break;

			case 'grimmsnarl':
				this.item = 'Leftovers';
				break;

			case 'incineroar':
				this.moves = ['Knock Off', 'Overheat', 'Toxic', 'Parting Shot'];
				break;

			case 'milotic':
				this.moves[2] = 'Ice beam';
				break;

			case 'necrozma':
				this.moves = [
					'Meteor Beam',
					'Photon Geyser',
					'Heat Wave',
					'Autotomize',
				];
				break;

			case 'noivern':
				this.moves[2] = 'Hurricane';
				break;

			case 'registeel':
				this.moves[1] = 'Heavy Slam';
				break;

			case 'reuniclus':
				this.item = 'Leftovers';
				break;

			case 'salazzle':
				this.moves = [
					'Toxic',
					'Fire Blast',
					'Sludge Wave',
					'Nasty Plot',
				];
				break;

			case 'shiftry':
				this.ability = 'Chlorophyll';
				this.moves[3] = 'Solar Blade';
				break;

			case 'steelix':
				this.moves[0] = 'Body Press';
				this.moves[3] = 'Iron Defense';
				break;

			case 'volcanion':
				this.moves = [
					'Steam Eruption',
					'Fire Blast',
					'Earth Power',
					'Toxic',
				];
				break;

			default:
				break;
		}
	}

	/**
	 * - Renvoie vrai si le pokemon n'est pas de type vol et ne levite pas, faux sinon
	 * @returns {Boolean}
	 */
	isGrounded() {
		return !this.isType('Flying') && this.ability !== 'Levitate';
	}

	/**
	 * - Renvoie la valeur du boost
	 * @param {string} stat
	 * @returns {Number}
	 */
	getBoost(stat) {
		const statFormat = stat.toLowerCase();
		if (stat === 'hp') {
			return 0;
		}
		if (statsList.includes(statFormat)) {
			return this.boosts[stat];
		} else {
			console.error(`Impossible de faire getBoost(${stat})`);
		}
	}

	/**
	 * - Définit le boost de la statistique et ramène dans l'interval [-6; 6]
	 * @param {string} stat - Nom de la statistique
	 * @param {Number} value - Valeur entière du boost
	 */
	setBoost(stat, value) {
		const statFormat = stat.toLowerCase();
		if (['atk', 'def', 'spa', 'spd', 'spe'].includes(statFormat)) {
			let valueFormat = Math.floor(+value);
			// On prends la valeur comprise entre -6 et 6
			valueFormat = Math.min(6, valueFormat);
			valueFormat = Math.max(-6, valueFormat);

			this.boosts[statFormat] = valueFormat;
		} else {
			console.error(`Impossible de faire setBoost(${stat}, ${value})`);
		}
	}

	/**
	 * - Ajoute la valeur (possiblement négative) au boost
	 * @param {string} stat - Statistique
	 * @param {Number} value - La valeur à ajouter au boost
	 */
	addBoost(stat, value) {
		this.setBoost(stat, this.getBoost(stat) + value);
	}

	getWeightkg() {
		let weight = this.weightkg;

		// La capacité Heavy Metal double le poids
		if (this.ability.name === 'Heavy Metal') {
			weightkg *= 2;
		}
		return weight;
	}

	setStatus(statusName) {
		const status = getStatus(statusName);
		this.status = status.name;
	}

	getStatus() {
		let status;
		statusList.forEach((element) => {
			if (element.name === this.status) {
				status = element;
			}
		});
		return status;
	}

	/**
	 * - Définit le status du pokemon à normal (healthy)
	 */
	resetStatus() {
		this.setStatus('healthy');
	}

	/**
	 * - Renvoie vrai si le pokemon n'a pas de problème de status, faux si oui
	 * @returns {Boolean}
	 */
	isHealthy() {
		return this.status === 'healthy';
	}

	/**
	 * - Soigne le pokemon
	 */
	heal() {
		this.status = 'healthy';
	}

	/**
	 * - Définit le status du pokemon à burned
	 */
	burned() {
		this.setStatus('burned');
	}

	/**
	 * - Définit le status du pokemon à paralysed
	 */
	paralysed() {
		this.setStatus('paralysed');
	}

	/**
	 * - Renvoie la valeur de la statistique en prenant en compte les ev, iv
	 * @param {string} stat
	 * @param {String[]} options - si l'option 'no-boost' alors on ignore les boosts
	 * @returns {Number}
	 */
	getStat(stat, climate = '', options = []) {
		const nature = this.nature;
		const base = +this.baseStats[stat];
		const ev = +this.ev[stat];
		const iv = +this.iv[stat];
		const level = +this.level;
		let total = calcStat(stat, base, iv, ev, level, nature);

		if (typeof climate !== 'string') {
			climate = climate.value;
		}

		// Gestion des objets
		if (stat === 'spe' && this.getItem() === 'Choice Scarf') {
			total *= 1.5;
		}
		if (stat === 'spe' && this.getItem() === 'Iron Ball') {
			total *= 0.5;
		}
		if (['atk', 'spa'].includes(stat) && this.getItem() === 'Light Ball') {
			total *= 2;
		}

		// Gestion du status
		const status = this.getStatus();
		if (status.name === 'burned' && stat === 'atk') {
			total /= 2;
		}
		if (status.name === 'paralysed' && stat === 'spe') {
			total /= 2;
		}

		// Gestion des capacités
		switch (this.ability) {
			case 'Chlorophyll':
				// Vitesse doublée sous le soleil
				if (stat === 'spe' && climate === 'sun') {
					total *= 2;
				}
				break;

			case 'Guts':
				// Attaque augmenté de 50% si brulé, paralisé ou empoinsonné
				if (stat === 'atk') {
					if (status.name === 'burned') {
						total *= 3;
					}
					if (
						status.name === 'paralysed' ||
						status.name === 'poisoned' ||
						status.name === 'toxic'
					) {
						total *= 1.5;
					}
				}
				break;

			case 'Huge Power':
				// Attaque doublée
				if (stat === 'atk') {
					total *= 2;
				}
				break;

			case 'Hustle':
				// Attaque augmentée de 50%
				if (stat === 'atk') {
					total *= 1.5;
				}
				break;

			case 'Marvel Scale':
				// Défense augmenté de 50% si problème de status
				if (stat === 'def' && !this.isHealthy()) {
					total *= 1.5;
				}
				break;

			case 'Solar Power':
				// Attaque spéciale *1.5 sous le soleil
				if (stat === 'spa' && climate === 'sun') {
					total *= 1.5;
				}
				break;

			case 'Slush Rush':
				// Vitesse doublée sous la grêle
				if (stat === 'spe' && climate === 'hail') {
					total *= 2;
				}
				break;

			case 'Swift Swim':
				// Vitesse doublée sous la pluie
				if (stat === 'spe' && climate === 'rain') {
					total *= 2;
				}
				break;

			default:
				break;
		}

		// Gestion des boosts
		if (!options.includes('no-boost')) {
			const boost = this.getBoost(stat);
			if (boost >= 0) {
				total *= (2 + boost) / 2;
			} else {
				total *= 2 / (2 - boost);
			}
		}

		return Math.floor(total);
	}

	/**
	 * - Renvoie vrai si le pokemon a le type typeName
	 * @param {string} typeName - Nom du type
	 * @returns {Boolean}
	 */
	isType(typeName) {
		return this.types.includes(capitalize(typeName.toLowerCase()));
	}

	/**
	 * - Renvoie le stab de l'attaque :1 par défaut et  1.5 si le type de l'attaue est un type du pokemon
	 * @param {Object} move - Objet comportant type
	 * @returns {number}
	 */
	getStab(move) {
		if (this.isType(move.type)) {
			return 1.5;
		} else {
			return 1;
		}
	}

	/**
	 * @returns {string}
	 */
	getItem() {
		if (this.hasItem()) {
			return this.item;
		}
		return 'no item';
	}

	/**
	 * @param {string} item
	 */
	setItem(item) {
		if (typeof item === 'string') {
			this.item = item;
		}
		if (typeof item === 'object') {
			this.item = item.name;
		}
	}

	/**
	 * - Renvoie la meilleure statistique du pokemon en ignorant les boosts.
	 * @returns {string}
	 */
	getBestStat() {
		let bestStat = 'atk';
		['def', 'spa', 'spd', 'spe'].forEach((stat) => {
			if (
				this.getStat(stat, '', ['no-boost']) >
				this.getStat(bestStat, '', ['no-boost'])
			) {
				bestStat = stat;
			}
		});
		return bestStat;
	}

	/**
	 * - Renvoie vrai si le pokemon a son objet
	 * @returns {boolean}
	 */
	hasItem() {
		return this._hasItem;
	}

	/**
	 * - Enlève l'objet du pokemon (à la suite d'un sabotage ou pour les baies)
	 */
	looseItem() {
		this._hasItem = false;
	}

	getAbility() {
		return this.ability;
	}
}
