// ? - Move
/**
 * @typedef {Object} Move
 * @property {number} accuracy
 * @property {number} basePower
 * @property {string} category
 * @property {Flag} flags
 * @property {string} name
 * @property {number} priority
 * @property {Secondary?} secondary
 * @property {string} shortDesc
 * @property {string} type
 */

// ? - Secondary
/**
 * @typedef {Object} Secondary
 * @property {Boost?} boosts
 * @property {number} chance
 * @property {string?} status
 * @property {string?} volatileStatus
 */

// ? - Flag
/**
 * @typedef {Object} Flag
 * @property {1?} contact
 * @property {1?} protect
 * @property {1?} mirror
 * @property {1?} bypasssub
 * @property {1?} allyanim
 * @property {1?} reflectable
 * @property {1?} snatch
 * @property {1?} sound
 * @property {1?} punch
 */

// ? - Boost
/**
 * @typedef {Object} Boost
 * @property {number?} atk
 * @property {number?} def
 * @property {number?} spa
 * @property {number?} spd
 * @property {number?} spe
 */

// ? - Stats
/**
 * @typedef {Object} Stats
 * @property {number?} hp
 * @property {number?} atk
 * @property {number?} def
 * @property {number?} spa
 * @property {number?} spd
 * @property {number?} spe
 */

// ? - Item
/**
 * @typedef {Object} Item
 * @property {string} name
 * @property {string} desc
 */

// ? - PokemonSet
/**
 * @typedef {Object} PokemonSet
 * @property {string} ability
 * @property {Stats} evs
 * @property {Stats?} ivs
 * @property {string} item
 * @property {number} level
 * @property {string[]} moves
 * @property {string} nature
 */
