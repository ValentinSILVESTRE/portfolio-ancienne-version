document.addEventListener('DOMContentLoaded', () => {
	const team = myTeam;
	team.forEach((pokemon) => {
		document
			.getElementById('table')
			.appendChild(displayPokemon(pokemon.name));
	});
});

/**
 *
 * @param {String} pokemonName
 * @returns {HTMLElement} - La div contenant le pokemon
 */
function displayPokemon(pokemonName) {
	const pokemon = getPokemon(pokemonName);
	const line = document.createElement('div');
	line.classList.add('line', 'line-content');

	// Bloc 1 : Le preview

	const pokemonPreview = document.createElement('div');
	pokemonPreview.classList.add('pokemon-preview', 'pokemon');

	// 1.1 - l'image
	const sprite = document.createElement('img');
	sprite.classList.add('sprite');
	sprite.src = `https://play.pokemonshowdown.com/sprites/ani/${pokemonName.toLowerCase()}.gif`;

	// 1.2 bloc : les infos
	const pokemonInfos = document.createElement('div');
	pokemonInfos.classList.add('pokemon-infos');

	// 1.2.1 - Le nom
	const name = document.createElement('h3');
	name.classList.add('name');
	name.innerText = pokemon['name'];

	// 1.2.2 - Les types
	const typesDiv = document.createElement('div');
	typesDiv.classList.add('types');
	const type1 = document.createElement('div');
	type1.innerText = pokemon['types'][0];
	type1.classList.add('type');
	type1.classList.add('first-type');
	type1.classList.add(pokemon['types'][0].toLowerCase());
	typesDiv.appendChild(type1);

	if (pokemon['types'].length === 2) {
		const type2 = document.createElement('div');
		type2.classList.add('second-type');
		type2.classList.add('last-type');
		type2.innerText = pokemon['types'][1];
		type2.classList.add('type');
		type2.classList.add(pokemon['types'][1].toLowerCase());
		typesDiv.appendChild(type2);
	} else {
		type1.classList.add('last-type');
	}

	// On relie le preview
	// pokemonInfos.appendChild(name);
	pokemonInfos.appendChild(typesDiv);

	pokemonPreview.appendChild(sprite);
	pokemonPreview.appendChild(pokemonInfos);

	line.appendChild(pokemonPreview);

	// Blocs 2 à 19 : Les résistances
	typesList.forEach((typeName) => {
		const cell = document.createElement('div');
		cell.classList.add('cell');

		// On gère les types
		let coeffTypes = 1;
		pokemon['types'].forEach((typeDef) => {
			coeffTypes *= types[typeDef.toLowerCase()][typeName.toLowerCase()];
		});

		// * Cas des talents qui influent
		// 1 - Lévitation
		const abilityName = getSet(pokemon['name'])['ability'];
		if (abilityName === 'Levitate' && typeName === 'ground') {
			coeffTypes = 0;
		}

		if (coeffTypes !== 1) {
			cell.innerText = `×${coeffTypes}`;
		}

		switch (coeffTypes) {
			case 0:
				cell.classList.add('immune');
				break;
			case 0.25:
				cell.classList.add('very-low');
				break;
			case 0.5:
				cell.classList.add('low');
				break;
			case 1:
				cell.classList.add('neutral');
				break;
			case 2:
				cell.classList.add('high');
				break;
			case 4:
				cell.classList.add('very-high');
				break;

			default:
				break;
		}

		line.appendChild(cell);
	});
	return line;
}

function formatAttack(attackName) {
	let res = attackName.toLowerCase();
	res = res.replaceAll('-', '');
	res = res.replaceAll(' ', '');
	return res;
}

/**
 *
 * @param {number[]} list - Liste des dégats en pourcentages
 */
function displayDamages(list) {
	for (let i = 0; i < list.length; i++) {
		const damageDiv = document.getElementById(`damage-${i + 1}`);
		damageDiv.innerText = `${Math.floor(list[i] * 8.5) / 10}% ~ ${
			list[i]
		}%`;
	}
}

function resetDamages() {
	for (let i = 0; i < 6; i++) {
		document.getElementById(`damage-${i + 1}`).innerText = '';
	}
}
