document.addEventListener('DOMContentLoaded', () => {
	const internet = true;

	// ? - Récupération des équipes
	const debug = false;

	let _import;
	if (getCookie('team1import')) {
		_import = true;
	} else {
		_import = false;
	}

	/** @type{String} - Le tier joué en minuscules */
	let tier = 'ou';
	if (getCookie('tier')) {
		tier = getCookie('tier').toLowerCase();
	}

	// ? - Changement de climat
	const climateSelect = document.getElementById('climate');
	climateSelect.addEventListener('change', () => {
		// On met à jour tous les pokemons
		updateAllPokemons();
	});

	// ? - Terrain
	const terrainSelect = document.getElementById('terrain');

	const _teams = [];

	// Les valeurs de test
	teamA = [
		'Regieleki',
		'Grimmsnarl',
		'Cobalion',
		'Crobat',
		'Volcanion',
		'Necrozma',
	];
	teamB = [
		'Azumarill',
		'Porygon-Z',
		'Regidrago',
		'Umbreon',
		'Bewear',
		'Darmanitan',
		'Cobalion',
		'Grimmsnarl',
		'Gastrodon',
		'Roserade',
		'Milotic',
		'Necrozma',
		'Flygon',
		'Incineroar',
		'Weezing-Galar',
		'Togekiss',
		'Xurkitree',
		'Diancie',
		'Raikou',
		'Reuniclus',
		'Chandelure',
		'Metagross',
		'Toxtricity',
		'Volcanion',
		'Obstagoon',
		'Klefki',
	];

	// Les vrai valeurs venant du formulaire
	if (!debug) {
		teamA = team1;
		// teamA = getCookie('team1');
		teamB = team2;
		if (_import) {
			teamA = myTeam;
		}
	}

	_teams.push(teamA);
	_teams.push(teamB);

	const teams = [];
	const t1 = [];
	const t2 = [];
	teams.push(t1, t2);

	// ? - On affiche les deux équipes
	let pokemonDivEditHeight;
	let pokemonDivEditWidth;
	// Pour chaque équipe
	for (let teamIndex = 0; teamIndex < 2; teamIndex++) {
		const team = _teams[teamIndex];

		// Pour chaque pokémon
		team.forEach((pokemonName, pokemonIndex) => {
			let pokemon;
			if (teamIndex === 0 && _import) {
				importData = {
					item: team[pokemonIndex].item,
					ability: team[pokemonIndex].ability,
					nature: team[pokemonIndex].nature,
					moves: team[pokemonIndex].moves,
					ev: team[pokemonIndex].ev,
					iv: team[pokemonIndex].iv,
					level: team[pokemonIndex].level,
				};

				pokemon = new Pokemon(
					team[pokemonIndex].name,
					tier,
					importData
				);
			} else {
				pokemon = new Pokemon(pokemonName, tier);
			}
			teams[teamIndex].push(pokemon);

			// On affiche pokemon-show
			const pokemonDiv = displayPokemon(
				pokemon,
				teamIndex + 1,
				pokemonIndex
			);
			document
				.getElementById(`team${teamIndex + 1}`)
				.appendChild(pokemonDiv);
			pokemonDiv.id = `team${teamIndex + 1}-pokemon${pokemonIndex}`;

			// On affiche pokemon-edit
			const pokemonDivEdit = displayEditPokemon(pokemon, teamIndex + 1);
			document
				.getElementById(`team${teamIndex + 1}`)
				.appendChild(pokemonDivEdit);
			pokemonDivEdit.id = `team${
				teamIndex + 1
			}-pokemon${pokemonIndex}-edit`;
			pokemonDivEdit.style.opacity = 0;
			pokemonDivEditHeight = pokemonDivEdit.offsetHeight;
			pokemonDivEditWidth = pokemonDivEdit.offsetWidth;
			pokemonDivEdit.style.opacity = 1;
			pokemonDivEdit.style.display = 'none';

			// On positionne le pokemon edit au centre
			const top = Math.floor(
				(window.innerHeight - pokemonDivEditHeight) / 2
			);
			const left = Math.floor(
				(window.innerWidth - pokemonDivEditWidth) / 2
			);
			// pokemonDivEdit.style.top = `${top}px`;
			pokemonDivEdit.style.left = `${left}px`;

			// ? Gestion des boutons edit et save

			// Bouton edit
			const editButton = pokemonDiv.getElementsByClassName('edit')[0];
			editButton.addEventListener('click', () => {
				// On attend 100 ms que la page se charge
				setTimeout(() => {
					// On masque tous les pokemon show
					for (const pokemon of document.getElementsByClassName(
						'pokemon-show'
					)) {
						pokemon.style.display = 'none';
					}

					// On affiche le pokemon à éditer
					pokemonDivEdit.style.display = 'flex';
				}, 100);
			});

			// Bouton save
			const saveButton = pokemonDivEdit.getElementsByClassName('save')[0];
			saveButton.addEventListener('click', () => {
				console.log('save', pokemon);
				// On masque l'édition
				pokemonDivEdit.style.display = 'none';

				// On affiche tous les pokemon show
				for (const pokemon of document.getElementsByClassName(
					'pokemon-show'
				)) {
					pokemon.style.display = 'flex';
				}

				// On actualise l'affichage
				updatePokemonDiv(teamIndex + 1, pokemonIndex, teams);
			});
		});
	}

	// ? - Bouton Show stats
	// Quand on clique dessus on masque les infos et affiche les statistiques ou inversement
	const showStatsButton = document.getElementById('show-stats');
	showStatsButton.checked = false;
	showStatsButton.addEventListener('change', () => {
		const isChecked = showStatsButton.checked;
		// Si le bouton est coché alors on affiche les stats
		if (isChecked) {
			for (const pokemonShow of document.getElementsByClassName(
				'pokemon-show'
			)) {
				const infosDiv = pokemonShow.getElementsByClassName('infos')[0];
				infosDiv.classList.add('no-display');
				const statsDiv = pokemonShow.getElementsByClassName(
					'statsDiv'
				)[0];
				statsDiv.classList.remove('no-display');
			}
		}
		// Sinon on affiche les infos
		else {
			for (const pokemonShow of document.getElementsByClassName(
				'pokemon-show'
			)) {
				const infosDiv = pokemonShow.getElementsByClassName('infos')[0];
				infosDiv.classList.remove('no-display');
				const statsDiv = pokemonShow.getElementsByClassName(
					'statsDiv'
				)[0];
				statsDiv.classList.add('no-display');
			}
		}
	});

	document.addEventListener('keypress', (event) => {
		// ? control + enter ferme l'édition
		if (event.ctrlKey && event.key === 'Enter') {
			// On masque l'édition
			for (
				let i = 0;
				i < document.getElementsByClassName('pokemon-edit').length;
				i++
			) {
				const div = document.getElementsByClassName('pokemon-edit')[i];
				div.style.display = 'none';
			}

			// On affiche tous les pokemon show
			for (const pokemon of document.getElementsByClassName(
				'pokemon-show'
			)) {
				pokemon.style.display = 'flex';
			}

			// On actualise l'affichage

			// Pour chaque équipe
			for (let teamIndex = 0; teamIndex < 2; teamIndex++) {
				const team = _teams[teamIndex];

				// Pour chaque pokémon
				team.forEach((pokemonName, pokemonIndex) => {
					updatePokemonDiv(teamIndex + 1, pokemonIndex, teams);
				});
			}
		}
	});

	// ! - Fonctions ==============================================================================================

	/**
	 * @param {Pokemon} pokemon
	 * @param {Number} teamNumber - Numéro de l'équipe : 1 ou 2
	 * @param {Number} pokemonIndex - Index du pokemon
	 * @returns {HTMLElement} - La div contenant le pokemon
	 */
	function displayPokemon(pokemon, teamNumber, pokemonIndex) {
		/** @type {number}- Index de l'équipe: 0 ou 1 */
		const teamIndex = teamNumber - 1;
		/** @type {number}- Index de l'équipe opposée: 0 ou 1 */
		const opponentTeamIndex = (teamIndex + 1) % 2;

		/** @type {String} - Le climat */
		const climate = document.getElementById('climate').value;

		/** @type {HTMLElement} - Bloc pricipal */
		const pokemonDiv = document.createElement('div');
		pokemonDiv.classList.add('pokemon', 'pokemon-show');

		// On met de la couleur aux vitesses adverses en hover
		pokemonDiv.addEventListener('mouseover', () => {
			// On affiche la meilleure attaque en mouseenter
			previewDiv.addEventListener('mouseenter', () => {
				// On reset les damages
				resetDamages();

				// On affiche les dégats
				const damageDiv1 = document
					.getElementById('team1')
					.getElementsByClassName('damage');
				const damageDiv2 = document
					.getElementById('team2')
					.getElementsByClassName('damage');

				/** - Liste des deux divs .damage */
				const damagesDiv = [damageDiv1, damageDiv2];

				let index = 0;
				// Pour chaque pokemonDiv adverse, on ajoute les dégats maiximaux qu'il subit à damage
				for (const damageDiv of damagesDiv[opponentTeamIndex]) {
					damageDiv.style.display = 'flex';
					damageDiv.innerHTML = '';
					const pokemonOffensive = pokemon;
					const pokemonDefensive = teams[opponentTeamIndex][index];
					const defensiveDamageContent = getDamagesDiv(
						pokemonOffensive,
						pokemonDefensive,
						getBestMove(pokemonOffensive, pokemonDefensive)
					);
					damageDiv.appendChild(defensiveDamageContent);
					defensiveDamageContent.classList.add('damage-defensive');

					const offensiveDamageContent = getDamagesDiv(
						pokemonDefensive,
						pokemonOffensive,
						getBestMove(pokemonDefensive, pokemonOffensive)
					);
					damageDiv.appendChild(offensiveDamageContent);
					offensiveDamageContent.classList.add('damage-offensive');
					index++;
				}
			});
			// Pour chaque pokemon adverse
			teams[opponentTeamIndex].forEach((oppPokemon, index) => {
				const oppPokemonDiv = document.getElementById(
					`team${opponentTeamIndex + 1}-pokemon${index}`
				);
				const oppSpeedDiv = oppPokemonDiv.getElementsByClassName(
					'speed'
				)[0];
				// Si ma vitesse est supérieur à la sienne alors sa div de vitesse prend la classe low-speed
				if (
					pokemon.getStat(
						'spe',
						document.getElementById('climate').value
					) >
					oppPokemon.getStat(
						'spe',
						document.getElementById('climate').value
					)
				) {
					oppSpeedDiv.classList.add('low-speed');
					oppPokemonDiv.classList.add('border-low-speed');
				}
				// Si ma vitesse est inférieur à la sienne alors sa div de vitesse prend la classe high-speed
				if (
					pokemon.getStat(
						'spe',
						document.getElementById('climate').value
					) <
					oppPokemon.getStat(
						'spe',
						document.getElementById('climate').value
					)
				) {
					oppSpeedDiv.classList.add('high-speed');
					oppPokemonDiv.classList.add('border-high-speed');
				}
				// Si ma vitesse est égale à la sienne alors sa div de vitesse prend la classe equal-speed
				if (
					pokemon.getStat(
						'spe',
						document.getElementById('climate').value
					) ===
					oppPokemon.getStat(
						'spe',
						document.getElementById('climate').value
					)
				) {
					oppSpeedDiv.classList.add('equal-speed');
					oppPokemonDiv.classList.add('border-equal-speed');
				}
			});
		});

		// En mouseleave on efface les classes low et high speed et on efface le dégats
		pokemonDiv.addEventListener('mouseleave', () => {
			resetSpeedColor();
			resetDamages();
		});

		// ? Bloc 0 - Boutons edit et reset ==========================================================================================

		const buttonDiv = document.createElement('div');
		pokemonDiv.appendChild(buttonDiv);
		buttonDiv.classList.add('buttons');

		const editButton = document.createElement('span');
		buttonDiv.appendChild(editButton);
		editButton.classList.add('material-icons', 'edit', 'button');
		editButton.innerText = 'edit';
		if (!internet) {
			editButton.classList.remove('material-icons');
			editButton.innerText = 'E';
		}

		const resetButton = document.createElement('span');
		buttonDiv.appendChild(resetButton);
		resetButton.classList.add('material-icons', 'reset', 'button');
		resetButton.innerText = 'restart_alt';
		if (!internet) {
			resetButton.classList.remove('material-icons');
			resetButton.innerText = 'R';
		}

		// Bouton reset
		resetButton.addEventListener('click', () => {
			// On modifie le pokemon à son état initial
			pokemon = resetPokemon(pokemon, teamIndex, pokemonIndex);
			teams[teamNumber - 1][pokemonIndex] = pokemon;

			// On actualise l'affichage principal
			updatePokemonDiv(teamNumber, pokemonIndex, teams, pokemon);

			// On actualise l'affichage d'édition
			updatePokemonDivEdit(teamNumber, pokemonIndex, teams);

			console.log('reset', pokemon);
		});

		// ? Bloc 1 - Preview ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant les informations */
		const previewDiv = document.createElement('div');
		previewDiv.classList.add('preview');

		// On ouvre la fenêtre smogon du pokemon
		previewDiv.addEventListener('click', () => {
			window.open(
				`https://www.smogon.com/dex/ss/pokemon/${pokemon.name}`,
				'_blank'
			);
		});

		// 1.0 Le status
		previewDiv.appendChild(getStatusDiv(pokemon));

		// 1.1 - l'image
		const sprite = document.createElement('div');
		previewDiv.appendChild(sprite);
		sprite.classList.add('sprite');
		const abbr = document.createElement('abbr');
		sprite.appendChild(abbr);
		abbr.title = pokemon.name;
		const img = document.createElement('img');
		abbr.appendChild(img);
		if (internet) {
			img.src = getPokemonSprite(pokemon.name);
		} else {
			const pName = document.createElement('p');
			sprite.appendChild(pName);
			pName.innerText = pokemon.name;
		}

		// 1.2 - Le nom
		const name = document.createElement('h3');
		// previewDiv.appendChild(name);
		name.classList.add('name');
		name.innerText = pokemon.name;

		// 1.3 - Les types
		const types = document.createElement('div');
		previewDiv.appendChild(types);
		types.classList.add('types');
		const type1 = document.createElement('p');
		type1.innerText = pokemon.types[0];
		type1.classList.add('type');
		type1.classList.add('first-type');
		type1.classList.add(pokemon.types[0].toLowerCase());
		types.appendChild(type1);

		if (pokemon.types.length === 2) {
			const type2 = document.createElement('p');
			types.appendChild(type2);
			type2.classList.add('second-type');
			type2.classList.add('last-type');
			type2.innerText = pokemon.types[1];
			type2.classList.add('type');
			type2.classList.add(pokemon.types[1].toLowerCase());
		} else {
			type1.classList.add('last-type');
		}

		// ? Bloc 2 - Infos ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant le set (Vitesse, attaque, boosts, objet et capacité) */
		const infosDiv = document.createElement('div');
		infosDiv.classList.add('infos');

		// On affiche la meilleure attaque en mouseenter
		infosDiv.addEventListener('mouseenter', () => {
			resetDamages();

			// On affiche les dégats
			const damageDiv1 = document
				.getElementById('team1')
				.getElementsByClassName('damage');
			const damageDiv2 = document
				.getElementById('team2')
				.getElementsByClassName('damage');

			/** - Liste des deux divs .damage */
			const damagesDiv = [damageDiv1, damageDiv2];

			// On reset les damages
			resetDamages();

			let index = 0;
			// Pour chaque pokemonDiv adverse, on affiche le max degats dans damage
			for (const damageDiv of damagesDiv[opponentTeamIndex]) {
				damageDiv.style.display = 'flex';
				damageDiv.innerHTML = '';
				const pokemonOffensive = pokemon;
				const pokemonDefensive = teams[opponentTeamIndex][index];
				const defensiveDamageContent = getDamagesDiv(
					pokemonOffensive,
					pokemonDefensive,
					getBestMove(pokemonOffensive, pokemonDefensive)
				);
				damageDiv.appendChild(defensiveDamageContent);
				defensiveDamageContent.classList.add('damage-defensive');

				const offensiveDamageContent = getDamagesDiv(
					pokemonDefensive,
					pokemonOffensive,
					getBestMove(pokemonDefensive, pokemonOffensive)
				);
				damageDiv.appendChild(offensiveDamageContent);
				offensiveDamageContent.classList.add('damage-offensive');
				index++;
			}
		});

		// 2.1 - La vitesse
		const speedDiv = document.createElement('p');
		infosDiv.appendChild(speedDiv);
		speedDiv.classList.add('speed');
		speedDiv.innerText = `Speed : ${pokemon.getStat('spe', climate)}`;

		// 2.2 - Les boosts
		const boostDiv = getBoostDiv(pokemon);
		infosDiv.appendChild(boostDiv);

		// 2.3 - L'objet
		const itemDiv = document.createElement('p');
		infosDiv.appendChild(itemDiv);
		const item = getItem(pokemon.getItem());
		itemDiv.classList.add('item');

		if (internet && item.name.toLowerCase() !== 'no item') {
			const imgSrc = getItemUrl(item.name);
			itemDiv.innerHTML = `<abbr title="${item.name}: ${formatDesc(
				item.desc
			).replaceAll(
				'<br/>',
				' '
			)}"><img class="img" src="${imgSrc}"></img></abbr>`;
		} else {
			itemDiv.innerHTML = `<abbr title="${formatDesc(
				item.desc
			).replaceAll('<br/>', ' ')}">${item.name}</abbr>`;
		}

		if (
			itemDiv &&
			itemDiv.getElementsByClassName('img') &&
			itemDiv.getElementsByClassName('img')[0]
		) {
			const imageItem = itemDiv.getElementsByClassName('img')[0];
			window.addEventListener('load', () => {
				if (imageItem.naturalHeight === 0) {
					itemDiv.innerHTML = `<abbr title="${formatDesc(
						item.desc
					).replaceAll('<br/>', ' ')}">${item.name}</abbr>`;
				}
			});
		}

		// ? - Quand on clique sur un objet
		itemDiv.addEventListener('click', () => {
			// On modifie les boosts s'il en modifie
			const boost = getItemBoost(pokemon.getItem());

			// Pour chaque stat on ajoute les boosts
			statsList.forEach((stat) => {
				if (boost[stat]) {
					const value = boost[stat];
					pokemon.addBoost(stat, value);
				}
			});

			// Cas spécial : White Herb
			if (item.name === 'White Herb') {
				// On remet à 0 chaque stat baissée
				statsList.forEach((stat) => {
					if (pokemon.getBoost(stat) < 0) {
						pokemon.setBoost(stat, 0);
					}
				});
			}

			// On modifie le status
			pokemon.setStatus(getItemStatus(item));

			// On enlève l'objet
			pokemon.looseItem();

			// On met à jour l'affichage
			updatePokemonDiv(teamNumber, pokemonIndex, teams);
			updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
		});

		// 2.4 - La capacité
		const abilityDiv = document.createElement('p');
		infosDiv.appendChild(abilityDiv);
		const ability = getAbility(
			pokemon.ability.toLowerCase().replaceAll(' ', '')
		);
		abilityDiv.classList.add('ability', 'popup');

		switch (pokemon.ability) {
			case 'Electric Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Electric Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Grassy Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Grassy Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Misty Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Misty Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Psychic Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Psychic Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			default:
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					ability.shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;
		}

		// ? - Quand on clique sur une capacité
		// On modifie les boosts si elle en modifie, ainsi que le terrain ou le climat si c'est lié
		abilityDiv.addEventListener('click', () => {
			const boost = getAbilityBoost(pokemon);

			// Pour chaque stat on ajoute les boosts
			statsList.forEach((stat) => {
				if (boost[stat]) {
					const value = boost[stat];
					pokemon.addBoost(stat, value);
				}
			});

			// Si on a cliqué sur natural cure, alors on soigne le pokemon
			switch (pokemon.ability.toLowerCase()) {
				case 'air lock':
					climateSelect.value = '';
					updateAllPokemons();
					break;

				case 'drizzle':
					climateSelect.value = 'rain';
					updateAllPokemons();
					break;

				case 'misty surge':
					terrainSelect.value = 'Misty';
					// updateAllPokemons();
					break;

				case 'electric surge':
					terrainSelect.value = 'Electric';
					// updateAllPokemons();
					break;

				case 'psychic surge':
					terrainSelect.value = 'Psychic';
					// updateAllPokemons();
					break;

				case 'grassy surge':
					terrainSelect.value = 'Grassy';
					// updateAllPokemons();
					break;

				case 'drought':
					climateSelect.value = 'sun';
					updateAllPokemons();
					break;

				case 'natural cure':
					pokemon.heal();
					break;

				case 'sand stream':
					climateSelect.value = 'sand';
					updateAllPokemons();
					break;

				case 'snow warning':
					climateSelect.value = 'hail';
					updateAllPokemons();
					break;

				default:
					break;
			}

			// On met à jour l'affichage
			updatePokemonDiv(teamNumber, pokemonIndex, teams);
			updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
		});

		// ? Bloc 2-bis - Statistiques ==========================================================================================

		const statsDiv = document.createElement('div');
		statsDiv.classList.add('statsDiv', 'no-display');

		const statSpe = document.createElement('div');
		statsDiv.appendChild(statSpe);
		statSpe.classList.add('stat-line', 'spe');
		const speName = document.createElement('p');
		statSpe.appendChild(speName);
		speName.innerText = 'Spe';
		const speValue = document.createElement('p');
		statSpe.appendChild(speValue);
		speValue.innerText = `:   ${pokemon.getStat('spe', climate)}`;
		speValue.classList.add('stat', 'spe');

		const statAtk = document.createElement('div');
		statsDiv.appendChild(statAtk);
		statAtk.classList.add('stat-line', 'atk');
		const atkName = document.createElement('p');
		statAtk.appendChild(atkName);
		atkName.innerText = 'Atk';
		const atkValue = document.createElement('p');
		statAtk.appendChild(atkValue);
		atkValue.innerText = `:   ${pokemon.getStat('atk', climate)}`;
		atkValue.classList.add('stat', 'atk');

		const statSpA = document.createElement('div');
		statsDiv.appendChild(statSpA);
		statSpA.classList.add('stat-line', 'spa');
		const spaName = document.createElement('p');
		statSpA.appendChild(spaName);
		spaName.innerText = 'SpA';
		const spaValue = document.createElement('p');
		statSpA.appendChild(spaValue);
		spaValue.innerText = `:   ${pokemon.getStat('spa', climate)}`;
		spaValue.classList.add('stat', 'spa');

		// On met en valeur la meilleure attaque
		if (
			pokemon.getStat('atk', climate) >= pokemon.getStat('spa', climate)
		) {
			statSpA.classList.add('low-luminosity');
		} else {
			statAtk.classList.add('low-luminosity');
		}

		const statDef = document.createElement('div');
		statsDiv.appendChild(statDef);
		statDef.classList.add('stat-line', 'def');
		const defName = document.createElement('p');
		statDef.appendChild(defName);
		defName.innerText = 'Def';
		const defValue = document.createElement('p');
		statDef.appendChild(defValue);
		defValue.innerText = `:   ${pokemon.getStat('def', climate)}`;
		defValue.classList.add('stat', 'def');

		const statSpD = document.createElement('div');
		statsDiv.appendChild(statSpD);
		statSpD.classList.add('stat-line', 'spd');
		const spdName = document.createElement('p');
		statSpD.appendChild(spdName);
		spdName.innerText = 'SpD';
		const spdValue = document.createElement('p');
		statSpD.appendChild(spdValue);
		spdValue.innerText = `:   ${pokemon.getStat('spd', climate)}`;
		spdValue.classList.add('stat', 'spd');

		// On met en valeur la meilleure défense
		if (pokemon.getStat('def', climate) < pokemon.getStat('spd', climate)) {
			statDef.classList.add('low-luminosity');
		} else {
			statSpD.classList.add('low-luminosity');
		}

		const statHp = document.createElement('div');
		statsDiv.appendChild(statHp);
		statHp.classList.add('stat-line', 'hp');
		const hpName = document.createElement('p');
		statHp.appendChild(hpName);
		hpName.innerText = 'Hp';
		const hpValue = document.createElement('p');
		statHp.appendChild(hpValue);
		hpValue.innerText = `:   ${pokemon.getStat('hp', climate)}`;
		hpValue.classList.add('stat', 'hp');

		// ? Bloc 3 - Moves ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant les 4 moves */
		const movesDiv = document.createElement('div');
		movesDiv.classList.add('moves');

		pokemon.moves.forEach((moveName) => {
			let move = getMove(moveName);

			/** @type{HTMLElement} - La div contenant le move .move */
			const moveDiv = document.createElement('div');
			movesDiv.appendChild(moveDiv);
			moveDiv.classList.add('move', move.type.toLowerCase());
			moveDiv.setAttribute('name', moveName);

			const moveNameDiv = document.createElement('abbr');
			moveDiv.appendChild(moveNameDiv);
			moveNameDiv.classList.add('move-name');
			moveNameDiv.innerText = move.name;
			moveNameDiv.title = move.shortDesc;

			const moveEnd = document.createElement('div');
			moveDiv.appendChild(moveEnd);
			moveEnd.classList.add('move-end');

			const movePowerDiv = document.createElement('p');
			moveEnd.appendChild(movePowerDiv);
			movePowerDiv.classList.add('move-power');
			if (move.category !== 'Status') {
				movePowerDiv.innerText = move.basePower;
			}

			const moveIconDiv = document.createElement('img');
			moveEnd.appendChild(moveIconDiv);
			moveIconDiv.classList.add('move-icon');
			moveIconDiv.src = getMoveCategoryIcon(move.category);

			// ? - Quand on est sur un move
			// On met le move en avant et affiche les dégats en mouse-enter
			moveDiv.addEventListener('mouseenter', () => {
				resetDamages();

				// On redéfinit le move au cas où il aurait été changé
				move = getMove(moveDiv.attributes['name'].value);

				// On affiche les dégats
				const damageDiv1 = document
					.getElementById('team1')
					.getElementsByClassName('damage');
				const damageDiv2 = document
					.getElementById('team2')
					.getElementsByClassName('damage');
				if (teamNumber === 1) {
					for (const damageDiv of damageDiv1) {
						damageDiv.innerHTML = '';
					}
					let index = 0;
					for (const div of damageDiv2) {
						div.innerHTML = '';
						const pokemonOffensive = pokemon;
						const pokemonDefensive = teams[1][index];
						div.style.display = 'flex';
						div.innerHTML = '';
						const defensiveDamageContent = getDamagesDiv(
							pokemonOffensive,
							pokemonDefensive,
							move
						);
						div.appendChild(defensiveDamageContent);
						defensiveDamageContent.classList.add(
							'damage-defensive'
						);

						const offensiveDamageContent = getDamagesDiv(
							pokemonDefensive,
							pokemonOffensive,
							getBestMove(pokemonDefensive, pokemonOffensive)
						);
						div.appendChild(offensiveDamageContent);
						offensiveDamageContent.classList.add(
							'damage-offensive'
						);
						index++;
					}
				} else {
					for (const damageDiv of damageDiv2) {
						damageDiv.innerHTML = '';
					}
					let index = 0;
					for (const div of damageDiv1) {
						const pokemonOffensive = pokemon;
						const pokemonDefensive = teams[0][index];
						div.style.display = 'flex';
						div.innerHTML = '';
						const defensiveDamageContent = getDamagesDiv(
							pokemonOffensive,
							pokemonDefensive,
							move
						);
						div.appendChild(defensiveDamageContent);
						defensiveDamageContent.classList.add(
							'damage-defensive'
						);

						const offensiveDamageContent = getDamagesDiv(
							pokemonDefensive,
							pokemonOffensive,
							getBestMove(pokemonDefensive, pokemonOffensive)
						);
						div.appendChild(offensiveDamageContent);
						offensiveDamageContent.classList.add(
							'damage-offensive'
						);
						index++;
					}
				}
			});

			// ? - Quand on clique sur un move
			// On modifie les boosts si move en modifie
			moveDiv.addEventListener('click', () => {
				/** @type{Object} - objet boost avec name et boosts */
				const boosts = getMoveBoosts(move.name);

				// S'il y a des boosts
				if (boosts) {
					statsList.forEach((stat) => {
						if (boosts[stat]) {
							const value = boosts[stat];
							pokemon.addBoost(stat, value);
						}
					});

					// On met à jour l'affichage
					updatePokemonDiv(teamNumber, pokemonIndex, teams);
					updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
				}

				// Cas spécial: Belly drum
				if (move.name === 'Belly Drum') {
					pokemon.setBoost('atk', 6);

					// On met à jour l'affichage
					updatePokemonDiv(teamNumber, pokemonIndex, teams);
					updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
				}
				if (move.name === 'Roost') {
					if (!pokemon.roosted) {
						if (pokemon.isType('Flying')) {
							if (pokemon.types.length === 1) {
								pokemon.types = ['Normal'];
							} else {
								pokemon.types.forEach((type) => {
									if (type !== 'Flying') {
										pokemon.types = [type];
									}
								});
							}
						}
					} else {
						const truePokemon = getPokemon(pokemon.name);
						pokemon.types = truePokemon.types;
					}

					// On modifie la valeur de roosted
					pokemon.roosted = !pokemon.roosted;

					// On met à jour l'affichage
					updatePokemonDiv(teamNumber, pokemonIndex, teams);
					updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
					// if (getPokemon(pokemon.name)) {

					// }
				}
			});
		});

		// ? Bloc 4 - Dégats ==========================================================================================

		const damagesDiv = document.createElement('div');
		damagesDiv.classList.add('damage');

		// On affiche la meilleure attaque en mouseenter
		damagesDiv.addEventListener('mouseenter', () => {
			// On affiche les dégats
			const damageDiv1 = document
				.getElementById('team1')
				.getElementsByClassName('damage');
			const damageDiv2 = document
				.getElementById('team2')
				.getElementsByClassName('damage');

			/** - Liste des deux divs .damage */
			const damagesDiv = [damageDiv1, damageDiv2];

			// On reset les damages
			resetDamages();

			let index = 0;
			// Pour chaque pokemonDiv adverse, on affiche le max degats dans damage
			for (const damageDiv of damagesDiv[opponentTeamIndex]) {
				damageDiv.style.display = 'flex';
				damageDiv.innerHTML = '';
				const pokemonOffensive = pokemon;
				const pokemonDefensive = teams[opponentTeamIndex][index];
				const defensiveDamageContent = getDamagesDiv(
					pokemonOffensive,
					pokemonDefensive,
					getBestMove(pokemonOffensive, pokemonDefensive)
				);
				damageDiv.appendChild(defensiveDamageContent);
				defensiveDamageContent.classList.add('damage-defensive');

				const offensiveDamageContent = getDamagesDiv(
					pokemonDefensive,
					pokemonOffensive,
					getBestMove(pokemonDefensive, pokemonOffensive)
				);
				damageDiv.appendChild(offensiveDamageContent);
				offensiveDamageContent.classList.add('damage-offensive');
				index++;
			}
		});

		// On efface tout en quittant le mouseover
		damagesDiv.addEventListener('mouseleave', () => {
			resetDamages();
		});

		// ? Finalisation ==========================================================================================

		if (teamNumber === 1) {
			pokemonDiv.appendChild(previewDiv);
			pokemonDiv.appendChild(infosDiv);
			pokemonDiv.appendChild(statsDiv);
			pokemonDiv.appendChild(movesDiv);
			pokemonDiv.appendChild(damagesDiv);
		} else {
			pokemonDiv.appendChild(damagesDiv);
			pokemonDiv.appendChild(movesDiv);
			pokemonDiv.appendChild(statsDiv);
			pokemonDiv.appendChild(infosDiv);
			pokemonDiv.appendChild(previewDiv);
		}

		return pokemonDiv;
	}

	/**
	 * @param {Pokemon} pokemon
	 * @returns {HTMLElement} - La div contenant le pokemon
	 */
	function displayEditPokemon(pokemon) {
		/** @type {HTMLElement} - Bloc pricipal */
		const pokemonDiv = document.createElement('div');
		pokemonDiv.classList.add('pokemon', 'pokemon-edit');

		// ? Bloc 0 - Le bouton pour valider les changements ==========================================================================================
		const saveButton = document.createElement('span');
		pokemonDiv.appendChild(saveButton);
		saveButton.classList.add('material-icons', 'save', 'button');
		saveButton.innerText = 'save';
		if (!internet) {
			saveButton.classList.remove('material-icons');
			saveButton.innerText = 'save';
		}

		// ? Bloc 1 - Preview ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant les informations */
		const previewDiv = document.createElement('div');
		pokemonDiv.appendChild(previewDiv);
		previewDiv.classList.add('preview');

		// 1.1 - l'image
		const spriteDiv = document.createElement('div');
		previewDiv.appendChild(spriteDiv);
		spriteDiv.classList.add('sprite-container');
		// 1.1.1 le sprite
		const abbr = document.createElement('abbr');
		spriteDiv.appendChild(abbr);
		abbr.title = pokemon.name;
		const sprite = document.createElement('img');
		abbr.appendChild(sprite);
		sprite.classList.add('sprite');
		if (internet) {
			sprite.src = getPokemonSprite(pokemon.name);
		} else {
			const pName = document.createElement('p');
			sprite.appendChild(pName);
			pName.innerText = pokemon.name;
		}

		// On ouvre la fenêtre smogon du pokemon quand on clique sur son sprite
		sprite.addEventListener('click', () => {
			window.open(
				`https://www.smogon.com/dex/ss/pokemon/${pokemon.name}`,
				'_blank'
			);
		});

		// 1.1.2 Le status
		spriteDiv.appendChild(getStatusDiv(pokemon));

		// 1.2 - Le nom
		const name = document.createElement('h3');
		// previewDiv.appendChild(name);
		name.classList.add('name');
		name.innerText = pokemon.name;

		// 1.3 - Les types
		const types = document.createElement('div');
		previewDiv.appendChild(types);
		types.classList.add('types');
		const type1 = document.createElement('p');
		type1.innerText = pokemon.types[0];
		type1.classList.add('type');
		type1.classList.add('first-type');
		type1.classList.add(pokemon.types[0].toLowerCase());
		types.appendChild(type1);

		if (pokemon.types.length === 2) {
			const type2 = document.createElement('p');
			types.appendChild(type2);
			type2.classList.add('second-type');
			type2.classList.add('last-type');
			type2.innerText = pokemon.types[1];
			type2.classList.add('type');
			type2.classList.add(pokemon.types[1].toLowerCase());
		} else {
			type1.classList.add('last-type');
		}

		// 1.4 - L'objet
		const itemDiv = document.createElement('div');
		previewDiv.appendChild(itemDiv);
		itemDiv.classList.add('item');
		const itemName = document.createElement('p');
		itemDiv.appendChild(itemName);
		itemName.innerText = 'Item : ';
		const itemSelect = document.createElement('select');
		itemDiv.appendChild(itemSelect);
		itemSelect.classList.add('item-select');
		itemSelect.setAttribute('name', 'item-select');
		itemSelect.id = 'item-select';
		for (const itemName in itemsList) {
			const item = getItem(itemName);
			const option = document.createElement('option');
			itemSelect.appendChild(option);
			option.value = item.name;
			option.innerText = item.name;
			option.classList.add('popup');

			// Si c'est sa capacité alors on lui ajoute selected
			if (item.name === pokemon.getItem()) {
				option.selected = 'selected';
			}
		}

		// Affichage du popup de l'objet
		itemSelect.addEventListener('mouseenter', () => {
			const item = getItem(
				pokemon.getItem().toLowerCase().replaceAll(' ', '')
			);
			// Le popup avec la description
			const descriptionPopup = document.createElement('span');
			previewDiv.appendChild(descriptionPopup);
			descriptionPopup.classList.add('description', 'popup-text', 'show');
			descriptionPopup.id = 'item-popup';
			descriptionPopup.innerText = item.desc;
			descriptionPopup.style.maxWidth = '20rem';

			// Positionnement
			const divTop = itemSelect.offsetTop;
			const divHeight = itemSelect.offsetHeight;
			const divLeft = itemSelect.offsetLeft;
			const divWidth = itemSelect.offsetWidth;
			const popupTop = descriptionPopup.offsetTop;
			const popupHeight = descriptionPopup.offsetHeight;
			const popupLeft = descriptionPopup.offsetLeft;
			const popupWidth = descriptionPopup.offsetWidth;

			descriptionPopup.style.top = `${
				divTop + (divHeight - popupHeight) / 2
			}px`;
			descriptionPopup.style.left = `${
				itemSelect.offsetLeft + itemSelect.offsetWidth
			}px`;
		});

		itemSelect.addEventListener('mouseleave', () => {
			if (document.getElementById('item-popup')) {
				previewDiv.removeChild(document.getElementById('item-popup'));
			}
		});

		itemSelect.addEventListener('change', () => {
			pokemon.setItem(itemSelect.value);
			pokemonDiv.removeChild(statsDiv);
			statsDiv = getStatsDiv(pokemon);
			pokemonDiv.appendChild(statsDiv);
		});

		// 1.5 - La capacité
		const abilityDiv = document.createElement('div');
		previewDiv.appendChild(abilityDiv);
		abilityDiv.classList.add('ability');
		const abilityName = document.createElement('p');
		abilityDiv.appendChild(abilityName);
		abilityName.innerText = 'Ability : ';
		const abilitySelect = document.createElement('select');
		abilityDiv.appendChild(abilitySelect);
		abilitySelect.classList.add('ability-select', 'popup');
		abilitySelect.setAttribute('name', 'ability-select');
		abilitySelect.id = 'ability-select';
		pokemon.abilitiesList.forEach((abilityName) => {
			const ability = getAbility(
				abilityName.toLowerCase().replaceAll(' ', '')
			);
			const option = document.createElement('option');
			abilitySelect.appendChild(option);
			option.value = ability.name;
			option.innerText = ability.name;

			// Si c'est sa capacité alors on lui ajoute selected
			if (abilityName === pokemon.ability) {
				option.selected = 'selected';
			}
		});

		abilitySelect.addEventListener('mouseenter', () => {
			const ability = getAbility(
				pokemon.ability.toLowerCase().replaceAll(' ', '')
			);
			// Le popup avec la description
			const descriptionPopup = document.createElement('span');
			previewDiv.appendChild(descriptionPopup);
			descriptionPopup.classList.add('description', 'popup-text', 'show');
			descriptionPopup.id = 'ability-popup';
			descriptionPopup.innerHTML = formatDesc(ability.shortDesc);

			switch (ability.name) {
				case 'Electric Surge':
					descriptionPopup.innerHTML = getMove(
						'Electric Terrain'
					).shortDesc;
					break;

				case 'Grassy Surge':
					descriptionPopup.innerHTML = getMove(
						'Grassy Terrain'
					).shortDesc;
					break;

				case 'Misty Surge':
					descriptionPopup.innerHTML = getMove(
						'Misty Terrain'
					).shortDesc;
					break;

				case 'Psychic Surge':
					descriptionPopup.innerHTML = getMove(
						'Psychic Terrain'
					).shortDesc;
					break;

				default:
					descriptionPopup.innerHTML = ability.shortDesc;
					break;
			}
			descriptionPopup.style.maxWidth = '20rem';
			descriptionPopup.style.top = `${
				abilitySelect.offsetTop +
				(abilitySelect.offsetHeight - descriptionPopup.offsetHeight) / 2
			}px`;
			descriptionPopup.style.left = `${
				abilitySelect.offsetLeft + abilitySelect.offsetWidth
			}px`;
		});

		abilitySelect.addEventListener('mouseleave', () => {
			if (document.getElementById('ability-popup')) {
				previewDiv.removeChild(
					document.getElementById('ability-popup')
				);
			}
		});

		abilitySelect.addEventListener('change', () => {
			pokemon.ability = abilitySelect.value;
			pokemonDiv.removeChild(statsDiv);
			statsDiv = getStatsDiv(pokemon);
			pokemonDiv.appendChild(statsDiv);
		});

		// 1.6 - La nature
		const natureDiv = document.createElement('div');
		previewDiv.appendChild(natureDiv);
		natureDiv.classList.add('nature');
		const natureName = document.createElement('p');
		natureDiv.appendChild(natureName);
		natureName.innerText = 'Nature : ';
		const natureSelect = document.createElement('select');
		natureDiv.appendChild(natureSelect);
		natureSelect.classList.add('nature-select');
		natureSelect.setAttribute('name', 'nature-select');
		natureSelect.id = 'nature-select';

		const atkGroup = document.createElement('optgroup');
		natureSelect.appendChild(atkGroup);
		atkGroup.label = 'Attack';
		const defGroup = document.createElement('optgroup');
		natureSelect.appendChild(defGroup);
		defGroup.label = 'Defense';
		const spaGroup = document.createElement('optgroup');
		natureSelect.appendChild(spaGroup);
		spaGroup.label = 'Special Attack';
		const spdGroup = document.createElement('optgroup');
		natureSelect.appendChild(spdGroup);
		spdGroup.label = 'Special Defense';
		const speGroup = document.createElement('optgroup');
		natureSelect.appendChild(speGroup);
		speGroup.label = 'Speed';
		const neutralGroup = document.createElement('optgroup');
		natureSelect.appendChild(neutralGroup);
		neutralGroup.label = 'Neutral';

		for (const natureName in natureList) {
			const option = document.createElement('option');
			option.value = natureName;
			let plus = '';
			let minus = '';
			for (const stat in natureList[natureName]) {
				if (+natureList[natureName][stat] === 0.9) {
					minus = stat;
				}
				if (+natureList[natureName][stat] === 1.1) {
					plus = stat;
				}
			}

			// On affiche le détail de la nature
			if (plus) {
				option.innerText = `${natureName} (+${statToUpperCase(
					plus
				)} / -${statToUpperCase(minus)})`;
			} else {
				option.innerText = `${natureName}`;
			}
			// Si c'est sa nature alors on lui ajoute selected
			if (natureName === pokemon.nature) {
				option.selected = 'selected';
			}

			// On ajoute l'option au optgroup correspondant
			switch (plus) {
				case 'atk':
					atkGroup.appendChild(option);
					break;
				case 'def':
					defGroup.appendChild(option);
					break;
				case 'spa':
					spaGroup.appendChild(option);
					break;
				case 'spd':
					spdGroup.appendChild(option);
					break;
				case 'spe':
					speGroup.appendChild(option);
					break;

				default:
					neutralGroup.appendChild(option);
					break;
			}
		}

		natureSelect.addEventListener('change', () => {
			pokemon.nature = natureSelect.value;
			pokemonDiv.removeChild(statsDiv);
			statsDiv = getStatsDiv(pokemon);
			pokemonDiv.appendChild(statsDiv);
		});

		// 1.7 Le status
		// On créé les éléments
		const statusChangeDiv = document.createElement('div');
		previewDiv.appendChild(statusChangeDiv);
		statusChangeDiv.classList.add('status-change');
		const statusName = document.createElement('p');
		statusChangeDiv.appendChild(statusName);
		statusName.innerText = 'Status : ';
		const statusSelect = document.createElement('select');
		statusChangeDiv.appendChild(statusSelect);
		statusSelect.classList.add('status-select', 'popup');
		statusSelect.setAttribute('name', 'status-select');
		statusSelect.id = 'status-select';

		// On créé les options du select
		statusList.forEach((status) => {
			const statusName = status.name;
			const option = document.createElement('option');
			statusSelect.appendChild(option);
			option.value = statusName;
			option.innerText =
				statusName !== 'healthy' ? capitalize(statusName) : '';

			// Si c'est sa capacité alors on lui ajoute selected
			if (statusName === pokemon.status) {
				option.selected = 'selected';
			}
		});

		// On créé le popup de status
		statusSelect.addEventListener('mouseenter', () => {
			const status = pokemon.getStatus();
			// Le popup avec la description
			const descriptionPopup = document.createElement('span');
			previewDiv.appendChild(descriptionPopup);
			descriptionPopup.classList.add('description', 'popup-text', 'show');
			descriptionPopup.id = 'status-popup';
			descriptionPopup.innerHTML = formatDesc(status.desc);
			descriptionPopup.style.maxWidth = '20rem';
			descriptionPopup.style.top = `${
				statusSelect.offsetTop +
				(statusSelect.offsetHeight - descriptionPopup.offsetHeight) / 2
			}px`;
			descriptionPopup.style.left = `${
				statusSelect.offsetLeft + statusSelect.offsetWidth
			}px`;
		});

		// On supprime le popup de status
		statusSelect.addEventListener('mouseleave', () => {
			if (document.getElementById('status-popup')) {
				previewDiv.removeChild(document.getElementById('status-popup'));
			}
		});

		statusSelect.addEventListener('change', () => {
			// On redéfinit le status
			pokemon.setStatus(statusSelect.value);
			spriteDiv.replaceChild(
				getStatusDiv(pokemon),
				spriteDiv.getElementsByClassName('status-container')[0]
			);
			pokemonDiv.removeChild(statsDiv);
			statsDiv = getStatsDiv(pokemon);
			pokemonDiv.appendChild(statsDiv);

			// On modifie pokemon-show
		});

		// ? Bloc 2 - Moves ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant le set */
		const movesDiv = document.createElement('div');
		pokemonDiv.appendChild(movesDiv);
		movesDiv.classList.add('moves');

		const movesTitle = document.createElement('h2');
		movesDiv.appendChild(movesTitle);
		movesTitle.classList.add('moves-title');
		movesTitle.innerText = 'Moves';

		/** @type {HTMLElement[]} - Liste des 4 selects */
		const moves = [];
		for (let i = 0; i < 4; i++) {
			const moveSelect = document.createElement('select');
			movesDiv.appendChild(moveSelect);
			moves.push(moveSelect);
			moveSelect.classList.add(`move${i}`);
			moveSelect.setAttribute(`name`, `move${i}-select`);
			moveSelect.id = `move${i}-select`;

			moveSelect.addEventListener('change', () => {
				pokemon.moves[i] = moveSelect.value;
			});

			// Popup du move
			moveSelect.addEventListener('mouseenter', () => {
				const move = getMove(moveSelect.value);

				// Le popup avec la description
				const descriptionPopup = document.createElement('span');
				movesDiv.appendChild(descriptionPopup);
				descriptionPopup.classList.add(
					'description',
					'popup-text',
					'show'
				);
				descriptionPopup.id = 'move-popup';
				appendMovePopup(move, descriptionPopup);
				descriptionPopup.style.top = `${
					moveSelect.offsetTop +
					(moveSelect.offsetHeight - descriptionPopup.offsetHeight) /
						2
				}px`;
				descriptionPopup.style.left = `${
					moveSelect.offsetLeft + moveSelect.offsetWidth
				}px`;
			});

			moveSelect.addEventListener('mouseleave', () => {
				if (document.getElementById('move-popup')) {
					movesDiv.removeChild(document.getElementById('move-popup'));
				}
			});
		}

		// On ajoute tous les moves que le pokemon peut apprendre en option des selects
		pokemon.learnMoves.forEach((move) => {
			if (pokemon.name.includes('Gastrodon')) {
			}
			for (let i = 0; i < 4; i++) {
				const option = document.createElement('option');
				moves[i].appendChild(option);
				option.value = move.name;
				option.classList.add(move.category);
				option.innerText = move.name;

				if (pokemon.moves[i] === move.name) {
					option.selected = 'selected';
					// ? Debug
				}
			}
		});

		// ? Bloc 3 - Statistiques ==========================================================================================

		let statsDiv = getStatsDiv(pokemon);
		pokemonDiv.appendChild(statsDiv);

		return pokemonDiv;
	}

	/**
	 * - Réinitialise le pokemon en gardant son objet, sa capacité, sa nature, ses attaques et ses ev/iv.
	 * @param {Pokemon} pokemon
	 * @param {number} teamIndex - 0 ou 1
	 * @param {number} pokemonIndex [[ 0 ; nbPokemons [[
	 * @returns {Pokemon} - Un nouveau pokemon réinitialisé
	 */
	function resetPokemon(pokemon, teamIndex, pokemonIndex) {
		const pkmnData = teams[teamIndex][pokemonIndex];
		let p = new Pokemon(pokemon.name, tier, {
			item: pkmnData.item,
			ability: pkmnData.ability,
			nature: pkmnData.nature,
			moves: pkmnData.moves,
			iv: pkmnData.iv,
			ev: pkmnData.ev,
		});
		console.log('reçu', pokemon);
		console.log('renvoyé', p);
	}

	/**
	 * - Met à jour la div correspondant au pokemon
	 * @param {Number} teamNumber - Numéro de l'équipe : 1 ou 2
	 * @param {Number} pokemonIndex - Index du pokemon dans la liste teams[teamNumber - 1]
	 * @param {Array} teams - Liste des deux listes de pkemons
	 */
	function updatePokemonDiv(
		teamNumber,
		pokemonIndex,
		teams,
		_pokemon = null
	) {
		const pokemon = _pokemon
			? _pokemon
			: teams[teamNumber - 1][pokemonIndex];
		console.log('updatePokemonDiv', pokemon);
		/** @type{HTMLElemnt} - La div du pokemon : .pokemon-show */
		const pokemonDiv = document.getElementById(
			`team${teamNumber}-pokemon${pokemonIndex}`
		);
		const climate = document.getElementById('climate').value;

		// ? 1 - Preview ==========================================================================================

		// Le status
		pokemonDiv
			.getElementsByClassName('preview')[0]
			.replaceChild(
				getStatusDiv(pokemon),
				pokemonDiv
					.getElementsByClassName('preview')[0]
					.getElementsByClassName('status-container')[0]
			);

		// 1.3 - Les types
		displayTypes(pokemon, pokemonDiv);

		// ? 2 - Les infos ==========================================================================================

		// La vitesse
		pokemonDiv
			.getElementsByClassName('infos')[0]
			.getElementsByClassName(
				'speed'
			)[0].innerText = `Speed : ${pokemon.getStat('spe', climate)}`;

		// Les boosts
		pokemonDiv
			.getElementsByClassName('infos')[0]
			.replaceChild(
				getBoostDiv(pokemon),
				pokemonDiv
					.getElementsByClassName('infos')[0]
					.getElementsByClassName('boost')[0]
			);

		// L'objet
		const item = getItem(pokemon.getItem());
		const itemDiv = pokemonDiv
			.getElementsByClassName('infos')[0]
			.getElementsByClassName('item')[0];
		const abbr = pokemonDiv
			.getElementsByClassName('infos')[0]
			.getElementsByClassName('item')[0]
			.getElementsByTagName('abbr')[0];

		if (internet && item.name.toLowerCase() !== 'no item') {
			const imgSrc = getItemUrl(item.name);
			console.log(imgSrc);
			abbr.innerHTML = `<abbr title="${item.name}: ${formatDesc(
				item.desc
			).replaceAll(
				'<br/>',
				' '
			)}"><img class="img" src="${imgSrc}"></img></abbr>`;
		} else {
			abbr.innerHTML = `<abbr title="${formatDesc(item.desc).replaceAll(
				'<br/>',
				' '
			)}">${item.name}</abbr>`;
		}

		const imageItem = itemDiv.getElementsByClassName('img')[0];
		window.addEventListener('load', () => {
			if (imageItem && imageItem.naturalHeight === 0) {
				itemDiv.innerHTML = `<abbr title="${formatDesc(
					item.desc
				).replaceAll('<br/>', ' ')}">${item.name}</abbr>`;
			}
		});
		if (imageItem && imageItem.naturalHeight === 0) {
			itemDiv.innerHTML = `<abbr title="${formatDesc(
				item.desc
			).replaceAll('<br/>', ' ')}">${item.name}</abbr>`;
		}

		// La capacité
		const abilityDiv = pokemonDiv.getElementsByClassName('ability')[0];
		const ability = getAbility(
			pokemon.ability.toLowerCase().replaceAll(' ', '')
		);

		switch (pokemon.ability) {
			case 'Electric Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Electric Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Grassy Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Grassy Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Misty Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Misty Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			case 'Psychic Surge':
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					getMove('Psychic Terrain').shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;

			default:
				abilityDiv.innerHTML = `<abbr title="${formatDesc(
					ability.shortDesc
				).replaceAll('<br/>', ' ')}">${formatDesc(
					pokemon.ability
				)}</abbr>`;
				break;
		}

		// ? 3 - Les statistiques ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant les statistiques */
		const statsDiv = pokemonDiv.getElementsByClassName('statsDiv')[0];
		for (const statDiv of statsDiv.getElementsByClassName('stat')) {
			statsList.forEach((statName) => {
				if (statDiv.classList.contains(statName)) {
					statDiv.innerText = `: ${pokemon.getStat(
						statName,
						climate
					)}`;
				}
			});
		}

		// ? 4 - Les moves ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant le set */
		const movesDiv = pokemonDiv.getElementsByClassName('moves')[0];

		pokemon.moves.forEach((moveName, index) => {
			const move = getMove(moveName);

			const moveDiv = movesDiv.getElementsByClassName('move')[index];
			moveDiv.setAttribute('name', moveName);
			moveDiv.classList.remove(...moveDiv.classList);
			moveDiv.classList.add('move', move.type.toLowerCase());

			/** @type {HTMLElement} - abbr de class move-name */
			const moveNameDiv = moveDiv.getElementsByClassName('move-name')[0];
			moveNameDiv.title = move.shortDesc;
			moveNameDiv.innerText = move.name;

			const movePowerDiv = moveDiv.getElementsByClassName(
				'move-power'
			)[0];
			if (move.category !== 'Status') {
				movePowerDiv.innerText = move.basePower;
			} else {
				movePowerDiv.innerHTML = '';
			}

			const moveIconDiv = moveDiv.getElementsByClassName('move-icon')[0];
			moveIconDiv.src = getMoveCategoryIcon(move.category);
		});
	}

	/**
	 * - Met à jour la div edit correspondant au pokemon
	 * @param {Number} teamNumber - Numéro de l'équipe : 1 ou 2
	 * @param {Number} pokemonIndex - Index du pokemon dans la liste teams[teamNumber - 1]
	 * @param {Array} teams - Liste des deux listes de pkemons
	 */
	function updatePokemonDivEdit(teamNumber, pokemonIndex, teams) {
		const pokemon = teams[teamNumber - 1][pokemonIndex];
		const climate = document.getElementById('climate').value;

		/** @type {HTMLElement} - Bloc pricipal */
		const pokemonDivEdit = document.getElementById(
			`team${teamNumber}-pokemon${pokemonIndex}-edit`
		);

		// ? Bloc 1 - Preview ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant les informations */
		const previewDiv = pokemonDivEdit.getElementsByClassName('preview')[0];

		// 1.4 - L'objet
		const itemSelect = previewDiv.getElementsByClassName('item-select')[0];
		itemSelect.value = pokemon.getItem();

		// 1.5 - La capacité
		const abilitySelect = previewDiv.getElementsByClassName(
			'ability-select'
		)[0];
		abilitySelect.value = pokemon.ability;

		// 1.6 - La nature
		const natureSelect = previewDiv.getElementsByClassName(
			'nature-select'
		)[0];
		natureSelect.value = pokemon.nature;

		// ? Bloc 2 - Moves ==========================================================================================

		/** @type {HTMLElement} - Bloc contenant le set */
		const movesDiv = pokemonDivEdit.getElementsByClassName('moves')[0];
		for (let i = 0; i < 4; i++) {
			let moveSelect = movesDiv.getElementsByClassName(`move${i}`)[0];
			moveSelect.value = pokemon.moves[i];
		}

		// ? Bloc 3 - Statistiques ==========================================================================================

		const statsDiv = pokemonDivEdit.getElementsByClassName(
			'stats-table'
		)[0];
		statsList.forEach((statName) => {
			// IV
			const ivInput = statsDiv
				.getElementsByClassName(statName)[0]
				.getElementsByClassName('iv-input')[0];
			ivInput.value = pokemon.iv[statName];

			// EV
			const evInput = statsDiv
				.getElementsByClassName(statName)[0]
				.getElementsByClassName('ev-input')[0];
			evInput.value = pokemon.ev[statName];

			// Boosts
			const boostInput = statsDiv
				.getElementsByClassName(statName)[0]
				.getElementsByClassName('boost-input')[0];
			boostInput.value = pokemon.getBoost(statName);

			// Total
			const total = statsDiv
				.getElementsByClassName(statName)[0]
				.getElementsByClassName('total')[0];
			total.innerText = pokemon.getStat(statName, climate);
		});
	}

	function getStatsDiv(pokemon) {
		/** @type {HTMLElement} - Bloc contenant les statistiques que l'on renvoie */
		const statsDiv = document.createElement('div');
		statsDiv.classList.add('stats');

		const statsHeader = document.createElement('h2');
		statsDiv.appendChild(statsHeader);
		statsHeader.classList.add('stats-header');
		statsHeader.innerText = 'Stats';

		const statsTable = document.createElement('div');
		statsDiv.appendChild(statsTable);
		statsTable.classList.add('stats-table');

		/** @type{HTMLElement} - Ligne contenant les titres des colonnes */
		const headerLine = document.createElement('div');
		statsTable.appendChild(headerLine);
		headerLine.classList.add('header', 'line');

		// Nom des stats
		const headerLineName = document.createElement('div');
		headerLine.appendChild(headerLineName);
		headerLineName.classList.add('name', 'cell');

		// Stats de base
		const headerLineBaseStat = document.createElement('div');
		headerLine.appendChild(headerLineBaseStat);
		headerLineBaseStat.classList.add('baseStats', 'cell');
		headerLineBaseStat.innerText = 'Base stats';

		// IV
		const headerLineIV = document.createElement('div');
		headerLine.appendChild(headerLineIV);
		headerLineIV.classList.add('iv', 'cell');
		headerLineIV.innerText = 'IV';

		// EV
		const headerLineEV = document.createElement('div');
		headerLine.appendChild(headerLineEV);
		headerLineEV.classList.add('ev', 'cell');
		headerLineEV.innerText = 'EV';

		// Boost
		const headerLineBoost = document.createElement('div');
		headerLine.appendChild(headerLineBoost);
		headerLineBoost.classList.add('boost', 'cell');
		headerLineBoost.innerText = 'Boosts';

		// Total
		const headerLineTotal = document.createElement('div');
		headerLine.appendChild(headerLineTotal);
		headerLineTotal.classList.add('total', 'cell');
		headerLineTotal.innerText = 'Total';

		// Pour chaque statistique, on créé et remplie la ligne
		statsList.forEach((statName) => {
			const line = document.createElement('div');
			statsTable.appendChild(line);
			line.classList.add(statName, 'line');

			// Nom de la statistique
			const name = document.createElement('div');
			line.appendChild(name);
			name.classList.add('name', 'cell');
			name.innerText = statName;

			// Base stats
			const baseStat = document.createElement('div');
			line.appendChild(baseStat);
			baseStat.classList.add('baseStats', 'cell');
			baseStat.innerText = pokemon.baseStats[statName];

			// IV
			const iv = document.createElement('div');
			line.appendChild(iv);
			const ivInput = document.createElement('input');
			iv.appendChild(ivInput);
			ivInput.classList.add('iv-input');
			ivInput.setAttribute('type', 'number');
			ivInput.setAttribute('min', 0);
			ivInput.setAttribute('max', 31);
			iv.classList.add('iv', 'cell');
			ivInput.value = pokemon.iv[statName];
			ivInput.addEventListener('focus', () => {
				selectText(ivInput);
			});
			ivInput.addEventListener('change', () => {
				ivInput.value = +ivInput.value;
				if (0 <= +ivInput.value && +ivInput.value <= 31) {
					pokemon.iv[statName] = +ivInput.value;
					total.innerText = pokemon.getStat(statName, climate);
				}
			});

			// EV
			const ev = document.createElement('div');
			line.appendChild(ev);
			const evInput = document.createElement('input');
			ev.appendChild(evInput);
			evInput.classList.add('ev-input');
			evInput.setAttribute('type', 'number');
			evInput.setAttribute('min', 0);
			evInput.setAttribute('max', 252);
			evInput.setAttribute('step', 4);
			ev.classList.add('ev', 'cell');
			evInput.value = pokemon.ev[statName];
			evInput.addEventListener('focus', () => {
				selectText(evInput);
			});
			evInput.addEventListener('change', () => {
				evInput.value = +evInput.value;
				if (0 <= +evInput.value && +evInput.value <= 252) {
					pokemon.ev[statName] = +evInput.value;
					total.innerText = pokemon.getStat(statName, climate);
				}

				// On modifie la valeur max des autres evs
				let remainingEvs = 508;
				for (const evInput of statsDiv.getElementsByClassName(
					'ev-input'
				)) {
					remainingEvs -= +evInput.value;
				}
				for (const evInput of statsDiv.getElementsByClassName(
					'ev-input'
				)) {
					evInput.setAttribute(
						'max',
						Math.min(252, +evInput.value + remainingEvs)
					);
				}
			});

			// Boost
			const boostDiv = document.createElement('div');
			line.appendChild(boostDiv);
			const boostInput = document.createElement('input');
			boostDiv.appendChild(boostInput);
			boostInput.classList.add('boost-input');
			boostInput.setAttribute('type', 'number');
			boostInput.setAttribute('min', -6);
			boostInput.setAttribute('max', 6);
			boostInput.setAttribute('step', 1);
			boostDiv.classList.add('boost', 'cell');
			boostInput.value = pokemon.getBoost(statName);
			boostInput.addEventListener('focus', () => {
				selectText(boostInput);
			});
			boostInput.addEventListener('change', () => {
				boostInput.value = Math.floor(+boostInput.value);
				if (-6 <= boostInput.value && boostInput.value <= 6) {
					// On modifie le boost du pokemon
					pokemon.setBoost(statName, boostInput.value);
					// On actualise le total
					total.innerText = pokemon.getStat(statName, climate);
				}
			});

			// Total
			const total = document.createElement('div');
			line.appendChild(total);
			total.classList.add('total', 'cell');
			total.innerText = pokemon.getStat(statName, climate);
		});

		return statsDiv;
	}

	/**
	 * - Met à jour tous les pokemons
	 */
	function updateAllPokemons() {
		for (let teamNumber = 1; teamNumber < 3; teamNumber++) {
			for (
				let pokemonIndex = 0;
				pokemonIndex < teams[teamNumber - 1].length;
				pokemonIndex++
			) {
				// On actualise l'affichage
				updatePokemonDiv(teamNumber, pokemonIndex, teams);
				updatePokemonDivEdit(teamNumber, pokemonIndex, teams);
			}
		}
	}
});

// ! - Fin du DOM ==================================================================================================

function getDamagesDiv(pokemonOffensive, pokemonDefensive, move) {
	// Calculs
	const maxPv = calcDamages(pokemonOffensive, pokemonDefensive, move, ['hp']);
	const max = Math.round(
		(100 * maxPv) / pokemonDefensive.getStat('hp', climate)
	);
	const minPv = 0.85 * maxPv;
	const min = Math.round(
		(100 * minPv) / pokemonDefensive.getStat('hp', climate)
	);
	const averagePv = (maxPv + minPv) / 2;
	const average = Math.round(
		(100 * averagePv) / pokemonDefensive.getStat('hp', climate)
	);

	const htk = Math.ceil(100 / average);

	// Bloc des dégats
	const damageDiv = document.createElement('div');
	damageDiv.classList.add('damage-content');

	// Si des dégats sont infligés, alors on remplie la div
	if (move.category !== 'Status' && max > 0) {
		damageDiv.classList.add(move.type.toLowerCase());
		const damageLine = document.createElement('div');
		damageDiv.appendChild(damageLine);
		damageLine.classList.add('damage-firstLine');

		const averageDiv = document.createElement('p');
		damageLine.appendChild(averageDiv);
		averageDiv.classList.add('average');
		averageDiv.innerText = `${average}%`;

		const minmaxDiv = document.createElement('div');
		damageLine.appendChild(minmaxDiv);
		minmaxDiv.classList.add('minmax');

		const minDiv = document.createElement('p');
		minmaxDiv.appendChild(minDiv);
		minDiv.classList.add('min');
		minDiv.innerText = `${min}%`;

		const maxDiv = document.createElement('p');
		minmaxDiv.appendChild(maxDiv);
		maxDiv.classList.add('max');
		maxDiv.innerText = `${max}%`;

		// Bloc du nom du move
		const moveNameDiv = document.createElement('p');
		damageDiv.appendChild(moveNameDiv);
		moveNameDiv.classList.add('damage-moveName');
		moveNameDiv.innerText = move.name;

		// Bloc du nombre de coups pour tuer
		const hitsToKillDiv = document.createElement('p');
		damageDiv.appendChild(hitsToKillDiv);
		hitsToKillDiv.classList.add('damage-htk');
		hitsToKillDiv.innerText = `${htk}-HKO`;
	}
	return damageDiv;
}

/**
 * - Vide toutes les "div.damage"
 */
function resetDamages() {
	for (const div of document.getElementsByClassName('damage')) {
		div.innerHTML = '';
	}
}

/**
 * - Vide toutes les "div.damage"
 */
function resetSpeedColor() {
	for (const div of document.getElementsByClassName('pokemon-show')) {
		div.classList.remove(
			'border-low-speed',
			'border-high-speed',
			'border-equal-speed'
		);
	}
	for (const div of document.getElementsByClassName('speed')) {
		div.classList.remove('low-speed', 'high-speed', 'equal-speed');
	}
}

function appendMovePopup(move, container) {
	container.classList.add('move-popup', 'card-popup');

	const description = formatDesc(move.shortDesc);
	const type = move.type;
	const category = move.category;
	const power = move.basePower;
	const accuracy = move.accuracy;

	const descriptionDiv = document.createElement('div');
	container.appendChild(descriptionDiv);
	descriptionDiv.classList.add('description', 'mb-1rem');
	descriptionDiv.innerHTML = description;

	const typeDiv = document.createElement('div');
	container.appendChild(typeDiv);
	typeDiv.classList.add('popup-type', 'mb-1rem');
	const typeName = document.createElement('p');
	typeDiv.appendChild(typeName);
	typeName.classList.add('name');
	typeName.innerText = 'Type : ';
	const typeValue = document.createElement('p');
	typeDiv.appendChild(typeValue);
	typeValue.innerText = move.type;
	typeValue.classList.add('type', move.type.toLowerCase());

	const categoryDiv = document.createElement('div');
	container.appendChild(categoryDiv);
	categoryDiv.classList.add('category', 'mb-1rem');
	const categoryName = document.createElement('p');
	categoryDiv.appendChild(categoryName);
	categoryName.classList.add('name');
	categoryName.innerText = 'Category : ';
	const categoryContent = document.createElement('div');
	categoryDiv.appendChild(categoryContent);
	categoryContent.classList.add('category-content');
	const categoryImage = document.createElement('img');
	categoryContent.appendChild(categoryImage);
	categoryImage.src = getMoveCategoryIcon(move.category);
	const categoryValue = document.createElement('p');
	categoryContent.appendChild(categoryValue);
	categoryValue.innerText = move.category;

	const powerDiv = document.createElement('div');
	// On ajoute la div de puissance que si ce n'est pas une attaque de status de Status
	if (move.category !== 'Status') {
		container.appendChild(powerDiv);
	}
	powerDiv.classList.add('power', 'mb-1rem');
	const powerName = document.createElement('p');
	powerDiv.appendChild(powerName);
	powerName.classList.add('name');
	powerName.innerText = 'Power : ';
	const powerValue = document.createElement('p');
	powerDiv.appendChild(powerValue);
	powerValue.innerText = `${power}BP`;

	const accuracyDiv = document.createElement('div');
	// On ajoute la div de précision que si elle est différente de true
	if (accuracy !== true) {
		container.appendChild(accuracyDiv);
	}
	accuracyDiv.classList.add('accuracy');
	const accuracyName = document.createElement('p');
	accuracyDiv.appendChild(accuracyName);
	accuracyName.classList.add('name');
	accuracyName.innerText = 'Accuracy : ';
	const accuracyValue = document.createElement('p');
	accuracyDiv.appendChild(accuracyValue);
	accuracyValue.innerText = `${accuracy}%`;
}

/**
 * - Renvoie la div du status du pokemon
 * @param {Pokemon} pokemon
 * @returns {HTMLElement}
 */
function getStatusDiv(pokemon) {
	const status = pokemon.getStatus();
	const statusDiv = document.createElement('div');
	statusDiv.classList.add('status-container');
	const statusSpan = document.createElement('div');
	statusDiv.appendChild(statusSpan);
	statusSpan.classList.add('status');
	const text = document.createElement('abbr');
	statusSpan.appendChild(text);
	if (status.type) {
		statusSpan.classList.add(status.type);
	}
	text.innerText = status.display;
	text.title = status.desc;

	return statusDiv;
}

/**
 * - Renvoie la div des boosts du pokemon
 * @param {Pokemon} pokemon
 * @returns {HTMLElement}
 */
function getBoostDiv(pokemon) {
	// const boost = pokemon.getboost();
	const boostDiv = document.createElement('p');
	boostDiv.classList.add('boost');
	const boostSpan = document.createElement('span');
	boostDiv.appendChild(boostSpan);
	boostSpan.classList.add('boost-value');

	let nbBoosts = 0;
	// Pour chaque stat
	statsList.forEach((stat) => {
		const boost = pokemon.getBoost(stat);
		// Si son boost n'est pas nul
		if (boost !== 0) {
			// On ajoute le boost au p
			boostDiv.innerHTML += `${nbBoosts > 0 ? ' / ' : ''}`;
			if (boost > 0) {
				boostDiv.innerHTML += `<span class="high-boost">+${boost} ${capitalize(
					stat
				)}</span>`;
			} else {
				boostDiv.innerHTML += `<span class="low-boost">${boost} ${capitalize(
					stat
				)}</span>`;
			}
			nbBoosts++;
		}
	});

	if (nbBoosts === 0) {
		boostDiv.style.display = 'none';
	}

	return boostDiv;
}

/**
 * - Renvoie le move (objet) qui fait le plus de dégats
 * @param {Pokemon} offensivePokemon
 * @param {Pokemon} defensivePokemon
 * @returns {Move}
 */
function getBestMove(offensivePokemon, defensivePokemon) {
	let bestMove = getMove(offensivePokemon.moves[0]);
	for (let i = 1; i < offensivePokemon.moves.length; i++) {
		const move = getMove(offensivePokemon.moves[i]);
		if (
			calcDamages(offensivePokemon, defensivePokemon, move, ['hp']) >
			calcDamages(offensivePokemon, defensivePokemon, bestMove, ['hp'])
		) {
			bestMove = move;
		}
	}
	return bestMove;
}

/**
 * - Renvoie SpA pour spa ...
 * @param {String} stat - Nom de la statistique
 * @returns {String | null}
 */
function formatStatName(stat) {
	switch (stat.toLowerCase()) {
		case 'hp':
			return 'Hp';
		case 'atk':
			return 'Atk';
		case 'def':
			return 'Def';
		case 'spa':
			return 'SpA';
		case 'spd':
			return 'SpD';
		case 'spe':
			return 'Spe';

		default:
			console.error(`Impossible de faire formatStatName(${stat})`);
			break;
	}
}

/**
 * - Renvoie l'object des boost
 * @param {String} moveName - Nom du move
 * @returns {Object} - Les boosts
 */
function getMoveBoosts(moveName) {
	const move = getMove(moveName);
	let moveBoosts;
	// Les status à 100%
	if (move.boosts) {
		moveBoosts = move.boosts;
	}
	// Les attaques à 100%
	if (move.self && move.self.boosts) {
		moveBoosts = move.self.boosts;
	}

	// Les boosts occasionnels
	if (move.secondary && move.secondary.self && move.secondary.self.boosts) {
		moveBoosts = move.secondary.self.boosts;
	}

	return moveBoosts;
}

/**
 * - Renvoie l'url de image de l'objet
 * @param {String} itemName - Nom de l'objet
 * @returns {String} - url de l'image
 */
function getItemUrl(itemName) {
	if (itemName === 'Assault Vest') {
		return '../images/pokemon/assault-vest.png';
	}
	if (itemName === 'Heavy-Duty Boots') {
		return '../images/pokemon/boots.png';
	}
	if (itemName === "King's Rock") {
		return 'https://play.pokemonshowdown.com/sprites/itemicons/kings-rock.png';
	}
	if (itemName === 'Weakness Policy') {
		return '../images/pokemon/weaknesspolicy.png';
	}
	if (itemName === 'Throat Spray') {
		return '../images/pokemon/throat spray.png';
	}
	if (itemName === 'Terrain Extender') {
		return '../images/pokemon/terrain extender.png';
	}

	return `https://play.pokemonshowdown.com/sprites/itemicons/${itemName
		.toLowerCase()
		.replaceAll(' ', '-')}.png`;
}

/**
 * - The sprite url
 * @param {String} pokemonName - Name of the pokemon
 * @returns {String}
 */
function getPokemonSprite(pokemonName) {
	switch (pokemonName) {
		case 'Silvally-%2A':
			return `https://play.pokemonshowdown.com/sprites/ani/silvally.gif`;

		case 'Sirfetch’d':
			return `https://play.pokemonshowdown.com/sprites/ani/sirfetchd.gif`;

		case 'Toxtricity-Low-Key':
			return `https://play.pokemonshowdown.com/sprites/ani/toxtricity-lowkey.gif`;

		default:
			return `https://play.pokemonshowdown.com/sprites/ani/${pokemonName.toLowerCase()}.gif`;
	}
}

/**
 * - Efface les types affichés et affiche les types du pokemon
 * @param {Pokemon} pokemon
 * @param {HTMLDivElement} pokemonDiv
 */
function displayTypes(pokemon, pokemonDiv) {
	// On efface les types affichés
	const typesDiv = pokemonDiv
		.getElementsByClassName('preview')[0]
		.getElementsByClassName('types')[0];
	while (typesDiv.childNodes.length > 0) {
		typesDiv.removeChild(typesDiv.childNodes[0]);
	}
	// On affiche les types
	const type1 = document.createElement('p');
	typesDiv.appendChild(type1);
	type1.innerText = pokemon.types[0];
	type1.classList.add('type');
	type1.classList.add('first-type');
	type1.classList.add(pokemon.types[0].toLowerCase());
	if (pokemon.types.length === 2) {
		const type2 = document.createElement('p');
		typesDiv.appendChild(type2);
		type2.classList.add('second-type');
		type2.classList.add('last-type');
		type2.innerText = pokemon.types[1];
		type2.classList.add('type');
		type2.classList.add(pokemon.types[1].toLowerCase());
	} else {
		type1.classList.add('last-type');
	}
}

function getMoveCategoryIcon(category) {
	switch (category) {
		case 'Physical':
			return `../images/pokemon/${category}.svg`;
		case 'Special':
			return `../images/pokemon/${category}.svg`;
		case 'Status':
			return `../images/pokemon/${category}.svg`;

		default:
			console.error(
				`Impossible de faire getMoveCategoryIcon(${category})`
			);
			break;
	}
}
