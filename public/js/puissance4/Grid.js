class Grid {
	// * La grille a 6 lignes et 7 colonnes, la ligne d'index 5 est celle du fond
	// * Une case a pour valeur '' si elle est vide, 'red' si elle est rouge et 'yellow' si elle est jaune

	debug = true;

	constructor() {
		this.initializeGrid();
		// this.cases = [
		//     ['', '', '', '', '', '', ''],
		//     ['', '', '', '', '', '', ''],
		//     ['', '', '', 'yellow', '', '', ''],
		//     ['', '', '', 'red', 'yellow', '', ''],
		//     ['', '', '', 'red', 'yellow', '', ''],
		//     ['', '', 'red', 'yellow', 'yellow', '', ''],
		// ];
	}

	initializeGrid() {
		this.color = 'red';
		this.cases = Array();
		for (let h = 0; h < 6; h++) {
			const raw = [];
			for (let w = 0; w < 7; w++) {
				raw.push('');
			}
			this.cases.push(raw);
		}
	}

	nb(color) {
		if (['red', 'yellow'].includes(color)) {
			let total = 0;
			for (let l = 0; l < 6; l++) {
				for (let c = 0; c < 7; c++) {
					if (this.getColor(l, c) === color) {
						total++;
					}
				}
			}
			return total;
		}
	}

	/**
	 * - Renvoie un choix de colonne
	 * @param {number} difficulty - Le niveau de difficulté de l'ordinateur, 0 par défaut
	 * @return {number} - [[0;6]]
	 */
	botChoice() {
		// * Premier coup
		if (this.nb('yellow') === 0) {
			return this.firstShot();
		}

		// * On renvoie une colonne ou l'ordi gagne si on trouve
		for (let c = 0; c < 7; c++) {
			let line = this.firstAvailableLine(c);
			let caseWinners = this.positionWinner(line, c);
			if (caseWinners.includes('yellow')) {
				return c;
			}
		}

		// * On renvoie une colonne ou l'humain gagne si on trouve
		for (let c = 0; c < 7; c++) {
			let line = this.firstAvailableLine(c);
			let caseWinners = this.positionWinner(line, c);
			if (caseWinners.includes('red')) {
				return c;
			}
		}

		// * On ne se place pas en dessous d'une case qui nous ferait perdre
		/** @type{Array.<number>} Liste des colonnes qui ne permettent pas à l'adversaire de gagner si l'on s'y met puis lui ensuite */
		let notLoosingColumns = this.getNotLoosingCulumns('red');

		/** @type{Array.<number>} Liste des colonnes qui permettent à l'adversaire de nous empêcher de gagner si l'on s'y met puis lui ensuite */
		let notWinningColumns = this.getNotLoosingCulumns('yellow');

		// console.log(`notLoosingColumns : ${notLoosingColumns}`);
		// console.log(`notWinningColumns : ${notWinningColumns}`);

		// * Fourchettes

		// Si on peut provoquer une fourchette, alors on le fait
		for (let column = 0; column < 7; column++) {
			if (
				!this.columnIsFull(column) &&
				notLoosingColumns.includes(column) &&
				this.fork2(column, 'yellow', true)
			) {
				console.log(`Fourchette jaune en ${column}`);
				return column;
			}
		}

		// Si on peut éviter une fourchette, alors on le fait
		for (let column = 0; column < 7; column++) {
			if (
				!this.columnIsFull(column) &&
				notLoosingColumns.includes(column) &&
				this.fork2(column, 'red', true)
			) {
				console.log(`Pas de fourchette rouge en ${column}`);
				return column;
			}
		}

		// * Alignements de trois avec un trou au milieu
		// for (let column = 0; column < 7; column++) {
		//     if (this.trouAuMilieu('yellow', column)) {
		//         return column;
		//     }
		// }
		// for (let column = 0; column < 7; column++) {
		//     if (this.trouAuMilieu('red', column)) {
		//         return column;
		//     }
		// }

		// * Alignements de trois
		if (false) {
			for (let column = 0; column < 7; column++) {
				const line = this.firstAvailableLine(column);
				// Pour chaque direction H, D1 et D2
				['H', 'D1', 'D2'].forEach((direction) => {
					// Pour chaque décalage
					for (let decalage = 0; decalage < 4; decalage++) {
						// Si la combinaison est possible
						if (
							this.getCasePosition(
								line,
								column,
								direction,
								decalage,
								0,
								4
							)
						) {
							console.log(
								`Alignement de trois pour this.getCasePosition(${line}, ${column}, ${direction}, ${decalage}, 0, 4)`
							);
							// Je note les positions des 4 cases
							const cases = [];
							for (let position = 0; position < 4; position++) {
								cases.push(
									this.getCasePosition(
										line,
										column,
										direction,
										decalage,
										position,
										4
									)
								);
							}
							// Si 3 sont de ma couleur et que la 4e n'est pas directement bouchable
							let nbYellow = 0;
							let nbEmpty = 0;
							let videEstBouchable = false;
							for (let i = 0; i < 4; i++) {
								let color = this.getColor(
									cases[i][0],
									cases[i][1]
								);
								if (color === 'yellow') {
									nbYellow++;
								}
								if (color === '') {
									nbEmpty++;
									if (
										this.firstAvailableLine(cases[i][1]) ===
										cases[i][0]
									) {
										videEstBouchable = true;
									}
								}
							}
							console.log(
								`Il y a ${nbYellow} jaunes et ${nbEmpty} vide : ${
									videEstBouchable ? 'ok' : 'pas ok'
								}`
							);
							if (
								nbYellow === 3 &&
								nbEmpty === 1 &&
								!videEstBouchable
							) {
								// Alors je m'y met
								return column;
							}
						}
					}
				});
			}
		}

		// * Alignement de trois parmis quatres
		for (let c = 0; c < 7; c++) {
			let column = centralColumn(c);
			if (
				notLoosingColumns.includes(column) &&
				notWinningColumns.includes(column) &&
				this.align3(column, 'yellow')
			) {
				console.log(`3 jaunes parmis 4 colonne ${column}`);
				return column;
			}
			if (
				notLoosingColumns.includes(column) &&
				notWinningColumns.includes(column) &&
				this.align3(column, 'red')
			) {
				console.log(`Pas 3 rouges parmis 4 colonne ${column}`);
				return column;
			}
		}

		// * Alignement de deux parmis quatres
		for (let c = 0; c < 7; c++) {
			let column = centralColumn(c);
			if (
				notLoosingColumns.includes(column) &&
				notWinningColumns.includes(column) &&
				this.align(column, 'yellow', 2)
			) {
				console.log(`Deux jaunes parmis quatre colonne ${column}`);
				return column;
			}
			if (
				notLoosingColumns.includes(column) &&
				notWinningColumns.includes(column) &&
				this.align(column, 'red', 2)
			) {
				console.log(`Pas deux rouges parmis quatre colonne ${column}`);
				return column;
			}
		}

		// * On choisit une colonne aléatoire parmis les restantes qui ne nous font pas perdre au tour suivant
		if (notLoosingColumns.length > 0) {
			let choice;
			choice = +Math.floor(Math.random() * notLoosingColumns.length);
			if (this.debug) {
				console.log(`Choix aléatoire qui ne nous fait pas perdre`);
			}
			return notLoosingColumns[choice];
		}

		// * On choisit une colonne aléatoire non pleine
		let choice;
		do {
			choice = +Math.floor(Math.random() * 7);
		} while (this.columnIsFull(choice));
		if (this.debug) {
			console.log(`Choix aléatoire`);
		}
		return choice;
	}

	firstShot() {
		if (this.getColor(5, 3) === 'red') {
			return 2;
		}
		return 3;
	}

	/**
	 * - Renvoie vrai si en plaçant une pièce dans cette colonne on en aligne deux parmis les quatres
	 * @param {number} column
	 * @param {String} color
	 * @return {Boolean}
	 */
	align(column, color, number) {
		const line = this.firstAvailableLine(column);
		const directions = ['H', 'D1', 'D2', 'V'];
		let res = false;
		directions.forEach((direction) => {
			for (let decalage = 0; decalage < 4; decalage++) {
				// Si la combinaison est valide
				if (
					this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						0,
						4
					)
				) {
					// On note les 4 cases
					let c0 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						0,
						4
					);
					let c1 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						1,
						4
					);
					let c2 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						2,
						4
					);
					let c3 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						3,
						4
					);
					let cases = [c0, c1, c2, c3];
					let nbYellow = 0;
					let nbRed = 0;
					let nbEmpty = 0;
					for (let i = 0; i < 4; i++) {
						let l = cases[i][0];
						let c = cases[i][1];
						if (c === column) {
							if (color === 'red') {
								nbRed++;
							} else {
								nbYellow++;
							}
						} else {
							if (this.getColor(l, c) === 'red') {
								nbRed++;
							}
							if (this.getColor(l, c) === 'yellow') {
								nbYellow++;
							}
							if (this.getColor(l, c) === '') {
								nbEmpty++;
							}
						}
					}
					if (
						color === 'yellow' &&
						nbYellow === number &&
						nbEmpty === 4 - number
					) {
						// Pour chaque case différente de celle qu'on ajoute
						for (let i = 0; i < 4; i++) {
							// Si elle est vide et non atteignable
							if (
								cases[i][1] !== column &&
								this.getColor(cases[i][0], cases[i][1]) ===
									'' &&
								cases[i][0] <
									this.firstAvailableLine(cases[i][1])
							) {
								console.log(
									`On peux aligner ${number} ${color}s en se plaçant en ${column}`
								);
								res = true;
							}
						}
					}
					if (
						color === 'red' &&
						nbRed === number &&
						nbEmpty === number - 1
					) {
						// Pour chaque case différente de celle qu'on ajoute
						for (let i = 0; i < 4; i++) {
							// Si elle est vide et non atteignable
							if (
								cases[i][1] !== column &&
								this.getColor(cases[i][0], cases[i][1]) ===
									'' &&
								cases[i][0] <
									this.firstAvailableLine(cases[i][1])
							) {
								console.log(
									`On peux aligner ${number} ${color}s en se plaçant en ${column}`
								);
								res = true;
							}
						}
					}
				}
			}
		});
		return res;
	}

	/**
	 * - Renvoie vrai si en plaçant une pièce dans cette colonne on en aligne trois parmis les quatres
	 * @param {number} column
	 * @param {String} color
	 * @return {Boolean}
	 */
	align3(column, color) {
		const line = this.firstAvailableLine(column);
		const directions = ['H', 'D1', 'D2'];
		let res = false;
		directions.forEach((direction) => {
			for (let decalage = 0; decalage < 4; decalage++) {
				// Si la combinaison est valide
				if (
					this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						0,
						4
					)
				) {
					let c0 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						0,
						4
					);
					let c1 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						1,
						4
					);
					let c2 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						2,
						4
					);
					let c3 = this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						3,
						4
					);
					let cases = [c0, c1, c2, c3];
					let nbYellow = 0;
					let nbRed = 0;
					let nbEmpty = 0;
					for (let i = 0; i < 4; i++) {
						let l = cases[i][0];
						let c = cases[i][1];
						if (c === column) {
							if (color === 'red') {
								nbRed++;
							} else {
								nbYellow++;
							}
						} else {
							if (this.getColor(l, c) === 'red') {
								nbRed++;
							}
							if (this.getColor(l, c) === 'yellow') {
								nbYellow++;
							}
							if (this.getColor(l, c) === '') {
								nbEmpty++;
							}
						}
					}
					if (color === 'yellow' && nbYellow === 3 && nbEmpty === 1) {
						for (let i = 0; i < 4; i++) {
							if (
								cases[i][1] !== column &&
								this.getColor(cases[i][0], cases[i][1]) ===
									'' &&
								cases[i][0] <
									this.firstAvailableLine(cases[i][1])
							) {
								res = true;
							}
						}
					}
					if (color === 'red' && nbRed === 3 && nbEmpty === 1) {
						for (let i = 0; i < 4; i++) {
							if (
								cases[i][1] !== column &&
								this.getColor(cases[i][0], cases[i][1]) ===
									'' &&
								cases[i][0] <
									this.firstAvailableLine(cases[i][1])
							) {
								res = true;
							}
						}
					}
				}
			}
		});
		return res;
	}

	/**
	 * - Liste des colonnes qui ne permettent pas à !color de gagner si color s'y place
	 * @param {String} color - couleur de la seconde pièce, celle qui gagnerait
	 * @returns {Array}
	 */
	getNotLoosingCulumns(color) {
		/** @type{Array.<number>} Liste des colonnes qui ne permettent pas à l'adversaire de gagner si l'on s'y met puis lui ensuite */
		let okColumns = [];
		// On liste toutes les collonnes non-pleines
		for (let c = 0; c < 7; c++) {
			if (!this.columnIsFull(c)) {
				okColumns.push(c);
			}
		}
		// On supprime celles où il gagne en se plaçant au dessus de nous
		// console.log(`okColumns : ${okColumns}`);
		okColumns.forEach((column) => {
			if (this.debug && color === 'red') {
				// console.log(`${column} ?`);
			}
			let line = this.firstAvailableLine(column);
			if (
				line > 0 &&
				this.positionWinner(line - 1, column).includes(color)
			) {
				if (this.debug && color === 'red') {
					// console.log(`-> danger`);
				}
				okColumns = okColumns.filter((c) => c !== column);
			} else {
				if (this.debug && color === 'red') {
					// console.log('-> ok');
				}
			}
		});
		return okColumns;
	}

	trouAuMilieu(color, column) {
		const directions = ['H', 'D1', 'D2'];
		const line = this.firstAvailableLine(column);
		// Pour chaque direction envisageable
		directions.forEach((direction) => {
			// Pour chaque décalage
			for (let decalage = 0; decalage < 4; decalage++) {
				// On note les 4 cases
				let cases = [];
				for (let i = 0; i < 4; i++) {
					if (i === decalage) {
						cases.push(color);
					} else {
						cases.push(
							this.getCase2(
								line,
								column,
								direction,
								decalage,
								i,
								4
							)
						);
					}
				}
				let c0 = cases[0];
				let c1 = cases[1];
				let c2 = cases[2];
				let c3 = cases[3];

				// Si la première, la dernière et une des 2 du milieu sont de la couleur color et la restante est vide avec au moins une case vide en dessous
				if (c0 === color && c3 === color) {
					// Le trou est en c2
					if (
						(c1 === color && c2 === '') ||
						(c1 === '' && c2 === color)
					) {
						// Alors c'est bon, on renvoie column
						return true;
					}
				}
			}
			return false;
		});
	}

	/**
	 * - Renvoie la position [line, column] de la case ou faux si les paramètres ne sont pas correctes
	 * @param {number} line
	 * @param {number} column
	 * @param {String} direction - H, V, D1 ou D2
	 * @param {number} decalage - [[0;length-1]]
	 * @param {number} position - [[0;length-1]]
	 * @param {number} length - [[2;4]]
	 * @returns {Array.<number>|Boolean}
	 */
	getCasePosition(line, column, direction, decalage, position, length) {
		if (
			this.positionIsValid(line, column) &&
			2 <= length &&
			length <= 6 &&
			0 <= decalage &&
			decalage < length &&
			0 <= position &&
			position < length &&
			['H', 'V', 'D1', 'D2'].includes(direction)
		) {
			let firstLine;
			let firstColumn;
			let lastLine;
			let lastColumn;

			switch (direction) {
				case 'H':
					firstLine = line;
					lastLine = line;
					firstColumn = column - decalage;
					lastColumn = column - decalage + length - 1;
					if (
						0 <= firstLine &&
						lastLine < 6 &&
						0 <= firstColumn &&
						lastColumn < 7
					) {
						const caseLine = line;
						const caseColumn = column - decalage + position;
						return [caseLine, caseColumn];
					}
					break;
				case 'V':
					firstLine = line - decalage;
					lastLine = line - decalage + length - 1;
					firstColumn = column;
					lastColumn = column;
					if (
						0 <= firstLine &&
						lastLine < 6 &&
						0 <= firstColumn &&
						lastColumn < 7
					) {
						const caseLine = line - decalage + position;
						const caseColumn = column;
						return [caseLine, caseColumn];
					}
					break;
				case 'D1':
					firstLine = line - decalage;
					lastLine = line - decalage + length - 1;
					firstColumn = column - decalage;
					lastColumn = column - decalage + length - 1;
					if (
						0 <= firstLine &&
						lastLine < 6 &&
						0 <= firstColumn &&
						lastColumn < 7
					) {
						const caseLine = line - decalage + position;
						const caseColumn = column - decalage + position;
						return [caseLine, caseColumn];
					}
					break;
				case 'D2':
					firstLine = line - decalage;
					lastLine = line - decalage + length - 1;
					firstColumn = column + decalage - length + 1;
					lastColumn = column + decalage;
					if (
						0 <= firstLine &&
						lastLine < 6 &&
						0 <= firstColumn &&
						lastColumn < 7
					) {
						const caseLine = line - decalage + position;
						const caseColumn = column + decalage - position;
						return [caseLine, caseColumn];
					}
					break;
			}
		}
		return false;
	}

	/**
	 * - Renvoie vrai si en plaçant une piece de la couleur dans la colonne provoque un alignement de 3 en diagonale où les 2 extrémités sont disponibles
	 * @param {number} column
	 * @param {String} color
	 * @return {Boolean}
	 */
	fork2(column, color, debug = false) {
		const line = this.firstAvailableLine(column);
		const directions = ['V', 'H', 'D1', 'D2'];

		// Pour chaque couple de combinaison de 4 cases alignées
		for (let i = 0; i < directions.length; i++) {
			const direction1 = directions[i];
			for (let j = 0; j < directions.length; j++) {
				const direction2 = directions[j];
				for (let decalage1 = 0; decalage1 < 4; decalage1++) {
					for (let decalage2 = 0; decalage2 < 4; decalage2++) {
						let cases1 = [];
						let cases2 = [];
						// On vérifie que les 2 combinaisons existent et sont différentes
						if (
							(direction1 !== direction2 ||
								decalage1 !== decalage2) &&
							this.getCasesInCombinaison(
								line,
								column,
								direction1,
								decalage1,
								4
							) &&
							this.getCasesInCombinaison(
								line,
								column,
								direction2,
								decalage2,
								4
							)
						) {
							cases1 = this.getCasesInCombinaison(
								line,
								column,
								direction1,
								decalage1,
								4
							);
							cases2 = this.getCasesInCombinaison(
								line,
								column,
								direction2,
								decalage2,
								4
							);
							// On garde les cases différente de celle qu'on teste
							let k1 = 0;
							while (k1 < cases1.length) {
								let l1 = cases1[k1][0];
								let c1 = cases1[k1][1];
								if (l1 === line && c1 === column) {
									cases1 = removeElementByIndex(cases1, k1);
								} else {
									k1++;
								}
							}
							let k2 = 0;
							while (k2 < cases2.length) {
								let l2 = cases2[k2][0];
								let c2 = cases2[k2][1];
								if (l2 === line && c2 === column) {
									cases2 = removeElementByIndex(cases2, k2);
								} else {
									k2++;
								}
							}
							// Si ce sont deux alignements de 2/3 de la couleur
							if (
								this.combinaisonColors(cases1, color) === 2 &&
								this.combinaisonColors(cases1, '') === 1 &&
								this.combinaisonColors(cases2, color) === 2 &&
								this.combinaisonColors(cases2, '') === 1
							) {
								// Si les deux cases vides sont soit directement atteignables, soit l'une au dessus de l'autre
								let emptyCase1 = this.getEmptycasePosition(
									cases1
								);
								let emptyCase2 = this.getEmptycasePosition(
									cases2
								);
								let line1 = emptyCase1[0];
								let line2 = emptyCase2[0];
								let column1 = emptyCase1[1];
								let column2 = emptyCase2[1];
								// Si elles sont directement atteignables
								if (
									(this.firstAvailableLine(column1) ===
										line1 &&
										this.firstAvailableLine(column2) ===
											line2) ||
									(column1 === column2 &&
										(line1 === line2 + 1 ||
											line1 === line2 - 1)) ||
									(this.firstAvailableLine(column1) ===
										line1 &&
										line2 === line1 - 1) ||
									(this.firstAvailableLine(column2) ===
										line2 &&
										line1 === line2 - 1)
								) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
		// Si on leur trouve respectivement un decalage (decalage1 et decalage2) et une position (d1p et d2p)
		// Avec direction1 != direction2 ou decalage1 != decalage2 ou d1p != d2p
		// qui forment deux alignements de 3/4
		// Où les deux manquants sont soit directements atteignables, soient l'un au dessus de l'autre
		// Alors on renvoit vrai
	}

	/**
	 * - Renvoie le nombre de cases de la couleur demandée de la liste de cases
	 * @param {Array} combinaison - Liste de cases (tableau [ligne, colonne])
	 * @param {String} color
	 * @returns {Array}
	 */
	combinaisonColors(combinaison, color = 'empty', debug = false) {
		let nbRed = 0;
		let nbYellow = 0;
		let nbEmpty = 0;
		combinaison.forEach((position) => {
			let line = position[0];
			let column = position[1];
			if (this.getColor(line, column) === 'red') {
				if (debug) {
					console.log(`L${line} C${column} : red`);
				}
				nbRed++;
			} else if (this.getColor(line, column) === 'yellow') {
				if (debug) {
					console.log(`L${line} C${column} : yellow`);
				}
				nbYellow++;
			} else {
				if (debug) {
					console.log(`L${line} C${column} : vide`);
				}
				nbEmpty++;
			}
		});
		if (color === 'red') {
			return nbRed;
		}
		if (color === 'yellow') {
			return nbYellow;
		}
		return nbEmpty;
	}

	getEmptycasePosition(casesList) {
		// On vérifie qu'il n'y a qu'une seule case vide
		let nbEmpty = 0;
		let emptyLine;
		let emptyColumn;
		for (let i = 0; i < casesList.length; i++) {
			let line = casesList[i][0];
			let column = casesList[i][1];
			if (this.getColor(line, column) === '') {
				nbEmpty++;
				emptyLine = line;
				emptyColumn = column;
			}
		}
		if (nbEmpty === 1) {
			return [emptyLine, emptyColumn];
		}
		return 0;
	}

	/**
	 * - Renvoie la liste des cases de la combinaison dans l'ordre haut -> bas, gauche -> droite
	 * @param {number} line
	 * @param {number} column
	 * @param {String} direction
	 * @param {number} decalage
	 * @param {number} length
	 * @returns
	 */
	getCasesInCombinaison(line, column, direction, decalage = 0, length = 4) {
		const minLine = 0;
		const minColumn = 0;
		const maxLine = 5;
		const maxColumn = 6;
		let firstLine;
		let lastLine;
		let firstColumn;
		let lastColumn;
		switch (direction) {
			case 'H':
				// * horizontalement
				firstLine = line;
				lastLine = line;
				firstColumn = column - decalage;
				lastColumn = firstColumn + length - 1;
				if (
					minLine <= firstLine &&
					lastLine <= maxLine &&
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					firstLine <= lastLine &&
					firstColumn <= lastColumn
				) {
					let list = [];
					for (let i = 0; i < length; i++) {
						list.push([line, firstColumn + i]);
					}
					return list;
				}
				break;
			case 'V':
				// * verticalement
				firstLine = line - decalage;
				lastLine = firstLine + length - 1;
				firstColumn = column;
				lastColumn = column;
				if (
					minLine <= firstLine &&
					lastLine <= maxLine &&
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					firstLine <= lastLine &&
					firstColumn <= lastColumn
				) {
					let list = [];
					for (let i = 0; i < length; i++) {
						list.push([firstLine + i, column]);
					}
					return list;
				}
				break;
			case 'D1':
				// * première diagonale
				firstLine = line - decalage;
				lastLine = firstLine + length - 1;
				firstColumn = column - decalage;
				lastColumn = firstColumn + length - 1;
				if (
					minLine <= firstLine &&
					lastLine <= maxLine &&
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					firstLine <= lastLine &&
					firstColumn <= lastColumn
				) {
					let list = [];
					for (let i = 0; i < length; i++) {
						list.push([firstLine + i, firstColumn + i]);
					}
					return list;
				}
				break;
			case 'D2':
				// * seconde diagonale
				firstLine = line - decalage;
				lastLine = firstLine + length - 1;
				firstColumn = column + decalage - length + 1;
				lastColumn = firstColumn + length - 1;
				if (
					minLine <= firstLine &&
					lastLine <= maxLine &&
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					firstLine <= lastLine &&
					firstColumn <= lastColumn
				) {
					let list = [];
					for (let i = 0; i < length; i++) {
						list.push([firstLine + i, lastColumn - i]);
					}
					return list;
				}
				break;

			default:
				console.error(
					`Impossible de faire getCasesinCombinaison(${line}, ${column}, ${direction}, ${decalage}, ${length})`
				);
				break;
		}
		return false;
	}

	/**
	 * - Renvoie vrai si en plaçant une piece de la couleur dans la colonne provoque un alignement de 3 en diagonale où les 2 extrémités sont disponibles
	 * @param {number} column
	 * @param {String} color
	 * @return {Boolean}
	 */
	isFork(column, color) {
		const line = this.firstAvailableLine(column);
		const directions = ['H', 'D1', 'D2'];

		for (let i = 0; i < directions.length; i++) {
			let direction = directions[i];
			let cases = [];
			let colors = [];

			for (let decalage = 1; decalage <= 3; decalage++) {
				if (
					this.getCasePosition(
						line,
						column,
						direction,
						decalage,
						1,
						5
					)
				) {
					cases = [];
					cases.push(
						this.getCasePosition(
							line,
							column,
							direction,
							decalage,
							0,
							5
						)
					);
					cases.push(
						this.getCasePosition(
							line,
							column,
							direction,
							decalage,
							1,
							5
						)
					);
					cases.push(
						this.getCasePosition(
							line,
							column,
							direction,
							decalage,
							2,
							5
						)
					);
					cases.push(
						this.getCasePosition(
							line,
							column,
							direction,
							decalage,
							3,
							5
						)
					);
					cases.push(
						this.getCasePosition(
							line,
							column,
							direction,
							decalage,
							4,
							5
						)
					);

					// On note les couleurs
					colors = [];
					if (cases[0][1] === column) {
						colors.push(color);
					} else {
						colors.push(this.getColor(cases[0][0], cases[0][1]));
					}
					if (cases[1][1] === column) {
						colors.push(color);
					} else {
						colors.push(this.getColor(cases[1][0], cases[1][1]));
					}
					if (cases[2][1] === column) {
						colors.push(color);
					} else {
						colors.push(this.getColor(cases[2][0], cases[2][1]));
					}
					if (cases[3][1] === column) {
						colors.push(color);
					} else {
						colors.push(this.getColor(cases[3][0], cases[3][1]));
					}
					if (cases[4][1] === column) {
						colors.push(color);
					} else {
						colors.push(this.getColor(cases[4][0], cases[4][1]));
					}

					// Si les 3 du centre sont de la couleur et les deux extrémitées sont vides et atteignables
					// Alors on renvoie vrai
					if (
						colors[0] === '' &&
						colors[1] === color &&
						colors[2] === color &&
						colors[3] === color &&
						colors[4] === '' &&
						this.firstAvailableLine(cases[0][1]) === cases[0][0] &&
						this.firstAvailableLine(cases[4][1]) === cases[4][0]
					) {
						return true;
					}
				}
			}
		}

		// Si on n'a rien trouvé alors on renvoie faux
		return false;
	}

	/**
	 * - Renvoie la liste des couleur pouvant gagner si elle prennent la case
	 * @param {number} line
	 * @param {number} column
	 */
	positionWinner(line, column) {
		const list = [];
		const combinations = this.getCombinaisons(4, line, column);
		let i = 0;
		while (i < combinations.length && list.length < 2) {
			let combination = combinations[i];
			let c0 = combination[0];
			let c1 = combination[1];
			let c2 = combination[2];
			if (c0 === c1 && c0 === c2 && c0 !== '' && !list.includes(c0)) {
				list.push(c0);
			}
			i++;
		}
		return list;
	}

	/**
	 * - Renvoie la liste des alignements de length pièces qui passeraient par la case passée en paramètres si je la pose
	 * @param {number} length
	 * @param {number} line
	 * @param {number} column
	 */
	getCombinaisons(length, line, column) {
		if (this.positionIsValid(line, column) && 2 <= length && length <= 4) {
			let minLine = 0;
			let minColumn = 0;
			let maxLine = 5;
			let maxColumn = 6;
			const list = [];

			// * verticalement
			if (line <= maxLine - length + 1) {
				let combination = [];
				for (let i = 1; i < length; i++) {
					let color = this.getColor(line + i, column);
					combination.push(color);
				}
				list.push(combination);
			}

			// * horizontalement
			// Pour les length décalages possibles
			// Le premier : les cases à droite
			for (let l = 0; l < length; l++) {
				let firstLine = line;
				let firstColumn = column - l;
				let lastLine = line;
				let lastColumn = column - l + length - 1;
				// on vérifie que la position est valide
				if (
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					minLine <= firstLine &&
					lastLine <= maxLine
				) {
					let combination = [];
					// Pour chaque position p de la combinaison
					for (let p = -l; p < length - l; p++) {
						// Si p = 0 il s'agit de la case passée en paramètres
						if (p !== 0) {
							let color = this.getColor(line, column + p);
							combination.push(color);
						}
					}
					list.push(combination);
				}
			}

			// * première diagonale
			// Pour les length décalages possibles
			// Le premier : les cases en bas à droite
			for (let l = 0; l < length; l++) {
				let firstLine = line - l;
				let firstColumn = column - l;
				let lastLine = line - l + length - 1;
				let lastColumn = column - l + length - 1;
				// on vérifie que la position est valide
				if (
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					minLine <= firstLine &&
					lastLine <= maxLine
				) {
					let combination = [];
					// Pour chaque position p de la combinaison
					for (let p = -l; p < length - l; p++) {
						// Si p = 0 il s'agit de la case passée en paramètres
						if (p !== 0) {
							let color = this.getColor(line + p, column + p);
							combination.push(color);
						}
					}
					list.push(combination);
				}
			}

			// * seconde diagonale
			// Pour les length décalages possibles
			// Le premier : les cases en bas à gauche
			for (let l = 0; l < length; l++) {
				let firstLine = line - l;
				let firstColumn = column + l - length + 1;
				let lastLine = line - l + length - 1;
				let lastColumn = column + l;
				// on vérifie que la position est valide
				if (
					minColumn <= firstColumn &&
					lastColumn <= maxColumn &&
					minLine <= firstLine &&
					lastLine <= maxLine
				) {
					let combination = [];
					// Pour chaque position p de la combinaison
					for (let p = -l; p < length - l; p++) {
						// Si p = 0 il s'agit de la case passée en paramètres
						if (p !== 0) {
							let color = this.getColor(line + p, column - p);
							combination.push(color);
						}
					}
					list.push(combination);
				}
			}

			return list;
		}
	}

	positionIsValid(line, column) {
		if (line < 0 || 6 <= line || column < 0 || 7 <= column) {
			return false;
		}
		return true;
	}

	/**
	 * - Renvoie la couleur de la case
	 * @param {number} line
	 * @param {number} column
	 * @returns {String}
	 */
	getColor(line, column) {
		if (this.positionIsValid(line, column)) {
			return this.cases[line][column];
		}
	}

	/**
	 * - Colorie la case
	 * @param {number} line
	 * @param {number} column
	 * @param {String} color
	 */
	setColor(line, column, color) {
		if (this.positionIsValid(line, column)) {
			this.cases[line][column] = color;
		}
	}

	getColumn(column) {
		if (0 <= column && column < 7) {
			const list = [];
			for (let line = 0; line < 6; line++) {
				list.push(this.getColor(line, column));
			}
			return list;
		}
	}

	/**
	 * - Renvoie vrai si la colonne est remplie, faux sinon.
	 * @returns {Boolean}
	 */
	columnIsFull(column) {
		return this.getColor(0, column) !== '';
	}

	/**
	 * - Renvoie vrai si la grille est remplie, faux sinon.
	 * @returns {Boolean}
	 */
	isComplete() {
		for (let c = 0; c < 7; c++) {
			if (this.columnIsFull(c)) {
				return false;
			}
		}
		return true;
	}

	firstAvailableLine(column) {
		if (!this.columnIsFull(column)) {
			for (let l = 5; l >= 0; l--) {
				let color = this.getColor(l, column);
				if (color === '') {
					return l;
				}
			}
		}
	}

	/**
	 *
	 * @param {number} column
	 */
	addColor(column) {
		if (
			0 <= column &&
			column < 7 &&
			['red', 'yellow'].includes(this.color) &&
			!this.columnIsFull(column)
		) {
			this.setColor(this.firstAvailableLine(column), column, this.color);
			this.nextColor();
		}
	}

	nextColor() {
		if (this.color === 'red') {
			this.color = 'yellow';
		} else {
			this.color = 'red';
		}
	}

	getWinner() {
		// On vérifie en ligne
		for (let line = 0; line < 6; line++) {
			for (let column = 0; column < 4; column++) {
				let c0 = this.getColor(line, column);
				let c1 = this.getColor(line, column + 1);
				let c2 = this.getColor(line, column + 2);
				let c3 = this.getColor(line, column + 3);
				if (c0 != '' && c0 === c1 && c0 === c2 && c0 === c3) {
					return {
						color: c0,
						position1: { line: line, column: column },
						position2: { line: line, column: column + 1 },
						position3: { line: line, column: column + 2 },
						position4: { line: line, column: column + 3 },
					};
				}
			}
		}

		// On vérifie en colonne
		for (let line = 0; line < 3; line++) {
			for (let column = 0; column < 7; column++) {
				let c0 = this.getColor(line, column);
				let c1 = this.getColor(line + 1, column);
				let c2 = this.getColor(line + 2, column);
				let c3 = this.getColor(line + 3, column);
				if (c0 != '' && c0 === c1 && c0 === c2 && c0 === c3) {
					return {
						color: c0,
						position1: { line: line, column: column },
						position2: { line: line + 1, column: column },
						position3: { line: line + 2, column: column },
						position4: { line: line + 3, column: column },
					};
				}
			}
		}

		// On vérifie en première diagonale
		for (let line = 0; line < 3; line++) {
			for (let column = 0; column < 4; column++) {
				let c0 = this.getColor(line, column);
				let c1 = this.getColor(line + 1, column + 1);
				let c2 = this.getColor(line + 2, column + 2);
				let c3 = this.getColor(line + 3, column + 3);
				if (c0 != '' && c0 === c1 && c0 === c2 && c0 === c3) {
					return {
						color: c0,
						position1: { line: line, column: column },
						position2: { line: line + 1, column: column + 1 },
						position3: { line: line + 2, column: column + 2 },
						position4: { line: line + 3, column: column + 3 },
					};
				}
			}
		}

		// On vérifie en seconde diagonale
		for (let line = 0; line < 3; line++) {
			for (let column = 3; column < 7; column++) {
				let c0 = this.getColor(line, column);
				let c1 = this.getColor(line + 1, column - 1);
				let c2 = this.getColor(line + 2, column - 2);
				let c3 = this.getColor(line + 3, column - 3);
				if (c0 != '' && c0 === c1 && c0 === c2 && c0 === c3) {
					return {
						color: c0,
						position1: { line: line, column: column },
						position2: { line: line + 1, column: column - 1 },
						position3: { line: line + 2, column: column - 2 },
						position4: { line: line + 3, column: column - 3 },
					};
				}
			}
		}
		return 0;
	}
}

function centralColumn(column) {
	let c = +column;
	if (0 <= c && c < 7) {
		switch (c) {
			case 0:
				return 3;
			case 1:
				return 2;
			case 2:
				return 4;
			case 3:
				return 1;
			case 4:
				return 5;
			case 5:
				return 0;
			case 6:
				return 6;
		}
	}
}
