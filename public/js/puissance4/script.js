document.addEventListener('DOMContentLoaded', () => {
	// ? Les constantes
	const debug = true;
	const grid = new Grid();
	const HTMLgrid = document.getElementsByClassName('grid')[0];
	const cases = document.getElementsByClassName('case');
	const newGameButton = document.getElementsByClassName('new-game')[0];
	const changeOpponentButton = document.getElementsByClassName(
		'change-opponent'
	)[0];
	const sideBarText = document
		.getElementsByClassName('side-bar')[0]
		.getElementsByClassName('text')[0];
	const pieceColor = document.getElementsByClassName('piece')[0];
	/** @type {Boolean} - Vrai si on joue contre un bot, faux sinon. */
	let bot = false;
	let botIsPlaying = false;

	// ? Le script

	// * On initialise le plateau
	// * On pose les pièces tant qu'il n'y a pas de gagnant

	drawGrid(grid);

	for (let i = 0; i < cases.length; i++) {
		const _case = cases[i];
		_case.addEventListener('click', () => {
			// Si la partie n'est pas finie et qu'on a cliqué sur une case
			if (
				_case.classList.contains('empty') &&
				grid.getWinner() === 0 &&
				!botIsPlaying
			) {
				// On récupère l'index de la colonne
				let columnIndex = getCaseColumn(_case) - 1;

				// On ajoute la couleur
				grid.addColor(columnIndex);
				drawGrid(grid);

				// On modifie le texte à droite
				let winner = grid.getWinner();
				if (winner) {
					let finalText = document
						.getElementsByClassName('side-bar')[0]
						.getElementsByClassName('text')[0];

					// On met en valeur les cases gagnantes
					console.log(winner);
					document
						.getElementsByClassName(
							`row${winner.position1.line + 1}`
						)[0]
						.getElementsByClassName(
							`column${winner.position1.column + 1}`
						)[0]
						.getElementsByClassName(`case`)[0]
						.classList.add('winningCell');
					document
						.getElementsByClassName(
							`row${winner.position2.line + 1}`
						)[0]
						.getElementsByClassName(
							`column${winner.position2.column + 1}`
						)[0]
						.getElementsByClassName(`case`)[0]
						.classList.add('winningCell');
					document
						.getElementsByClassName(
							`row${winner.position3.line + 1}`
						)[0]
						.getElementsByClassName(
							`column${winner.position3.column + 1}`
						)[0]
						.getElementsByClassName(`case`)[0]
						.classList.add('winningCell');
					document
						.getElementsByClassName(
							`row${winner.position4.line + 1}`
						)[0]
						.getElementsByClassName(
							`column${winner.position4.column + 1}`
						)[0]
						.getElementsByClassName(`case`)[0]
						.classList.add('winningCell');

					if (bot) {
						finalText.innerHTML = `Bravo vous avez gagné !`;
					} else {
						finalText.innerHTML = `Bravo les <span class="color ${
							winner === 'yellow'
								? 'yellow">jaunes'
								: 'red">rouges'
						}</span> ont gagné !`;
					}
				} else {
					changeColor();
					if (bot) {
						botIsPlaying = true;
						setTimeout(() => {
							let choice = grid.botChoice();

							let color = document
								.getElementsByClassName('side-bar')[0]
								.getElementsByClassName('text')[0]
								.getElementsByClassName('color')[0].innerHTML;

							// debug
							if (debug) {
								console.log(
									`${color} : Ligne ${grid.firstAvailableLine(
										choice
									)} Colonne ${choice}`
								);
							}

							grid.addColor(choice);
							drawGrid(grid);
							botIsPlaying = false;
							// On modifie le texte à droite
							let winner2 = grid.getWinner();
							if (winner2) {
								let finalText = document
									.getElementsByClassName('side-bar')[0]
									.getElementsByClassName('text')[0];
								finalText.innerHTML = `Machine > toi !`;

								// On met en valeur les cases gagnantes
								document
									.getElementsByClassName(
										`row${winner2.position1.line + 1}`
									)[0]
									.getElementsByClassName(
										`column${winner2.position1.column + 1}`
									)[0]
									.getElementsByClassName(`case`)[0]
									.classList.add('winningCell');
								document
									.getElementsByClassName(
										`row${winner2.position2.line + 1}`
									)[0]
									.getElementsByClassName(
										`column${winner2.position2.column + 1}`
									)[0]
									.getElementsByClassName(`case`)[0]
									.classList.add('winningCell');
								document
									.getElementsByClassName(
										`row${winner2.position3.line + 1}`
									)[0]
									.getElementsByClassName(
										`column${winner2.position3.column + 1}`
									)[0]
									.getElementsByClassName(`case`)[0]
									.classList.add('winningCell');
								document
									.getElementsByClassName(
										`row${winner2.position4.line + 1}`
									)[0]
									.getElementsByClassName(
										`column${winner2.position4.column + 1}`
									)[0]
									.getElementsByClassName(`case`)[0]
									.classList.add('winningCell');
							} else {
								changeColor();
							}
						}, 500);
					}
				}
			}
		});
	}

	changeOpponent();

	// ! Bouton nouvelle partie
	// Réinitialise la partie quand on clique sur le bouton nouvelle partie
	newGameButton.addEventListener('click', () => {
		newGame();
		// On supprime toutes les cases gagnantes
		const winningCells = document.getElementsByClassName('winningCell');
		while (winningCells.length) {
			winningCells[0].classList.remove('winningCell');
		}
	});

	// ! Bouton changer d'adversaire
	changeOpponentButton.addEventListener('click', () => {
		changeOpponent();
		newGame();
		// On supprime toutes les cases gagnantes
		const winningCells = document.getElementsByClassName('winningCell');
		while (winningCells.length) {
			winningCells[0].classList.remove('winningCell');
		}
	});

	// ? Les fonctions

	function changeOpponent() {
		bot = !bot;
		if (bot) {
			changeOpponentButton.innerHTML = 'Jouer contre un ami';
		} else {
			changeOpponentButton.innerHTML = "Jouer contre l'ordinateur";
		}
	}

	/**
	 * - Change le texte et la couleur de la pièce indiquand qui doit jouer
	 */
	function changeColor() {
		let color = document
			.getElementsByClassName('side-bar')[0]
			.getElementsByClassName('text')[0]
			.getElementsByClassName('color')[0];
		if (color.classList.contains('red')) {
			color.classList.remove('red');
			color.classList.add('yellow');
			color.innerHTML = 'jaunes';
		} else {
			color.classList.remove('yellow');
			color.classList.add('red');
			color.innerHTML = 'rouges';
		}
		if (pieceColor.classList.contains('red')) {
			pieceColor.classList.remove('red');
			pieceColor.classList.add('yellow');
		} else {
			pieceColor.classList.remove('yellow');
			pieceColor.classList.add('red');
		}
	}

	function newGame() {
		grid.initializeGrid();
		drawGrid(grid);
		sideBarText.innerHTML =
			'Au tour des <span class="color red">rouges</span> de jouer';
		pieceColor.classList.remove('yellow');
		pieceColor.classList.add('red');
	}

	/**
	 * - Renvoie le numéro entre 1 et 7 de la colonne contenant la case
	 * @param {HTMLElement} _case
	 * @returns {number}
	 */
	function getCaseColumn(_case) {
		let classes = _case.parentNode.classList;
		for (let i = 0; i < classes.length; i++) {
			let c = classes[i];
			if (c.length === 7 && c.substring(0, 6) === 'column') {
				return c.substring(6);
			}
		}
	}

	/**
	 * - Ajoute une classe à la case pour la colorier, et la rend transparente si ce n'est ni red ni yellow
	 * @param {number} line - [[0;6]]
	 * @param {number} column - [[0;7]]
	 * @param {string} color
	 */
	function drawColor(line, column, color) {
		const _case = getColor(line, column);
		_case.classList.remove('red');
		_case.classList.remove('yellow');
		if (
			!(line < 0 || 6 <= line || column < 0 || 7 <= column) &&
			['red', 'yellow'].includes(color)
		) {
			const row = document.getElementsByClassName(`row${+line + 1}`)[0];
			_case.classList.remove('empty');
			_case.classList.add(color);
		} else {
			_case.classList.add('empty');
		}
	}

	/**
	 * - On dessine la grille
	 * @param {Grid} grid
	 */
	function drawGrid(grid) {
		for (let line = 0; line < 6; line++) {
			for (let column = 0; column < 7; column++) {
				let color = grid.getColor(line, column);
				drawColor(line, column, color);
			}
		}
	}

	/**
	 * - Renvoie la case
	 * @param {number} line - [[0;6]]
	 * @param {number} column - [[0;7]]
	 * @returns {HTMLElement}
	 */
	function getColor(line, column) {
		if (!(line < 0 || 6 <= line || column < 0 || 7 <= column)) {
			return document
				.getElementsByClassName(`row${+line + 1}`)[0]
				.getElementsByClassName(`column${+column + 1}`)[0]
				.getElementsByClassName('case')[0];
		}
	}
});
