/**
 * - Renvoie le tableau où l'on a supprimé l'élément passé par index
 * @param {Array} array
 * @param {number} index
 * @returns {Array}
 */
function removeElementByIndex(array, index) {
	let res = [];
	index = parseInt(index);
	if (0 <= index && index < array.length) {
		if (index > 0) {
			res = array.slice(0, index);
		}
		if (index < array.length - 1) {
			res = res.concat(array.slice(index + 1));
		}
	}
	return res;
}
