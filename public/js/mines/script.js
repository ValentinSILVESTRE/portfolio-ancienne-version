document.addEventListener('DOMContentLoaded', () => {
	/** - La difficulté : easy, medium ou hard */
	const difficulty = getCookie('difficulty');
	const cellList = document.getElementsByClassName('cell');
	/** @type {Boolean} - Vaut vrai tant qu'on n'a pas révélé de case */
	let firstClick = true;
	/** @type {HTMLDivElement} - La grille */
	const grid = document.getElementById('grid');

	/** @type {Number} - Le nombre de lignes de la grille */
	const nbRow = grid.getAttribute('height');
	/** @type {Number} - Le nombre de colonnes de la grille */
	const nbColumn = grid.getAttribute('width');
	/** @type {Number} - Le nombre de mines */
	const nbMines = grid.getAttribute('mines');

	/** @type {Number} - Nombre de mines n'ayant pas de flag */
	let nbMinesToDetect = nbMines;

	// On construit la partie
	/** @type {Game} */
	let game = new Game('custom', nbRow, nbColumn, nbMines);

	// ? - On empêche de scroll et d'ouvrir le contextmenu en cliquant sur la grille
	grid.addEventListener('contextmenu', (e) => {
		e.preventDefault();
	});
	grid.addEventListener('mousedown', (e) => {
		if (e.button === 1) {
			e.preventDefault();
		}
	});

	// On affiche la grille
	displayGrid();

	// On écrit les nombres de mines
	displayMinesToDetext();

	for (let i = 0; i < cellList.length; i++) {
		const td = cellList[i];
		if (td.getElementsByClassName('mine-picture')[0]) {
			td.getElementsByClassName('mine-picture')[0].style.display = 'none';
		}
	}

	// On affiche le nombre de mines à trouver
	// displayMinesToDetext();

	// Pour chaque cellule
	for (let i = 0; i < cellList.length; i++) {
		const td = cellList[i];
		const tdRow = Math.floor(i / nbColumn);
		const tdColumn = i % nbColumn;

		// On réagit aux cliques gauche
		td.addEventListener('click', () => {
			// Initialisation
			// Si c'est le premier clique, alors on construit la grille tel que la cellule dévoilée soit vide
			while (
				firstClick &&
				(getCase(tdRow, tdColumn).isMine ||
					game.grid.nbNeighbours(tdRow, tdColumn) > 0)
			) {
				game = new Game('custom', nbRow, nbColumn, nbMines);
			}
			firstClick = false;

			// On affiche la grille
			displayGrid();
			// On affiche les couleurs
			displayColors();

			// Si la partie n'est pas terminée et que la case n'a pas de flag
			if (!game.win && !game.loose && !getCase(tdRow, tdColumn).flag) {
				// On révelle la case ou le bloc des cases vides et les bordures si la case est vide
				td.classList.remove('no-reveal');
				game.reveal(tdRow, tdColumn);

				for (let i = 0; i < cellList.length; i++) {
					const _tdRow = Math.floor(i / nbColumn);
					const _tdColumn = i % nbColumn;
					if (game.loose) {
						getCase(_tdRow, _tdColumn).reveal();
					}
				}

				// On active la mine si s'en est une, et on perd
				if (td.classList.contains('mine')) {
					td.getElementsByClassName('mine-picture')[0].classList.add(
						'mine-active'
					);
				}

				// On écrit les nombres de mines
				displayNbMines();

				// On affiche la grille
				displayGrid();

				// On traite la fin de partie
				if (game.isOver()) {
					endGame();

					if (game.win) {
						document.getElementById('bottom-text').innerText =
							'Gagné !';

						// On joue le son de victoire
						playSound('../sounds/victory.mp4');
					} else {
						document.getElementById('bottom-text').innerText =
							'Perdu ...';

						// On joue le son de l'explosion
						playSound('../sounds/explosion.mp4');
					}
				}
			}
		});

		// On réagit aux cliques gauche
		td.addEventListener('auxclick', (e) => {
			e.preventDefault();

			// Si on n'a pas encore révélé de case alors on ne fait rien
			if (!firstClick) {
				// On vérifie que la case n'est pas déjà révelée et que la partie n'est pas finie
				if (
					(e.button === 1 || e.button === 2) &&
					!getCase(tdRow, tdColumn).isReveal &&
					!game.isOver()
				) {
					// On affiche le nombre de mines restantes
					if (getCase(tdRow, tdColumn).flag) {
						nbMinesToDetect++;
					} else {
						nbMinesToDetect--;
					}
					displayMinesToDetext();

					// On inverse le flag de la case
					getCase(tdRow, tdColumn).flag = !getCase(tdRow, tdColumn)
						.flag;

					// On actualise l'affichage
					displayGrid();
				}
			}
		});
	}

	// ? =================================================================================
	// ? *                                                                               *
	// ? *                                  FONCTIONS                                    *
	// ? *                                                                               *
	// ? =================================================================================

	/**
	 * - Renvoie la cellule demandée en paramètre
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {HTMLTableCellElement}
	 */
	function getTd(row, column) {
		if (0 <= +row && +row < nbRow && 0 <= +column && +column < nbColumn) {
			return cellList[+row * nbColumn + +column];
		}
		console.error(`Impossible de faire getTd(${row}, ${column})`);
	}

	/**
	 * - Écrit dans la cellule le nombre de mines adjacentes
	 * @param {Number} tdRow
	 * @param {Number} tdColumn
	 */
	function displayNearMines(tdRow, tdColumn) {
		const td = getTd(tdRow, tdColumn);

		// On écrit si la case est révelée, n'est pas une mine et a des voisins
		if (
			getCase(tdRow, tdColumn).isReveal &&
			!getCase(tdRow, tdColumn).isMine &&
			game.grid.nbNeighbours(tdRow, tdColumn) > 0
		) {
			if (td.getElementsByTagName('p')[0]) {
				td.getElementsByTagName(
					'p'
				)[0].innerText = game.grid.nbNeighbours(tdRow, tdColumn);
			} else {
				const p = document.createElement('p');
				p.innerText = game.grid.nbNeighbours(tdRow, tdColumn);
				td.appendChild(p);
			}
		}
	}

	/**
	 * - Fonction qui affiche une cellule comme flag ou non en fonction de la Case correspondante
	 * @param {Number} row
	 * @param {Number} column
	 */
	function flagTD(row, column) {
		const td = getTd(row, column);
		const _case = getCase(row, column);
		// On ajoute / enlève la classe flag à la cellule
		td.classList.remove('flag');
		if (_case.flag) {
			td.classList.add('flag');
		}
	}

	/**
	 * - Retourne la case demandée
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {Case}
	 */
	function getCase(row, column) {
		return game.grid.getCase(row, column);
	}

	/**
	 * - Écrit le contenue du tbody de l'élément dont l'id est grid
	 * @param {Game} game
	 */
	function displayGrid() {
		let htmlGrid = document.getElementById('grid');

		// Pour chaque case / td
		for (let iRow = 0; iRow < game.grid.nbRow; iRow++) {
			for (let iColumn = 0; iColumn < game.grid.nbColumn; iColumn++) {
				const _case = getCase(iRow, iColumn);
				const cell = htmlGrid
					.getElementsByClassName('line')
					[iRow].getElementsByClassName('cell')[iColumn];
				cell.classList.remove('mine');

				// On ajoute la classe mine aux cell ainsi que les dessins de mines
				if (_case.isMine) {
					cell.classList.add('mine');
					if (!cell.getElementsByClassName('mine-picture')[0]) {
						addMine(cell);
					}
					if (cell.classList.contains('no-reveal')) {
						cell.getElementsByClassName(
							'mine-picture'
						)[0].style.display = 'none';
					} else {
						cell.getElementsByClassName(
							'mine-picture'
						)[0].style.display = 'flex';
					}
				}
				// S'il y a une mine qui ne devrait pas y être
				else if (cell.getElementsByClassName('mine-picture')[0]) {
					cell.removeChild(
						cell.getElementsByClassName('mine-picture')[0]
					);
				}

				// On revèlle la cellule
				cell.classList.remove('no-reveal');

				// Si la case n'est pas révelée
				if (!_case.isReveal && !game.isOver()) {
					// On masque la cellule
					cell.classList.add('no-reveal');
				}

				// On traite le flag
				flagTD(iRow, iColumn);

				// Si la partie est finie, on retire les flags
				if (game.loose) {
					cell.classList.remove('flag');
				}
			}
		}
	}

	function displayMinesToDetext() {
		const h2 = document.getElementById('bottom-text');
		h2.innerText = nbMinesToDetect;
		addMine(h2);
	}

	/**
	 * - Affichage quand la partie est gagnée ou perdue
	 */
	function endGame() {
		let htmlGrid = document.getElementById('grid');

		for (let r = 0; r < nbRow; r++) {
			for (let c = 0; c < nbColumn; c++) {
				const _case = getCase(r, c);
				const td = htmlGrid
					.getElementsByClassName('line')
					[r].getElementsByClassName('cell')[c];
				td.classList.remove('flag');
				_case.flag = false;
			}
		}

		displayGrid();

		// Pour chaque case
		for (let r = 0; r < nbRow; r++) {
			for (let c = 0; c < nbColumn; c++) {
				const _case = getCase(r, c);
				const td = htmlGrid
					.getElementsByClassName('line')
					[r].getElementsByClassName('cell')[c];
				if (td.classList.contains('mine')) {
					if (game.win) {
						td.classList.add('win');
					} else {
						td.classList.add('loose');
					}
				}
			}
		}
	}

	function displayColors() {
		let htmlGrid = document.getElementById('grid');
		for (let r = 0; r < nbRow; r++) {
			for (let c = 0; c < nbColumn; c++) {
				const _case = getCase(r, c);
				const td = htmlGrid
					.getElementsByClassName('line')
					[r].getElementsByClassName('cell')[c];
				if (game.grid.nbNeighbours(r, c)) {
					td.classList.add(`m${game.grid.nbNeighbours(r, c)}`);
				}
			}
		}
	}

	function addMine(element) {
		const mine = document.createElement('div');
		element.appendChild(mine);
		mine.classList.add('mine-picture');
		const top = document.createElement('div');
		mine.appendChild(top);
		top.classList.add('mine-top');
		const base = document.createElement('div');
		mine.appendChild(base);
		base.classList.add('mine-base');
	}

	function addMine_old(td) {
		const mine = document.createElement('div');
		mine.classList.add('mine-picture');
		for (let iLine = 1; iLine <= 4; iLine++) {
			const line = document.createElement('div');
			line.classList.add('mine-line');
			line.classList.add(`mine-line${iLine}`);
			if (iLine === 1 || iLine === 4) {
				for (let iColumn = 1; iColumn <= 4; iColumn++) {
					const column = document.createElement('div');
					column.classList.add('mine-column');
					column.classList.add(`mine-column${iColumn}`);
					line.appendChild(column);
				}
			} else {
				for (let iColumn = 1; iColumn <= 3; iColumn++) {
					const column = document.createElement('div');
					column.classList.add('mine-column');
					column.classList.add(`mine-column${iColumn}`);
					if (iColumn === 2) {
						if (iLine === 2) {
							const mineTop = document.createElement('div');
							mineTop.classList.add('mine-top');
							column.appendChild(mineTop);
						}
						if (iLine === 3) {
							const mineBottom = document.createElement('div');
							mineBottom.classList.add('mine-bottom');
							column.appendChild(mineBottom);
						}
					}
					line.appendChild(column);
				}
			}
			mine.appendChild(line);
		}
		for (let i = 0; i < td.children.length; i++) {
			td.removeChild(td.children[i]);
		}
		td.appendChild(mine);
	}

	function displayMinesPictures() {
		for (let r = 0; r < nbRow; r++) {
			for (let c = 0; c < nbColumn; c++) {
				const td = getTd(r, c);
				if (td.classList.contains('mine')) {
					addMine(td);
				}
			}
		}
	}

	function displayNbMines() {
		for (let r = 0; r < nbRow; r++) {
			for (let c = 0; c < nbColumn; c++) {
				displayNearMines(r, c);
			}
		}
	}
});
