/**
 * - La classe Game a une difficulté : easy (8*8 : 10 mines), medium (16*16 : 40 mines) ou hard (16 x 30 : 99 mines)
 */
class Game {
	/**
	 * @param {String} difficulty
	 */
	constructor(difficulty, nbRow = 0, nbColumn = 0, nbMines = 0) {
		/** @type {Boolean} */
		this.loose = false;
		/** @type {Boolean} */
		this.win = false;
		if (['easy', 'medium', 'hard', 'custom'].includes(difficulty)) {
			/** @type {String} - La difficulté de la partie : easy, medium ou hard */
			this.difficulty = difficulty;

			// On construit la grille

			if (difficulty === 'custom') {
				/** @type {Grid} - La grille */
				this.grid = new Grid(difficulty, nbRow, nbColumn, nbMines);
			} else {
				/** @type {Grid} - La grille */
				this.grid = new Grid(difficulty);
			}
		} else {
			console.error(
				`Impossible de créér une Game de difficulté ${difficulty}`
			);
		}
	}

	getGrid() {
		return this.grid.getGrid();
	}

	/**
	 * - Révelle la case ainsi que celles qui peuvent l'être
	 * @param {Number} row
	 * @param {Number} column
	 */
	reveal(row, column) {
		// On vérifie que les données sont valides
		if (
			0 <= row &&
			row < this.grid.nbRow &&
			0 <= column &&
			column < this.grid.nbColumn
		) {
			// On revelle la case
			this.grid.reveal(row, column);

			const _case = this.grid.getCase(row, column);

			// On supprime le flag
			_case.setFlag(false);

			// On traite les fins de parties
			if (_case.isMine) {
				this.loose = true;
			} else {
				this.win = this.grid.isWin();
			}
		} else {
			console.error(`Impossible de faire Game.reveal(${row}, ${column})`);
		}
	}

	/**
	 * - Renvoie vrai si la partie est finie, faux sinon.
	 * @returns {Boolean}
	 */
	isOver() {
		return this.win || this.loose;
	}
}
