/**
 * @typedef Case
 */

/**
 * - Une case a la propriété isMine (Booléen) qui vaut vrai si c'est une mine
 */
class Case {
	/**
	 * @param {Boolean} isMine - vrai si la case est une mine, faux sinon
	 */
	constructor(isMine, isReveal = false, flag = false) {
		/** @type {Boolean} - vrai si la case est une mine, faux sinon */
		this.isMine = isMine;
		this.isReveal = isReveal;
		this.flag = flag;
	}

	/**
	 * - Révelle la case
	 */
	reveal() {
		this.isReveal = true;
	}

	/**
	 * - Définit le flag de la case
	 * @param {Boolean} bool
	 */
	setFlag(bool) {
		this.flag = bool;
	}
}
