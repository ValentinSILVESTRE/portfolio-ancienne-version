/**
 * - La grille est un rectangle de nbRow lignes fois nbColumn colonnes cases
 */
class Grid {
	/**
	 * @param {String} difficulty - la difficulté de la grille : easy, medium ou hard
	 */
	constructor(difficulty, nbRow = 0, nbColumn = 0, nbMines = 0) {
		/** @type {Number} - Le nombre de lignes de la grille */
		this.nbRow;
		/** @type {Number} - Le nombre de colonnes de la grille */
		this.nbColumn;
		/** @type {Number} - Le nombre de mines */
		this.nbMines;

		if (difficulty === 'easy') {
			this.nbRow = 8;
			this.nbColumn = 8;
			this.nbMines = 10;
		} else if (difficulty === 'medium') {
			this.nbRow = 16;
			this.nbColumn = 16;
			this.nbMines = 40;
		} else if (difficulty === 'hard') {
			this.nbRow = 16;
			this.nbColumn = 30;
			this.nbMines = 99;
		} else {
			this.nbRow = nbRow;
			this.nbColumn = nbColumn;
			this.nbMines = nbMines;
		}

		/** @type {[Number]} - Les positions des mines */
		const positionsMines = parmis(
			this.nbMines,
			this.nbRow * this.nbColumn - 1
		);

		// On construit la grille
		this.grid = [];
		for (let iRow = 0; iRow < this.nbRow; iRow++) {
			const row = [];
			for (let iColumn = 0; iColumn < this.nbColumn; iColumn++) {
				// Pour chaque case où doit se trouver une mine, on met une mine
				const indice = iRow * this.nbColumn + iColumn;
				if (positionsMines.includes(indice)) {
					row.push(new Case(true));
				} else {
					row.push(new Case(false));
				}
			}
			this.grid.push(row);
		}
	}

	/**
	 * @returns {[Case]}
	 */
	getGrid() {
		return this.grid;
	}

	/**
	 * - Renvoie la case correspondante
	 * @param {Number} iRow
	 * @param {Number} iColumn
	 * @returns {Case?}
	 */
	getCase(iRow, iColumn) {
		const r = +iRow;
		const c = +iColumn;
		if (0 <= r && r < this.nbRow && 0 <= c && c < this.nbColumn) {
			return this.getGrid()[r][c];
		}
		console.error(`Impossible de faire getCase(${iRow}, ${iColumn})`);
	}

	/**
	 * - Renvoie le nombre de mines adjacentes à la case
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {Number}
	 */
	nbNeighbours(row, column) {
		let nb = 0;
		if (
			0 <= row &&
			row < this.nbRow &&
			0 <= column &&
			column < this.nbColumn
		) {
			for (let r = row - 1; r < row + 2; r++) {
				for (let c = column - 1; c < column + 2; c++) {
					if (
						0 <= r &&
						r < this.nbRow &&
						0 <= c &&
						c < this.nbColumn &&
						(r !== row || c !== column) &&
						this.getCase(r, c).isMine
					) {
						nb++;
					}
				}
			}
			return nb;
		} else {
			console.error(
				`Impossible de faire nbNeighbours(${row}, ${column})`
			);
		}
	}

	reveal(row, column) {
		// On vérifie que les données sont valides, et que la case n'est pas déjà révellée
		if (
			0 <= row &&
			row < this.nbRow &&
			0 <= column &&
			column < this.nbColumn
		) {
			// Si la case n'est pas déjà révellée
			if (!this.getCase(row, column).isReveal) {
				const _case = this.getCase(row, column);
				// Si ce n'est pas une mine, on revelle les cases adjacentes aux blocs de vide qui la touche
				if (!_case.isMine) {
					_case.reveal();

					// On supprime son flag
					_case.setFlag(false);

					// On affiche les cases devant l'être

					// Si la case est vide alors on appelle reveal à toutes les cases adjacentes
					if (!this.nbNeighbours(row, column)) {
						for (let r = row - 1; r < row + 2; r++) {
							for (let c = column - 1; c < column + 2; c++) {
								this.reveal(r, c);
							}
						}
					}
				} else {
					// On revèlle toutes les cases
					for (let r = 0; r < this.nbRow; r++) {
						for (let c = 0; c < this.nbColumn; c++) {
							this.getCase(r, c).reveal();
						}
					}
				}
			}
		}
	}

	propagation(row, column) {
		// On vérifie que les données sont valides
		if (
			0 <= row &&
			row < this.nbRow &&
			0 <= column &&
			column < this.nbColumn
		) {
			// Pour chacune des 8 cases adjacentes
			// Si la case est vide ou si isEmpty est vrai, alors on la révelle
			if (this.getCase(row, column).nbNeighbours === 0) {
				this.getCase(row, column).reveal = true;
			}
		} else {
			console.error(`Impossible de faire propagation(${row}, ${column})`);
		}
	}

	/**
	 * - Renvoie vrai si la partie est gagnée, faux sinon
	 * @returns {Boolean}
	 */
	isWin() {
		// Pour chaque case
		for (let r = 0; r < this.nbRow; r++) {
			for (let c = 0; c < this.nbColumn; c++) {
				const _case = this.getCase(r, c);
				// Si une case n'est pas révellée et n'est pas une mine, alors la partie n'est pas gagnée
				if (!_case.isMine && !_case.isReveal) {
					return false;
				}
			}
		}
		return true;
	}
}

/**
 * - Renvoie un tableau de nbIndices indices différents et aléatoires dans [[0;indiceMax]] avec 0 < nbIndices <= indiceMax
 * @param {Number} nbIndices - Le nombre d'indices à choisir
 * @param {Number} indiceMax - L'indice maximum
 */
function parmis(nbIndices, indiceMax) {
	nbIndices = +nbIndices;
	indiceMax = +indiceMax;
	if (nbIndices <= 0 || indiceMax <= 0 || nbIndices > indiceMax) {
		console.error(`Impossible de faire parmis(${nbIndices}, ${indiceMax})`);
	} else {
		// On construit la liste des entiers compris entre 0 et indiceMax
		const intList = [];
		for (let i = 0; i <= indiceMax; i++) {
			intList.push(i);
		}

		// On récupère nbIndices indices de cette liste
		const indicesList = [];
		while (indicesList.length < nbIndices) {
			const indice = Math.floor(Math.random() * (indiceMax + 1));
			// S'il n'a pas déjà été pris alors on le prends
			if (!indicesList.includes(indice)) {
				indicesList.push(indice);
			}
		}
		return indicesList;
	}
}
