document.addEventListener('DOMContentLoaded', () => {
	/** @type {HTMLInputElement} - L'input height */
	const heightInput = document.getElementById('heightInput');
	/** @type {HTMLInputElement} - L'input width */
	const widthInput = document.getElementById('widthInput');
	/** @type {HTMLInputElement} - L'input mines */
	const minesInput = document.getElementById('minesInput');

	minesInput.setAttribute('max', heightInput.value * widthInput.value - 10);
	heightInput.addEventListener('change', () => {
		minesInput.setAttribute(
			'max',
			heightInput.value * widthInput.value - 10
		);
	});
	widthInput.addEventListener('change', () => {
		minesInput.setAttribute(
			'max',
			heightInput.value * widthInput.value - 10
		);
	});
});
