/**
 * ! - À mettre dans la page de recherche
 * - N'affiche que les événements qui correspondent au filtre de recherche
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	/** @type{HTMLDivElement} - La div d'id table */
	const table = document.getElementById('table');
	/** @type{HTMLDivElement} - Les lignes de la table (sauf celle des titres) */
	const rows = table
		.getElementsByClassName('table-content')[0]
		.getElementsByClassName('row');
	/** @type{string} - La recherche */
	const search_query = window.location.href.split('results?search_query=')[1];
	/** @type{HTMLInputElement} - L'input de recherche */
	const filter_search = document.getElementById('filter-search');

	// On affiche la recherche dans l'input de recherche
	if (search_query) {
		filter_search.value = search_query;
	}

	displayFilteredRows(search_query, rows);
});

/**
 * - Renvoie vrai si la ligne contient la valeur recherchée dans la colonne categories ou title
 * @param {string} search_query - La recherche
 * @param {HTMLElement} row - La ligne (div de class row)
 */
function rowContain(search_query, row) {
	const categories = row
		.getElementsByClassName('category')[0]
		.innerText.toUpperCase();
	const title = row
		.getElementsByClassName('title')[0]
		.innerText.toUpperCase();

	return (
		categories.includes(search_query.toUpperCase()) ||
		title.includes(search_query.toUpperCase())
	);
}

/**
 * - Met un display none aux ligne qui ne contiennent pas la recherche
 * @param {string} search_query - La recherche
 * @param {Array} rows - Liste des lignes de la table (sauf celle des titres)
 */
function displayFilteredRows(search_query, rows) {
	for (let i = 0; i < rows.length; i++) {
		const row = rows[i];

		// On affiche la ligne
		row.style.display = 'flex';

		// On masque le ligne si elle ne correspond pas
		if (!rowContain(search_query, row)) {
			row.style.display = 'none';
		}
	}
}
