/**
 * ! - À mettre dans le header
 * - Affiche la date dans le header, actualisée toutes les secondes
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	printCurrentDateTime();
	setInterval(() => {
		printCurrentDateTime();
	}, 1000);
});
