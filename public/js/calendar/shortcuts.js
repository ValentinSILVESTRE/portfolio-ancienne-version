/**
 * ! - À mettre dans toutes les pages du calendrier
 * - Permet d'utiliser les raccourcis (ctrl+enter => newEvent)
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	// Si on appuie sur ctrl+enter alors on redirige à newEvent.php
	document.addEventListener('keypress', (event) => {
		if (event.ctrlKey && event.key === 'Enter') {
			location = '/event/new';
		}
	});
});
