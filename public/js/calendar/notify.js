/**
 * ! - À mettre dans tout ce qui concerne le calendrier
 * - Créé une notification et joue un son quand un événement commence
 * @requires functions.js
 */

// On fait une alerte sonore quand un événement commence
document.addEventListener('DOMContentLoaded', () => {
	// A chaque seconde
	setInterval(() => {
		const rowList = document
			.getElementById('eventList')
			.getElementsByClassName('table-content')[0]
			.getElementsByClassName('row');

		/** {Date} - Date de l'instant présent */
		const now = new Date();

		// Pour chaque événement (ligne)
		for (let i = 0; i < rowList.length; i++) {
			const row = rowList[i];
			const eventTitle = row
				.getElementsByClassName('title')[0]
				.innerText.replaceAll('\t', '')
				.replaceAll('\n', '');

			// On joue un son si l'événement commence
			if (
				isNow(
					row
						.getElementsByTagName('time')[0]
						.getAttribute('datetime'),
					now
				)
			) {
				playSound('sounds/non.mp4');
				notify(eventTitle);
			}
		}
	}, 1000);
});
