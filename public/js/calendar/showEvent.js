/**
 * ! - À mettre dans showEvent
 * - Affiche la date relative
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	// On affiche la date relative actualisée toutes les secondes
	printRelativeDate(document.getElementsByClassName('relative-date')[0]);
});
