/**
 * ! - À mettre dans la création des nouveaux événements
 * - initialise la date des nouveaux événements à celle actuelle
 */

document.addEventListener('DOMContentLoaded', () => {
	const deleteButtons = document.getElementsByClassName('delete');
	const eventLinks = document.getElementById('event-links');

	for (const deleteButton of deleteButtons) {
		deleteButton.addEventListener('click', () => {
			// S'il n'y a plus de link alors on masque la liste
			let nbLink = document.getElementsByClassName('event-link').length;
			if (nbLink === 0) {
				eventLinks.style.display = 'none';
			}
		});
	}

	// On initialise la date à maintenant
	let now = new Date();
	now.setSeconds(0);
	now.setUTCMilliseconds(0);
	now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
	document.getElementById('event_date').valueAsDate = now;
});

/**
 * - Ajoute un formulaire de lien à l'élément container
 * @param {HTMLElement} container - L'élément auquel on ajoute le nouveau lien
 */
function addLink(container) {
	const link = document.createElement('div');
	container.appendChild(link);
	link.classList.add('link');

	const url = document.createElement('div');
	link.appendChild(url);
	url.classList.add('field', 'link-url');

	const urlLabel = document.createElement('label');
	url.appendChild(urlLabel);
	urlLabel.setAttribute('for', 'link-url');
	urlLabel.innerText = 'Url';

	const urlInput = document.createElement('input');
	url.appendChild(urlInput);
	urlInput.type = 'text';
	urlInput.name = 'link-url';
	urlInput.minLength = 2;
	urlInput.maxLength = 100;

	const title = document.createElement('div');
	link.appendChild(title);
	title.classList.add('field', 'link-title');

	const titleLabel = document.createElement('label');
	title.appendChild(titleLabel);
	titleLabel.setAttribute('for', 'link-title');
	titleLabel.innerText = 'Title';

	const titleInput = document.createElement('input');
	title.appendChild(titleInput);
	titleInput.type = 'text';
	titleInput.name = 'link-title';
	titleInput.minLength = 2;
	titleInput.maxLength = 100;

	const type = document.createElement('div');
	link.appendChild(type);
	type.classList.add('field', 'link-type');
}
