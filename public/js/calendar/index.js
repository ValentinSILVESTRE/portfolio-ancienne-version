/**
 * ! - À mettre dans l'index du calendrier et la recherche, voir dans l'historique
 * - Affiche les dates absolues et relatives, plus la couleur, des événements de la table
 * - Gere la bouton to top
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	const table = document.getElementById('table');
	const relativesTimes = table
		.getElementsByClassName('table-content')[0]
		.getElementsByClassName('datetime-relative');
	const absolutesTimes = table
		.getElementsByClassName('table-content')[0]
		.getElementsByClassName('datetime-absolute');

	/** @type{HTMLDivElement} - La div qui ramène en haut */
	const toTopDiv = document.getElementById('to-top');

	scrollFunction();

	// ? - Affichage des dates absolues
	for (let i = 0; i < absolutesTimes.length; i++) {
		const absolutesTime = absolutesTimes[i];
		printAbsoluteDateTime(absolutesTime);
	}

	// ? - Gestion des dates relatives

	// On affiche la date relative dans tous les éléments correspondants, actualisé chaque secondes
	for (let i = 0; i < relativesTimes.length; i++) {
		const timeElement = relativesTimes[i];
		printRelativeDate(timeElement);
	}

	// ? - Gestion des couleurs et notifications

	// A chaque seconde
	setInterval(() => {
		const rowList = table
			.getElementsByClassName('table-content')[0]
			.getElementsByClassName('row');

		/** {Date} - Date de l'instant présent */
		const now = new Date();

		// Pour chaque événement (ligne)
		for (let i = 0; i < rowList.length; i++) {
			const row = rowList[i];

			// On met la couleur correspondant
			setColorsClasses(row, now);
		}
	}, 1000);

	window.onscroll = function () {
		scrollFunction();
	};

	toTopDiv.addEventListener('click', () => {
		topFunction();
	});

	function scrollFunction() {
		if (
			document.body.scrollTop > 20 ||
			document.documentElement.scrollTop > 20
		) {
			toTopDiv.style.display = 'flex';
		} else {
			toTopDiv.style.display = 'none';
		}
	}

	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
});
