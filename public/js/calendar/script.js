document.addEventListener('DOMContentLoaded', () => {
	// ? - Filtre
	/** @type{HTMLButtonElement} - Le bouton de recherche */
	const filterButton = document.getElementById('filter-button');
	/** @type{HTMLInputElement} - L'input de recherche */
	const filterSearch = document.getElementById('filter-search');

	if (isEmpty(filterSearch.value)) {
		filterButton.classList.add('disabled');
	}

	filterButton.addEventListener('click', () => {
		filterButton.href =
			filterButton.href.split('/results?search_query=')[0] +
			'/results?search_query=' +
			filterSearch.value;
	});
	filterSearch.addEventListener('keyup', (event) => {
		filterButton.href =
			filterButton.href.split('/results?search_query=')[0] +
			'/results?search_query=' +
			filterSearch.value;
		if (event.key === 'Enter') {
			window.location = '/results?search_query=' + filterSearch.value;
		}
		// displayFilteredRows();
	});
});
