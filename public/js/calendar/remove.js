/**
 * ! - À mettre dans les formulaires d'événements
 * - Permet de supprimer des liens
 * @requires functions.js
 */

document.addEventListener('DOMContentLoaded', () => {
	const addFormToCollection = (e) => {
		const collectionHolder = document.querySelector(
			'.' + e.currentTarget.dataset.collectionHolderClass
		);

		const item = document.createElement('li');

		item.innerHTML = collectionHolder.dataset.prototype.replace(
			/__name__/g,
			collectionHolder.dataset.index
		);

		collectionHolder.appendChild(item);

		collectionHolder.dataset.index++;
	};

	const addLinkFormDeleteLink = (linkFormLi) => {
		const removeFormButton = document.createElement('button');
		removeFormButton.classList.add('delete');
		removeFormButton.innerText = 'Delete';
		removeFormButton.type = 'button';

		linkFormLi.appendChild(removeFormButton);

		removeFormButton.addEventListener('click', (e) => {
			console.log(removeFormButton);
			e.preventDefault();
			// remove the li for the link form
			linkFormLi.remove();
		});
	};

	const links = document.querySelectorAll('li.event-link');
	links.forEach((link) => {
		addLinkFormDeleteLink(link);
	});

	document
		.querySelectorAll('.add_item_link')
		.forEach((btn) => btn.addEventListener('click', addFormToCollection));
});
