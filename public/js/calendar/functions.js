/**
 * - Affiche la date actuelle dans l'élément passé en paramètres
 * @param {HTMLElement} htmlElement - Élément html dans lequel on affiche la date
 */
function printDateTime(htmlElement) {
	htmlElement.innerText = Date();
}

/**
 * - Renvoie vrai si la chaîne de caractères n'est composée que de vide ou d'espaces, faux sinon.
 * @param {string} s - Chaîne de caractères
 * @returns {boolean}
 */
function isEmpty(s) {
	return s.trim().length === 0;
}

/**
 * - Affiche la date actuelle dans le header
 * @param {HTMLElement} - Le header
 */
function printCurrentDateTime() {
	const currentDateTimeDiv = document.getElementById('currentDateTime');
	const timeDiv = currentDateTimeDiv.getElementsByClassName('time')[0];
	const dateDiv = currentDateTimeDiv.getElementsByClassName('date')[0];

	/** @type {Date} - Date actuelle */
	const currentDate = new Date();
	const time = getTime(currentDate);
	const date = getDate(currentDate);

	timeDiv.innerText = time;
	dateDiv.innerText = date;
}

/**
 * - Renvoie vrai si l'événement se déroule à l'instant présent (à la seconde près)
 * @param {String} datetime - Date de l'événement
 * @param {Date} now - Date de l'instant présent (optionnel)
 * @returns {Boolean}
 */
function isNow(datetime, now = new Date()) {
	return (
		Math.floor(now.getTime() / 1000) ===
		Math.floor(new Date(datetime).getTime() / 1000)
	);
}

/**
 * - Renvoie la date au format local
 * @param {Date} date
 * @returns {String} - USA : Monday, November 5, 2021
 */
function getDate(date, options = []) {
	if (options.includes('short')) {
		return new Intl.DateTimeFormat(undefined, {
			weekday: 'short',
			day: 'numeric',
			month: 'short',
		}).format(date);
	}
	return new Intl.DateTimeFormat(undefined, {
		weekday: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric',
	}).format(date);
}

/**
 * - Renvoie l'heure au fomat local
 * @param {Date} date
 * @returns {String} - USA : 5:45:12 PM
 */
function getTime(date, options = []) {
	if (options.includes('short')) {
		return date.toLocaleTimeString([], {
			hour: 'numeric',
			minute: '2-digit',
		});
	}
	return date.toLocaleTimeString();
}

/**
 * - Écrit la date et l'heure absolue contenue dans l'élément time
 * @param {HTMLTimeElement} timeElement - Élément contenant l'attibut datetime avec la date absolue au format 2021-12-25 00:00:00 ainsi que deux p avec les classe date et time
 */
function printAbsoluteDateTime(timeElement) {
	// On vérifie que l'attribut datetime est présent
	if (timeElement.hasAttribute('datetime')) {
		const datetime = timeElement.getAttribute('datetime');
		const dateP = timeElement.getElementsByClassName('date')[0];
		const timeP = timeElement.getElementsByClassName('time')[0];
		const date = new Date(datetime);

		// On écrit la date chaque secondes
		dateP.innerText = getDate(date, ['short']);
		timeP.innerText = getTime(date, ['short']);
	} else {
		console.error(
			`Impossible de faire printRelativeDate(${timeElement}) car l'élément time n'a pas l'attribut datetime.`
		);
	}
}

/**
 * - Écrit la date relative contenue dans l'élément time
 * @param {HTMLTimeElement} timeElement - Élément contenant l'attibut datetime avec la date absolue au format 2021-12-25 00:00:00
 */
function printRelativeDate(timeElement) {
	// On vérifie que l'attribut datetime est présent
	if (timeElement.hasAttribute('datetime')) {
		const datetime = new Date(timeElement.getAttribute('datetime'));

		// On écrit la date chaque secondes
		timeElement.innerText = getRelativeDate(datetime, new Date());
		setInterval(() => {
			const now = new Date();

			timeElement.innerText = getRelativeDate(datetime, now);
		}, 1000);
	} else {
		console.error(
			`Impossible de faire printRelativeDate(${timeElement}) car l'élément time n'a pas l'attribut datetime.`
		);
	}
}

/**
 * - Renvoie la date relative : `${unité}${(+/-)}${quantité}`
 * @param {Date} now
 * @param {Date} date
 * @returns {String}
 */
function getRelativeDate(now, date) {
	const size = getRelativeDate_array(date, now)[0];
	const value = Math.abs(getRelativeDate_array(date, now)[1]);
	const sign = date >= now ? '+' : '-';

	return `${size.toUpperCase()}${sign}${value}`;
}

/**
 * - Renvoie la date relative : [unité, quantité] ex ['m', -2] pour passé de 2 minutes
 * @param {Date} datetime1 - Date de référence (événement)
 * @param {Date} datetime2
 * @returns {Array}
 */
function getRelativeDate_array(datetime1, datetime2, isPassed = false) {
	// On affiche l'événement datetime2 par rapport à aujourd'hui datetime1

	// Si l'événement est passé
	if (datetime1 < datetime2) {
		return getRelativeDate_array(datetime2, datetime1, true);
	}

	// * YEARS
	let years = datetime1.getFullYear() - datetime2.getFullYear();
	// La retenue
	if (datetime1.getMonth() < datetime2.getMonth()) {
		years--;
	}
	if (years > 0) {
		if (!isPassed) {
			return ['y', years];
		} else {
			return ['y', -years];
		}
	}

	// * MONTHS
	let months = datetime1.getMonth() - datetime2.getMonth();
	// S'il y a une année d'écart alors on rajoute 12 mois
	if (datetime1.getFullYear() > datetime2.getFullYear()) {
		months += 12;
	}
	// La retenue
	if (datetime1.getDate() < datetime2.getDate()) {
		months--;
	}
	if (months !== 0) {
		if (!isPassed) {
			return ['m', months];
		} else {
			return ['m', -months];
		}
	}

	// * DAYS
	let days = datetime1.getDate() - datetime2.getDate();

	// S'il y a un mois d'écart alors on rajoute le nombre de jours du mois de now
	if (datetime1.getMonth() > datetime2.getMonth()) {
		days +=
			new Date(
				datetime2.getFullYear(),
				datetime2.getMonth(),
				0
			).getDate() - 1;
	}
	if (datetime1.getFullYear() > datetime2.getFullYear()) {
		days += new Date(
			datetime1.getFullYear(),
			datetime1.getMonth(),
			0
		).getDate();
	}

	// La retenue s'il y a moins d'un jours d'écart
	if (days <= 1 && datetime1.getHours() < datetime2.getHours()) {
		days--;
	}

	if (days !== 0) {
		if (!isPassed) {
			return ['j', days];
		} else {
			return ['j', -days];
		}
	}

	// * HOURS
	let hours = datetime1.getHours() - datetime2.getHours();
	// S'il y a une journée d'écart alors on rajoute 24 heures
	if (isTomorrow(datetime1, datetime2)) {
		hours += 24;
	}
	// La retenue
	if (datetime1.getMinutes() < datetime2.getMinutes()) {
		hours--;
	}
	if (hours !== 0) {
		if (!isPassed) {
			return ['h', hours];
		} else {
			return ['h', -hours];
		}
	}

	// * MINUTES
	let minutes = datetime1.getMinutes() - datetime2.getMinutes();
	// S'il y a une heure d'écart alors on rajoute 60 minutes
	if (datetime1.getHours() > datetime2.getHours()) {
		minutes += 60;
	}
	// La retenue
	if (datetime1.getSeconds() < datetime2.getSeconds()) {
		minutes--;
	}
	if (minutes !== 0) {
		if (!isPassed) {
			return ['m', minutes];
		} else {
			return ['m', -minutes];
		}
	}

	// * SECONDS
	let seconds = datetime1.getSeconds() - datetime2.getSeconds();
	// S'il y a une minute d'écart alors on rajoute 60 secondes
	if (datetime1.getMinutes() > datetime2.getMinutes()) {
		seconds += 60;
	}
	if (!isPassed) {
		return ['s', seconds];
	} else {
		return ['s', -seconds];
	}
}

/**
 * - Renvoie la date au format 2021-12-24 00:00:00
 * @param {Date} date
 * @returns {String}
 */
function formatDate(date) {
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getUTCDate();
	const hours = date.getHours();
	const minutes = date.getMinutes();
	const seconds = date.getSeconds();
	return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

/**
 * - Ajoute les classes en fonction de la date de l'événement
 * @param {HTMLDivElement} row - Div de classe row, contenant les éléments .buttons, time, ...
 * @param {Date} now - Date de l'instant présent (optionnel)
 */
function setColorsClasses(row, now = new Date()) {
	/** @type{Date} Date de l'événement */
	const date = new Date(
		row.getElementsByTagName('time')[0].getAttribute('datetime')
	);

	/** @type{Date} Date de l'instant présent */
	// const now = new Date();

	// Si la date est passée
	if (date < now) {
		row.classList.add('passed');
	}

	// Sinon si l'événement est aujourd'hui
	else if (
		date.getFullYear() === now.getFullYear() &&
		date.getMonth() === now.getMonth() &&
		date.getDate() === now.getDate()
	) {
		row.classList.add('today');
	}

	// Sinon si l'événement est demain
	else if (
		date.getFullYear() === now.getFullYear() &&
		date.getMonth() === now.getMonth() &&
		date.getDate() === now.getDate() + 1
	) {
		row.classList.add('tomorrow');
	}

	// Sinon si l'événement est cette semaine
	else if (isSameWeek(date, now)) {
		row.classList.add('week');
	}
}

/**
 * - Renvoie vrai si les deux dates sont sont séparées d'un seul jour, faux sinon
 * @param {Date} date1
 * @param {Date} date2
 * @returns {Boolean}
 */
function isTomorrow(date1, date2) {
	if (date1 < date2) {
		return isTomorrow(date2, date1);
	}
	// Ici date1 est après date2

	const date1Day = date1.getDate();
	const date1Month = date1.getMonth();
	const date1Year = date1.getFullYear();
	const date2Day = date2.getDate();
	const date2Month = date2.getMonth();
	const date2Year = date2.getFullYear();

	// S'il y a une année d'écart
	if (date1Year === date2Year + 1) {
		// On renvoie vrai si date1 est le 1er janvier et date2 le 31 décembre
		return (
			date1Day === 1 &&
			date1Month === 0 &&
			date2Day === 31 &&
			date2Month === 11
		);
	}

	// S'il y a un mois d'écart
	if (date1Year === date2Year && date1Month === date2Month + 1) {
		// On renvoie vrai si date1 est le permier jour du mois suivant date2 et date2 dernier jour du mois
		return (
			date1Day === 1 &&
			date2Day === getMonthDays(date2Year, date2Month + 1)
		);
	}

	// Sinon on renvoie vrai si les jours se suivent dans le même mois de la même année
	return (
		date1Year === date2Year &&
		date1Month === date2Month &&
		date1Day === date2Day + 1
	);
}

/**
 * - Renvoie vrai si les deux dates sont dans la même semaine, faux sinon
 * @param {Date} date1
 * @param {Date} date2
 * @returns {Boolean}
 */
function isSameWeek(date1, date2) {
	if (date1 > date2) {
		return isSameWeek(date2, date1);
	}
	if (date1 == date2) {
		return true;
	}

	// Ici date1 est avant date2

	// Si les deux dates sont dans le même mois
	if (
		date1.getMonth() === date2.getMonth() &&
		date1.getFullYear() === date2.getFullYear()
	) {
		return (
			date1.getDate() - getDayFR(date1) ===
			date2.getDate() - getDayFR(date2)
		);
	}
	// Sinon si les deux mois se suivent
	else if (
		(date1.getFullYear() === date2.getFullYear() &&
			date1.getMonth() === date2.getMonth() - 1) ||
		(date1.getFullYear() === date2.getFullYear() - 1 &&
			date1.getMonth() === 11 &&
			date2.getMonth() === 0)
	) {
		/** type{number} Nombre de jour du mois */
		const monthDay = getMonthDays(
			date1.getFullYear(),
			date1.getMonth() + 1
		);

		/** type{number} Numéro de jour du dernier lundi du premier mois */
		const mondayIndex = monthDay - getDayFR(date1) - 1;
		/** type{number} Numéro de jour du premier dimanche du second mois */
		const sundayIndex = 6 - monthDay + mondayIndex;

		// On renvoie vrai si les deux dates se situent dans la bonne semaine
		return mondayIndex <= date1.getDate() && date2.getDate() <= sundayIndex;
	}

	return false;
}

/**
 * - Renvoie le nombre de jours du mois
 * @param {Number} year
 * @param {Number} month - Month number, from 1 to 12
 * @returns {Number}
 */
function getMonthDays(year, month) {
	switch (month) {
		case 1:
			return 31;
		case 2:
			return isLeap(year) ? 29 : 28;
		case 3:
			return 31;
		case 4:
			return 30;
		case 5:
			return 31;
		case 6:
			return 30;
		case 7:
			return 31;
		case 8:
			return 31;
		case 9:
			return 30;
		case 10:
			return 31;
		case 11:
			return 30;
		case 12:
			return 31;

		default:
			break;
	}
}

/**
 * - Renvoie vrai si l'année est bissextile, faux sinon.
 * @param {Number} year
 * @returns {Boolean}
 */
function isLeap(year) {
	// Les multiples de 400 le sont
	if (year % 400 === 0) {
		return true;
	}
	// Les multiples de 100 ne le sont pas
	if (year % 100 === 0) {
		return false;
	}
	// Les multiples de 4 le sont
	return year % 4 === 0;
}

/**
 * - Renvoie le jour de la semaine: 0 pour lundi et  6 pour dimanche
 * @param {Date} date
 * @returns {Number}
 */
function getDayFR(date) {
	return (date.getDay() + 6) % 7;
}

/**
 * - Modifie la valeur de l'input en ajoutant http:// s'il ne commence pas par http
 * @param {HTMLInputElement} urlInput - Input de type url
 * @returns {HTMLInputElement} - this
 */
function checkUrl(urlInput) {
	const urlValue = urlInput.value;
	if (urlValue.indexOf('http') !== 0) {
		urlInput.value = 'http://' + urlValue;
	}
	return urlInput;
}

/**
 * - Modifie la valeur des inputs de type url en ajoutant http:// s'il ne commence pas par http
 */
function checkUrls() {
	const forms = document.getElementsByTagName('form');
	for (let i = 0; i < forms.length; i++) {
		const form = forms[i];
		const inputs = form.getElementsByTagName('input');

		for (const input of inputs) {
			if (input.type === 'url') {
				checkUrl(input);
			}
		}
	}
}

/**
 * - Ouvre un popup avec un message demandant confirmation de suppression
 * @param {String} url
 */
function confirmDelete(url) {
	if (window.confirm('Are you sure that you want to delete this event ?')) {
		window.location.replace(url);
	}
}

function notify(message) {
	// Let's check if the browser supports notifications
	if (!('Notification' in window)) {
		alert('This browser does not support desktop notification');
	}

	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === 'granted') {
		// If it's okay let's create a notification
		new Notification(message);
	}

	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') {
		Notification.requestPermission().then(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === 'granted') {
				new Notification(message);
			}
		});
	}

	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}
