class Grid {
	// ? -----------------------------------------------------------------------------------
	// ?                                                                                   |
	// ?                                 FONCTIONS DE BASE                                 |
	// ?                                                                                   |
	// ? -----------------------------------------------------------------------------------

	/**
	 * @param {Number} nbRow - Le nombre de lignes
	 * @param {Number} nbColumn - Le nombre de colonnes
	 * @param {[[Boolean]]?} _array - Optionel : un tableau de tableaux de booléens
	 */
	constructor(nbRow, nbColumn, _array = []) {
		/** @type {Number} - Le nombre de lignes */
		this.nbRow = +nbRow;
		/** @type {Number} - Le nombre de colonnes */
		this.nbColumn = +nbColumn;
		/** @type {[[Boolean]]} - Tableau 2D des cellules (booléennes) */
		this.array = this.createArray(_array);
	}

	/**
	 * - Créer un tableau 2D de booléens
	 * @param {[[Boolean]]} _array - Un tableau 2D de booléens à copier
	 * @returns {[[Boolean]]}
	 */
	createArray(_array = []) {
		const array = [];
		for (let iRow = 0; iRow < this.nbRow; iRow++) {
			const row = [];
			for (let iColumn = 0; iColumn < this.nbColumn; iColumn++) {
				/** @type {Boolean} */
				const cell =
					iRow < _array.length &&
					iColumn < _array[iRow].length &&
					_array[iRow][iColumn] != false
						? true
						: false;
				row.push(cell);
			}
			array.push(row);
		}
		return array;
	}

	/**
	 * - Renvoie une copie de la grille
	 * @returns {[[Boolean]]}
	 */
	getArray() {
		return this.createArray(this.array);
	}

	/**
	 * - Renvoie vrai si la position est valide, faux sinon
	 * @param {Number} iRow
	 * @param {Number} iColumn
	 * @returns {Boolean}
	 */
	positionIsValid(iRow, iColumn) {
		return (
			0 <= +iRow &&
			0 <= +iColumn &&
			+iRow < this.nbRow &&
			+iColumn < this.nbColumn
		);
	}

	/**
	 * - Renvoie la cellule (Booléen) demandé si la position est valide
	 * @param {Number} iRow
	 * @param {Number} iColumn
	 * @returns {Boolean?}
	 */
	getCell(iRow, iColumn) {
		if (this.positionIsValid(iRow, iColumn)) {
			return this.array[+iRow][+iColumn];
		} else {
			console.error(`Impossible de faire getCell(${iRow}, ${iColumn})`);
		}
	}

	/**
	 * - Écrit value dans la cellule demandé si la position est valide
	 * @param {Number} iRow
	 * @param {Number} iColumn
	 * @param {Boolean} value
	 */
	setCell(iRow, iColumn, value) {
		if (this.positionIsValid(iRow, iColumn)) {
			this.array[+iRow][+iColumn] = Boolean(value);
		} else {
			console.error(
				`Impossible de faire setCell(${iRow}, ${iColumn}, ${value})`
			);
		}
	}

	// ? -----------------------------------------------------------------------------------
	// ?                                                                                   |
	// ?                             FONCTIONS SPÉCIFIQUES GOL                             |
	// ?                                                                                   |
	// ? -----------------------------------------------------------------------------------

	/**
	 * - Renvoie vrai si la cellule demandée est vivante
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {Boolean?}
	 */
	cellIsAlive(row, column) {
		if (this.positionIsValid(row, column)) {
			return this.getCell(row, column);
		}
		console.error(`Impossible de faire cellIsAlive(${row}, ${column})`);
	}

	/**
	 * - Renvoie le nombre de cellules vivantes proche de la cellule demandée
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {Number?}
	 */
	nbNeighbours(row, column) {
		if (this.positionIsValid(row, column)) {
			let total = 0;
			for (let r = row - 1; r < row + 2; r++) {
				for (let c = column - 1; c < column + 2; c++) {
					if (
						this.positionIsValid(r, c) &&
						(r !== row) | (c !== column) &&
						this.cellIsAlive(r, c)
					) {
						total++;
					}
				}
			}
			return total;
		}
		console.error(`Impossible de faire nbNeighbours(${row}, ${column})`);
	}

	/**
	 * - Renvoie le status au prochain tour si la cellule existe
	 * @param {Number} row
	 * @param {Number} column
	 * @returns {Boolean?}
	 */
	nextStatus(row, column) {
		if (this.positionIsValid(row, column)) {
			switch (this.nbNeighbours(row, column)) {
				case 3:
					// 3 voisins : vivant
					return true;
				case 2:
					// 2 voisins : pas de changement
					return this.cellIsAlive(row, column);
				default:
					// Reste : mort
					return false;
			}
		}
		console.error(`Impossible de faire nextStatus(${row}, ${column})`);
	}

	/**
	 * - Fais évoluer d'un tour la grille
	 * @returns {Boolean} - Vrai si l'évolution provoque au moins un changement
	 */
	evolve() {
		let isDifferent = false;
		const newArray = this.createArray();
		for (let iRow = 0; iRow < this.nbRow; iRow++) {
			for (let iColumn = 0; iColumn < this.nbColumn; iColumn++) {
				newArray[iRow][iColumn] = this.nextStatus(iRow, iColumn);
				if (
					this.cellIsAlive(iRow, iColumn) !==
					this.nextStatus(iRow, iColumn)
				) {
					isDifferent = true;
				}
			}
		}
		this.array = newArray;
		return isDifferent;
	}
}

// ? -------------------------------------------------------------------------------
// ?                                                                               |
// ?                                     TESTS                                     |
// ?                                                                               |
// ? -------------------------------------------------------------------------------

// const t = [[1], [0, '', ' '], [2]];
// const g = new Grid(2, 3, t);
// console.log(g.getArray());
// g.setCell(1, 1, true);
// console.log(g.getArray());
