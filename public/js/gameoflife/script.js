document.addEventListener('DOMContentLoaded', () => {
	/** @type {Number} - Le nombre de lignes de la grille */
	let nbRow = +document.getElementById('height').value;
	/** @type {Number} - Le nombre de colonnes de la grille */
	let nbColumn = +document.getElementById('width').value;
	/** @type {Number} - Le nombre de millisecondes entre deux affichages */
	const baseTime = 300;

	/** @type {Boolean} - État du jeu, vrai s'il est actif et faux sinon */
	let isRunning = false;

	const cellsList = document.getElementsByClassName('cell');
	const startButton = document.getElementById('startButton');

	let startingArray = [
		[0, 1],
		[0, 0, 1],
		[1, 1, 1],
	];

	const grid = new Grid(nbRow, nbColumn, startingArray);
	const grid_html = document.getElementsByClassName('grid')[0];

	createGrid(grid, grid_html);
	drawGrid(grid, grid_html);

	// On active le jeu
	startButton.addEventListener('click', () => {
		// Si le jeu est actif, alors on l'arrête
		// Sinon on l'active
		console.log('------------');
		isRunning = !isRunning;
		console.log(`isRunning = ${isRunning}`);
		startButton.classList.toggle('running');
		if (isRunning) {
			console.log('RUN');
			startButton.innerText = 'Stop';
			run();
		} else {
			startButton.innerText = 'Start';
		}
		if (startButton.classList.contains('running')) {
			startButton.innerText = 'Stop';
		} else {
			startButton.innerText = 'Start';
		}

		// startButton.classList.remove('disable');
		console.log('FIN');
	});

	function run() {
		let i = 0;
		while (isRunning && ++i < 50) {
			// console.log(`${i} : ${isRunning}`);
			setTimeout(() => {
				// console.log(`${i} : ${isRunning}`);
				isRunning = grid.evolve();
				drawGrid(grid, grid_html);
			}, i * baseTime);
		}
	}

	// On modifie l'état des cellules en cliquant dessus
	for (let i = 0; i < cellsList.length; i++) {
		const cell = cellsList[i];
		cell.addEventListener('click', () => {
			const iRow = Math.floor(i / nbColumn);
			const iColumn = i % nbColumn;
			// On prend l'état inverse de la cellule
			let value = !grid.getCell(iRow, iColumn);
			grid.setCell(iRow, iColumn, value);
			// On redessine la grille
			drawGrid(grid, grid_html);
		});
	}
});

// ? -----------------------------------------------------------------------------------
// ?                                                                                   |
// ?                                     FONCTIONS                                     |
// ?                                                                                   |
// ? -----------------------------------------------------------------------------------

/**
 * - Créé une grille vide html basée sur la grille js passée en paramètre
 * @param {Grid} grid_js
 * @param {HTMLElement} grid_html
 */
function createGrid(grid_js, grid_html) {
	// On vide la grille html
	while (grid_html.children.length > 0) {
		grid_html.removeChild(grid_html.children[0]);
	}

	// On créé la grille vide
	for (let iRow = 0; iRow < grid_js.nbRow; iRow++) {
		for (let iColumn = 0; iColumn < grid_js.nbColumn; iColumn++) {
			const cell = document.createElement('div');
			cell.classList.add('cell');
			cell.id = `L${iRow}-C${iColumn}`;
			grid_html.appendChild(cell);
		}
	}
}

/**
 * - Remplie la grille html en fonction de la grille js passée en paramètre
 * @param {Grid} grid_js
 * @param {HTMLElement} grid_html
 */
function drawGrid(grid_js, grid_html) {
	// On dessine la grille
	for (let iRow = 0; iRow < grid_js.nbRow; iRow++) {
		for (let iColumn = 0; iColumn < grid_js.nbColumn; iColumn++) {
			const ind = iRow * grid_js.nbColumn + iColumn;
			const cell = document.getElementById(`L${iRow}-C${iColumn}`);
			// const cell = grid_html.getElementsByClassName('cell')[ind];
			cell.classList.remove('alive');
			if (grid_js.cellIsAlive(iRow, iColumn)) {
				cell.classList.add('alive');
			}
		}
	}
}
