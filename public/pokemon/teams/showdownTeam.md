Cobalion @ Rocky Helmet  
Ability: Justified  
EVs: 252 Atk / 4 Def / 252 Spe  
Jolly Nature

-   Stealth Rock
-   Close Combat
-   Iron Head
-   Volt Switch

Grimmsnarl (M) @ Leftovers  
Ability: Prankster  
EVs: 252 HP / 4 Atk / 252 SpD  
Careful Nature

-   Spirit Break
-   Darkest Lariat
-   Reflect
-   Light Screen

Flygon @ Choice Scarf  
Ability: Levitate  
EVs: 236 Atk / 20 SpD / 252 Spe  
Adamant Nature

-   Earthquake
-   Outrage
-   U-turn
-   Toxic

Crobat @ Heavy-Duty Boots  
Ability: Infiltrator  
EVs: 252 Atk / 4 Def / 252 Spe  
Jolly Nature

-   Brave Bird
-   U-turn
-   Taunt
-   Roost

Volcanion @ Heavy-Duty Boots  
Ability: Water Absorb  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
IVs: 0 Atk

-   Steam Eruption
-   Fire Blast
-   Toxic
-   Protect

Necrozma @ Power Herb  
Ability: Prism Armor  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature

-   Meteor Beam
-   Photon Geyser
-   Heat Wave
-   Autotomize
