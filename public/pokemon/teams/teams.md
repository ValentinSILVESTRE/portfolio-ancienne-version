Roserade @ Leftovers
Ability: Natural Cure
EVs: 252 SpA / 4 SpD / 252 Spe
Timid Nature
IVs: 0 Atk

-   Spikes
-   Giga Drain
-   Sludge Bomb
-   Synthesis

Crobat @ Heavy-Duty Boots
Ability: Infiltrator
EVs: 252 Atk / 4 Def / 252 Spe
Jolly Nature

-   Brave Bird
-   Defog
-   Roost
-   U-turn

Weezing-Galar @ Black Sludge
Ability: Neutralizing Gas
EVs: 252 HP / 252 Def / 4 SpD
Bold Nature
IVs: 0 Atk

-   Sludge Bomb
-   Strange Steam
-   Pain Split
-   Will-O-Wisp

Incineroar @ Heavy-Duty Boots
Ability: Intimidate
EVs: 252 HP / 20 Atk / 236 SpD
Sassy Nature

-   Knock Off
-   Overheat
-   Toxic
-   U-turn

Necrozma @ Power Herb
Ability: Prism Armor
EVs: 252 SpA / 4 SpD / 252 Spe
Timid Nature

-   Stealth Rock
-   Photon Geyser
-   Meteor Beam
-   Heat Wave

Flygon @ Life Orb
Ability: Levitate
EVs: 236 Atk / 20 SpD / 252 Spe
Jolly Nature

-   Earthquake
-   Stone Edge
-   Dragon Dance
-   U-turn
