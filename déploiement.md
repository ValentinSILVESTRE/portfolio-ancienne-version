# Étapes pour déployer le projet symfony en ligne [(tutoriel)](https://www.youtube.com/watch?v=AAap9qRHgIk)

-   Vider le cache

-   Modifier le .env :

    -   APP_ENV:prod

    -   APP_DEBUG:0

    -   DATABASE_URL="mysql://db_user:db_password@host_name:port/db_name?serverVersion=5.7"

-   Transférer les fichiers sur le serveur, dans le dossier www avec filezilla

-   Importer la base de données

-   Faire pointer la racine du site vers www/symfony/public

-   Se connecter au server en ssh : `ssh montper-crmrse@ssh.cluster028.hosting.ovh.net -p 22`

    -   Mot de passe : OybBJRaciSmZAfrH1BqF

-   Exécuter les commandes dans le dossier symfony du serveur

    -   `/usr/bin/php8.0-cli ../../composer.phar install`

    -   `/usr/bin/php8.0-cli ../../composer.phar require symfony/apache-pack`

## `Tuto`

-   4'00 : Début
-   6'10 : Commande deploy
-   7'10 : .env
-   7'30 : Base de données
-   9'00 : Migration de la bdd
-   9'50 : Pointage
-   10'30 : htaccess
-   15'20 : Cache
-   16'20 : Vendor
-   17'00 : Installation de composer
-   18'15 : Dataenv
-   19'10 : Commande composer install
-   19'30 : Assets
-   20'15 : Conclusion
