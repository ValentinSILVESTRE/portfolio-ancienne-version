<?php
session_start();

// ? -------------------------------------------------------------------------------
// ?                                                                               |
// ?                                      PHP                                      |
// ?                                                                               |
// ? -------------------------------------------------------------------------------

// Si la difficulté n'a pas été posté, on redirige à l'acceuil
if (
    (!isset($_POST['difficulty'])
        || ($_POST['difficulty'] !== 'easy'
            && $_POST['difficulty'] !== 'medium'
            && $_POST['difficulty'] !== 'hard'
            && $_POST['difficulty'] !== 'custom'))
) {
    header('Location: .');
}

$difficulty = $_POST['difficulty'];
$_SESSION['difficulty'] = $difficulty;
setcookie('difficulty', $difficulty);

// Si la difficulté choisie n'est pas custom alors on redirige au jeu
if ($difficulty !== 'custom') {
    header('Location: play.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mines - Settings</title>
    <link rel="stylesheet" href="../normalize.css">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="settings.css">
    <link rel="stylesheet" href="form.css">
    <link rel="stylesheet" href="header.css">
    <link rel="shortcut icon" href="../../public/favicon.ico" type="image/x-icon">
</head>

<body>

    <header>
        <div class="header-first">
            <a href="../.." class="icon">
                <img src="../../public/favicon.ico" alt="" class="icon-img" />
            </a>
        </div>
        <div class="header-second">
            <h1>Site de Valentin SILVESTRE</h1>
        </div>
    </header>

    <div class="main-container">
        <div class="top-bar">
            <a href="."><button>Acceuil</button></a>
        </div>

        <?php include('form.php'); ?>
    </div>

</body>

</html>